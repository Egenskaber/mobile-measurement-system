#!/bin/sh
set -e
    wait-for-it TEST_MMS_DB:5432
    python manage.py collectstatic --settings=settings.test --no-input
    python manage.py makemigrations measurement_server --settings=settings.test
    python manage.py migrate --settings=settings.test
    echo migration
    gunicorn --bind=0.0.0.0:8000 MMS2.wsgi:application --timeout 600 --threads 8
exec "$@"
