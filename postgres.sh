# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

sudo service postgresql stop
docker stop timescaledb
docker run -d --rm \
    --name timescaledb \
    -p 127.0.0.1:5432:5432 \
    -e POSTGRES_USER=mms\
    -e POSTGRES_DB=mms\
    -e POSTGRES_PASSWORD=mms\
    timescale/timescaledb:1.7.4-pg12 
