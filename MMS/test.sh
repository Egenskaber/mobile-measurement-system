VARIABLE="${1:-test}"
BASEDIR="$(dirname "$BASH_SOURCE")"
#python $BASEDIR/manage.py migrate --settings=settings.$VARIABLE

# Example for isolated testing
#python $BASEDIR/manage.py test measurement_server.test.test_api --settings=settings.$VARIABLE --failfast

coverage run --source=$BASH_DIR $BASEDIR/manage.py test core measurement_server --settings=settings.$VARIABLE --noinput
coverage report
coverage html

