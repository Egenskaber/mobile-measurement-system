# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.test import TestCase
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.measurement import SHT85

import json


class SensorModelJsonFields(TestCase):
    def setUp(self):
        # Populate database
        self.n_sensors = 3  # Number of sensors
        self.n_values = 10  # Number of values

        # Generate measurement object.
        data = MeasurementData(name="test")
        data.save()

        [SHT85.objects.create() for i in range(self.n_sensors)]
        [data.sht.add(sht) for sht in SHT85.objects.all()]
        for sht in data.sht.all():
            sht.temperature = json.dumps(list(range(100)))
