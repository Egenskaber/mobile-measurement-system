# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

from io import BytesIO
from measurement_server.views import settingsView
from measurement_server.forms import measurementModeForm
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.log import Log
from .test_helpers import genMeasurement


class SettingsPageTest(TestCase):
    def test_sensor_deletion(self):
        # Delete inactive
        # If all sensors are disconnected.
        genMeasurement()
        KnownSensors.objects.all().update(connected=False)
        response = self.client.post("/settings/", data={"delete_sensors": "inactive"})
        self.assertEqual(KnownSensors.objects.all().count(), 0)

        # If one sensor is disconnected.
        genMeasurement()
        n_sensors = KnownSensors.objects.all().count()
        s0 = KnownSensors.objects.first()
        s0.connected = False
        s0.save()
        response = self.client.post("/settings/", data={"delete_sensors": "inactive"})
        self.assertEqual(KnownSensors.objects.all().count(), n_sensors - 1)
        self.assertEqual(KnownSensors.objects.filter(connected=False).count(), 0)

        # Delete all sensors
        genMeasurement()
        s0 = KnownSensors.objects.first()
        s0.connected = False
        s0.save()
        self.assertGreater(KnownSensors.objects.all().count(), 0)
        response = self.client.post("/settings/", data={"delete_sensors": "all"})
        self.assertEqual(KnownSensors.objects.all().count(), 0)

        # Send broken delete command
        genMeasurement()
        self.assertGreater(KnownSensors.objects.all().count(), 0)
        response = self.client.post("/settings/", data={"delete_sensors": "schwurbel"})
        self.assertIn("Unknown sensor delete", Log.objects.last().message)
