# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import json
import datetime

from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest
from unittest.mock import patch

from pytz import timezone

from .test_helpers import genMeasurement
from .test_helpers import get_time_string

from measurement_server.views.data import data_plot
from measurement_server.models.measurement import Measurement
from measurement_server.models.measurement import MeasurementData
from core.modules.sensors import SupportedSensors


class DataViewsTest(TestCase):
    def setUp(self):
        # This range is chosen to be compatible
        # with all sensor measurement value database fields
        values = range(0, 200, 10)

        t0 = datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc)
        dt = datetime.timedelta(minutes=1)
        timestamps = [t0]

        for i in range(len(values) - 1):
            timestamps.append(timestamps[-1] + dt)

        # Store values
        data, sensor_models, result_models = genMeasurement(
            n_sensors=1, timestamps=timestamps, values=values
        )

        self.data = data
        self.sensor_models = sensor_models
        self.result_models = result_models
        self.values = values
        self.timestamps = timestamps
        self.dt = dt

    def test_plot_data_invalid_database(self):
        with patch.dict("measurement_server.views.data.SupportedSensors") as mock:
            old = mock["NAU7802"]["model"]["base"]
            mock["NAU7802"]["model"]["base"] = None
            response = self.client.get(f"/api/plot/{self.data.id}/")
            plot_data = response.json()
            self.assertEqual(len(plot_data["nau7802"]), 0)
            mock["NAU7802"]["model"]["base"] = old

    def test_last_data_invalid_database(self):
        with patch.dict("measurement_server.views.data.SupportedSensors") as mock:
            old = mock["NAU7802"]["model"]["base"]
            mock["NAU7802"]["model"]["base"] = None
            response = self.client.get(f"/api/last/current/10")
            last_data = response.json()
            self.assertEqual(len(last_data["nau7802"]), 0)
            mock["NAU7802"]["model"]["base"] = old

    def test_last_data_invalid(self):
        MeasurementData.objects.all().delete()
        resp = self.client.get("/api/last/current/10")
        self.assertIn("error", resp.json().keys())

    def test_last_data(self):
        n = 10
        resp = self.client.get(f"/api/last/current/{n}")
        self.assertEqual(len(resp.json()["sfm"][0]["time"]), n)

    def test_plot_data_no_data_available(self):
        # Test no result case
        # Get plot data
        response = self.client.get("/api/plot/123/")
        plot_data = response.json()
        # Assert if all sensors return empty lists
        for sensor_cls in self.sensor_models:
            sensor_type = sensor_cls.__name__.lower()
            if sensor_type == "sht85":
                sensor_type = "sht"
            for sensor in plot_data[sensor_type]:
                self.assertEqual(0, len(sensor["results"]))

    def test_plot_data_sensor_type_not_available(self):
        # Test missing sensor type
        self.data.nau7802_set.all().delete()
        # Get plot data
        response = self.client.get(f"/api/plot/{self.data.id}/")
        plot_data = response.json()
        # Assert if all sensors return empty lists
        self.assertEqual(0, len(plot_data["nau7802"]))
        self.assertEqual(1, len(plot_data["sdp8"]))

    def test_plot_data_one_sensor_without_results(self):
        # More than one sensor needed:
        data, sensor_models, result_models = genMeasurement(n_sensors=2, n_results=20)

        # Test missing results
        data.nau7802_set.first().nau7802results_set.all().delete()
        # Get plot data
        response = self.client.get(f"/api/plot/{data.id}/")
        plot_data = response.json()
        # Assert if all sensors return empty lists
        self.assertEqual(1, len(plot_data["nau7802"]))
        self.assertEqual(20, len(plot_data["nau7802"][0]["results"]))
        self.assertEqual(20, len(plot_data["sdp8"][0]["results"]))

    def test_plot_data(self):
        """Check if plot data reduction has been implemented correctly"""

        # Try without system setup registered at Measurement
        # This implies no averaging.
        self.assertEqual(Measurement.objects.all().count(), 0)

        # Get plot data
        response = self.client.get(f"/api/plot/{self.data.id}/")
        plot_data = response.json()
        for sensor_cls in self.sensor_models:
            sensor_type = sensor_cls.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            for sensor in plot_data[sensor_type]:
                # Check time and measurmeent values
                # check only first and last entry for speed
                self.assertEqual(len(self.timestamps), len(sensor["results"]))

                t_start_str = get_time_string(self.timestamps[0])
                t_stop_str = get_time_string(self.timestamps[-1])

                self.assertEqual(sensor["results"][0]["time"][:-7], t_start_str[:-7])
                self.assertEqual(sensor["results"][-1]["time"][:-7], t_stop_str[:-7])
                for qty in sensor["results"][0].keys():
                    if qty == "time":
                        continue
                    self.assertEqual(float(sensor["results"][0][qty]), self.values[0])
                    self.assertEqual(float(sensor["results"][-1][qty]), self.values[-1])
        # Test different time averages
        mr = Measurement()
        mr.avg_bucket_size = 2 * 60  # s
        mr.plot_period = 0
        mr.save()

        # Get plot data
        response = self.client.get(f"/api/plot/{self.data.id}/")
        plot_data = response.json()
        # Assert if averaged timestamps and values are computed correctly
        for sensor_cls in self.sensor_models:
            sensor_type = sensor_cls.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            for sensor in plot_data[sensor_type]:
                self.assertEqual(len(self.timestamps) / 2, len(sensor["results"]))
                t_start_str = get_time_string(self.timestamps[0])
                t_stop_str = get_time_string(self.timestamps[-1] - self.dt)
                self.assertEqual(sensor["results"][0]["time"], t_start_str)
                self.assertEqual(sensor["results"][-1]["time"], t_stop_str)
                for qty in sensor["results"][0].keys():
                    if qty == "time":
                        continue
                    self.assertEqual(
                        float(sensor["results"][0][qty]),
                        (self.values[0] + self.values[1]) / 2,
                    )
                    self.assertEqual(
                        float(sensor["results"][-1][qty]),
                        (self.values[-1] + self.values[-2]) / 2,
                    )

        mr.avg_bucket_size = 4 * 60  # s
        mr.plot_period = 0  # min
        mr.save()

        # Get plot data
        response = self.client.get(f"/api/plot/{self.data.id}/")
        plot_data = response.json()
        # Assert if averaged timestamps and values are computed correctly
        for sensor_cls in self.sensor_models:
            sensor_type = sensor_cls.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            for sensor in plot_data[sensor_type]:
                self.assertEqual(len(self.timestamps) / 4, len(sensor["results"]))
                t_start_str = get_time_string(self.timestamps[0])
                t_stop_str = get_time_string(self.timestamps[-1] - 3 * self.dt)
                self.assertEqual(sensor["results"][0]["time"], t_start_str)
                self.assertEqual(sensor["results"][-1]["time"], t_stop_str)
                for qty in sensor["results"][0].keys():
                    if qty == "time":
                        continue
                    self.assertEqual(
                        float(sensor["results"][0][qty]),
                        (
                            self.values[0]
                            + self.values[1]
                            + self.values[2]
                            + self.values[3]
                        )
                        / 4,
                    )
                    self.assertEqual(
                        float(sensor["results"][-1][qty]),
                        (
                            self.values[-1]
                            + self.values[-2]
                            + self.values[-3]
                            + self.values[-4]
                        )
                        / 4,
                    )

        # Check plot period
        mr.plot_period = 9  # min
        mr.avg_bucket_size = 0
        mr.save()
        # Get plot data
        response = self.client.get(f"/api/plot/{self.data.id}/")
        plot_data = response.json()
        # Assert if averaged timestamps and values are computed correctly
        for sensor_cls in self.sensor_models:
            sensor_type = sensor_cls.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            for sensor in plot_data[sensor_type]:
                self.assertEqual(mr.plot_period, len(sensor["results"]))
                t_start_str = get_time_string(self.timestamps[-9])
                t_stop_str = get_time_string(self.timestamps[-1])
                self.assertEqual(sensor["results"][0]["time"][:-7], t_start_str[:-7])
                self.assertEqual(sensor["results"][-1]["time"][:-7], t_stop_str[:-7])
                for qty in sensor["results"][0].keys():
                    if qty == "time":
                        continue
                    self.assertEqual(float(sensor["results"][0][qty]), self.values[-9])
                    self.assertEqual(float(sensor["results"][-1][qty]), self.values[-1])

        # Check plot period
        mr.plot_period = 9  # min
        mr.avg_bucket_size = 2 * 60
        mr.save()
        # Get plot data
        response = self.client.get(f"/api/plot/{self.data.id}/")
        plot_data = response.json()
        # Assert if averaged timestamps and values are computed correctly
        for sensor_cls in self.sensor_models:
            sensor_type = sensor_cls.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            for sensor in plot_data[sensor_type]:
                self.assertEqual(5, len(sensor["results"]))
                t_start_str = get_time_string(self.timestamps[-10])
                t_stop_str = get_time_string(self.timestamps[-1] - self.dt)
                self.assertEqual(sensor["results"][0]["time"][:-7], t_start_str[:-7])
                self.assertEqual(sensor["results"][-1]["time"][:-7], t_stop_str[:-7])
                for qty in sensor["results"][0].keys():
                    if qty == "time":
                        continue
                    self.assertEqual(
                        float(sensor["results"][0][qty]),
                        (self.values[-10] + self.values[-9] + self.values[-8]) / 3,
                    )
                    self.assertEqual(
                        float(sensor["results"][-1][qty]),
                        (self.values[-1] + self.values[-2]) / 2,
                    )
