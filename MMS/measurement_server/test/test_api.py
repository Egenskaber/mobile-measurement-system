# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

import datetime
import json

from .test_helpers import genMeasurement
from .test_helpers import get_time_string

from measurement_server.models.measurement import Measurement
from measurement_server.models.init import Init

from measurement_server.views.api import quick_measurement


class DocumentationApiTest(TestCase):
    def test_documentation(self):
        resp = self.client.get("/api/downloads/")
        data = resp.json()
        self.assertIsInstance(data, list)
        for key in ["url", "name", "image"]:
            self.assertIn(key, data[0].keys())


class InitApiTest(TestCase):
    def test_api_redirect(self):
        # Test no result case
        # Get plot data
        resp0 = self.client.get("/cmd/init")
        Init.objects.all().delete()
        resp1 = self.client.get("/api/initialize")
        self.assertEqual(resp0.json()["error"], resp1.json()["error"])

    def test_get(self):
        # No entry in DB -> return error
        resp = self.client.get("/api/initialize")
        self.assertIn("error", resp.json().keys())
        # After first error an entry is created
        resp = self.client.get("/api/initialize")
        self.assertIn("message", resp.json().keys())

    def test_start_init(self):
        Init.objects.create()
        self.assertEqual(Init.objects.all().count(), 1)
        resp = self.client.post("/api/initialize", {"init": True})
        self.assertIn("message", resp.json().keys())
        self.assertEqual(resp.json()["data"]["init"], True)

    def test_invalid_calls(self):
        # Init entry needs to be True
        resp = self.client.post("/api/initialize", {"init": False})
        self.assertIn("error", resp.json().keys())
        Init.objects.all().delete()

        # A key named `init` needs to be included
        resp = self.client.post("/api/initialize", {"invalid": True})
        self.assertIn("error", resp.json().keys())

        # If a Init is running already an error is returned
        init = Init.objects.last()
        init.init = True
        init.save()
        resp = self.client.post("/api/initialize", {"init": True})
        self.assertIn("error", resp.json().keys())

        # If more than one init element is present in the database return an error
        Init.objects.create()
        self.assertEqual(Init.objects.all().count(), 2)
        resp = self.client.post("/api/initialize", {"init": True})
        self.assertIn("error", resp.json().keys())


class QuickApiTest(TestCase):
    def test_api_redirect(self):
        resp0 = self.client.get("/quick/")
        resp1 = self.client.get("/api/quick")
        self.assertEqual(resp0.json(), resp1.json())

    def test_quick_measurement_unhealthy_database(self):
        resp1 = self.client.get("/api/quick")
        answ = resp1.json()
        self.assertIn("error", answ.keys())

    def test_quick_invalid_post(self):
        resp = self.client.post("/api/quick", {"quick": "invalid_post"})
        self.assertIn("error", resp.json().keys())
        resp = self.client.post("/api/quick", {"invalid": "start"})
        self.assertIn("error", resp.json().keys())
        resp = self.client.post("/api/quick", {"invalid": "stop"})
        self.assertIn("error", resp.json().keys())

    def test_quick_start(self):
        self.assertEqual(Measurement.objects.count(), 0)
        resp = self.client.post("/api/quick", {"quick": "start"})
        self.assertEqual(Measurement.objects.count(), 1)
        resp = self.client.post("/api/quick", {"quick": "start"})
        self.assertIn("error", resp.json().keys())
        Measurement().save()
        self.assertEqual(Measurement.objects.count(), 2)
        resp = self.client.post("/api/quick", {"quick": "start"})
        self.assertEqual(Measurement.objects.count(), 1)

    def test_quick_stop(self):
        # No measurement is running -> throws error
        self.assertEqual(Measurement.objects.count(), 0)
        resp = self.client.post("/api/quick", {"quick": "stop"})
        self.assertIn("error", resp.json().keys())

        # Stop a running measurement
        self.assertEqual(Measurement.objects.count(), 1)
        mr = Measurement.objects.last()
        mr.active = True
        mr.save()
        resp = self.client.post("/api/quick", {"quick": "stop"})
        self.assertEqual(Measurement.objects.count(), 1)
        self.assertFalse(Measurement.objects.last().active)

        # Multiple system state entries
        Measurement().save()
        self.assertEqual(Measurement.objects.count(), 2)
        resp = self.client.post("/api/quick", {"quick": "stop"})
        self.assertIn("error", resp.json().keys())
