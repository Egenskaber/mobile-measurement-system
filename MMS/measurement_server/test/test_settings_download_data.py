import os
import io
import json
import re
import csv
from statistics import mean
import time

import zipfile

import numpy as np
import netCDF4
from unicodedata import name
from unittest import result, skip

from pytz import timezone
from datetime import datetime
from datetime import timedelta
from typing import final
from pathlib import Path

from django import db, test
from django.test import TestCase
from django.http import HttpRequest


from core.management.commands.readSensor import Command

from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.measurement import Units
from measurement_server.models.measurement import Measurement
from measurement_server.models.measurement import PlotSettings
from measurement_server.models.log import Log

from measurement_server.modules.dataExport import dataExport

from measurement_server.views import settingsView

from core.modules.sensors import SupportedSensors

from .test_helpers import genMeasurement


class DBSensorTest(TestCase):
    n_sensors = 2
    n_results = 2

    def test_check_for_sensor_results(self):
        _, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        self.assertEqual(MeasurementData.objects.all().count(), 1)
        data = MeasurementData.objects.last()

        for sensor_model, result_model in zip(sensor_models, result_models):
            sensors = getattr(data, sensor_model.__name__.lower() + "_set").all()
            self.assertEqual(sensors.count(), self.n_sensors)
            results = getattr(
                sensors[0], sensor_model.__name__.lower() + "results_set"
            ).all()
            self.assertEqual(results.count(), self.n_results)

    def test_delete_measurement(self):
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_results, n_results=self.n_results
        )

        for sensor_model in sensor_models:
            self.assertEqual(sensor_model.objects.all().count(), self.n_results)

        self.assertEqual(MeasurementData.objects.all().count(), 1)

        data = MeasurementData.objects.first()
        data.delete()

        # Check if all MeasurementData objects were deleted
        self.assertEqual(MeasurementData.objects.all().count(), 0)

        # Check if all sensor objects were deleted
        for sensor_model in sensor_models:
            self.assertEqual(sensor_model.objects.all().count(), 0)

        # Check if all result objects were deleted
        for result_model in result_models:
            self.assertEqual(result_model.objects.all().count(), 0)


class ExportUnifiedTimeAxis(TestCase):
    n_sensors = 2
    n_results = 10

    # Set /results/ folder path
    absolute_path = os.path.abspath(__file__)
    file_dir = os.path.dirname(absolute_path)
    parent_dir = os.path.dirname(file_dir)
    result_dir = Path(os.path.join(parent_dir, "media", "results"))

    def setUp(self):
        # Check if /media/results/ folder exists
        if not os.path.exists(self.result_dir):
            Path(self.result_dir).mkdir()

    def tearDown(self):
        pass
        ## Delete exported unit test files
        # for file in os.listdir(self.result_dir):
        #    if file.startswith("unit_test_export_"):
        #        os.remove(os.path.join(self.result_dir, file))

    def test_export_nc_unified(self):
        nc_results_list = []  # list for checking csv results
        nc_total_results = 0  # total number of csv results
        db_results_list = []  # list for checking database results
        db_total_results = 0  # total number of database results
        sensor_counter = 0  # total number of sensors in nc files

        temp_qties_list = []  # temporary list for storing qties
        temp_results_list = []  # temporary list for storing results

        n_results = 300
        t0 = datetime(2000, 1, 1, 1, 11).astimezone(timezone("Europe/Berlin"))
        t = np.linspace(0, 10 * np.pi, n_results)

        # Generate chirp as sample data
        timestamps = [t0 + timedelta(seconds=i) for i in t]
        values = np.exp(-(((np.mean(t) - t) / np.std(t)) ** 2)) * np.sin(t * 2)

        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors,
            n_results=n_results,
            timestamps=timestamps,
            values=values,
        )

        # Create database result list
        for result_model in result_models:
            for value in result_model.objects.values():
                del value["results_id"]
                for key, value in value.items():
                    # Differentiate between adding time or measurement results
                    # Convert and add time
                    if key == "time":
                        value = value.astimezone(timezone("Europe/Berlin"))
                        temp_list = [key, value.strftime("%Y-%m-%d %H:%M:%S.%f%z")]
                        db_results_list.append(temp_list)
                    # Convert and add measurement results
                    else:
                        temp_list = [key, float(value)]
                        db_results_list.append(temp_list)

        dataExport(data, file_format="nc")


class ExportAndDeleteMeasurementsTest(TestCase):
    n_sensors = 2
    n_results = 10

    # Set /results/ folder path
    absolute_path = os.path.abspath(__file__)
    file_dir = os.path.dirname(absolute_path)
    parent_dir = os.path.dirname(file_dir)
    result_dir = Path(os.path.join(parent_dir, "media", "results"))

    def setUp(self):
        # Check if /media/results/ folder exists
        if not os.path.exists(self.result_dir):
            Path(self.result_dir).mkdir()

    def tearDown(self):
        pass
        ## Delete exported unit test files
        # for file in os.listdir(self.result_dir):
        #    if file.startswith("unit_test_export_"):
        #        os.remove(os.path.join(self.result_dir, file))

    def test_export_nc_unified(self):
        nc_results_list = []  # list for checking csv results
        nc_total_results = 0  # total number of csv results
        db_results_list = []  # list for checking database results
        db_total_results = 0  # total number of database results
        sensor_counter = 0  # total number of sensors in nc files

        temp_qties_list = []  # temporary list for storing qties
        temp_results_list = []  # temporary list for storing results

        n_results = 300
        t0 = datetime(2000, 1, 1, 1, 11).astimezone(timezone("Europe/Berlin"))
        t = np.linspace(0, 10 * np.pi, n_results)

        # Generate chirp as sample data
        timestamps = [t0 + timedelta(seconds=i) for i in t]
        values = np.exp(-(((np.mean(t) - t) / np.std(t)) ** 2)) * np.sin(t * 2)

        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors,
            n_results=n_results,
            timestamps=timestamps,
            values=values,
        )

        # Create database result list
        for result_model in result_models:
            for value in result_model.objects.values():
                del value["results_id"]
                for key, value in value.items():
                    # Differentiate between adding time or measurement results
                    # Convert and add time
                    if key == "time":
                        value = value.astimezone(timezone("Europe/Berlin"))
                        temp_list = [key, value.strftime("%Y-%m-%d %H:%M:%S.%f%z")]
                        db_results_list.append(temp_list)
                    # Convert and add measurement results
                    else:
                        temp_list = [key, float(value)]
                        db_results_list.append(temp_list)

        dataExport(data, file_format="nc")


class ExportAndDeleteMeasurementsTest(TestCase):
    n_sensors = 2
    n_results = 10

    # Set /results/ folder path
    absolute_path = os.path.abspath(__file__)
    file_dir = os.path.dirname(absolute_path)
    parent_dir = os.path.dirname(file_dir)
    result_dir = Path(os.path.join(parent_dir, "media", "results"))

    def setUp(self):
        # Check if /media/results/ folder exists
        if not os.path.exists(self.result_dir):
            Path(self.result_dir).mkdir()

    def tearDown(self):
        # Delete exported unit test files
        for file in os.listdir(self.result_dir):
            if file.startswith("unit_test_export_"):
                os.remove(os.path.join(self.result_dir, file))

    def test_export_json(self):
        json_results_list = []  # list for checking json results
        json_total_results = 0  # total number of json results
        db_results_list = []  # list for checking database results
        db_total_results = 0  # total number of database results
        file_count = 0  # total number of exported files

        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        # Create database result list
        for result_model in result_models:
            for value in result_model.objects.values():
                del value["results_id"]
                for key, value in value.items():
                    # Differentiate between adding time or measurement results
                    # Convert and add time
                    if key == "time":
                        value = value.astimezone(timezone("Europe/Berlin"))
                        temp_list = [key, value.strftime("%Y-%m-%d %H:%M:%S.%f%z")]
                        db_results_list.append(temp_list)
                    # Convert and add measurement results
                    else:
                        temp_list = [key, float(value)]
                        db_results_list.append(temp_list)

        # Count total number of results found in database
        db_total_results = len(db_results_list)

        # Send save request
        response = self.client.post("/settings/", data={"save": f"{data.id}_json"})

        # Check response for correct attachment
        response_check = response["Content-Disposition"]
        self.assertIn("attachment" and f"filename={data.name}", response_check)

        # Access zip file
        zip = zipfile.ZipFile(io.BytesIO(response.content))

        # Count total number of exported files
        file_count = len(zip.filelist)

        # Access files in zip
        for item in zip.infolist():
            temp_file_content = zip.read(item.filename)

            # Convert file content from byte to string
            temp_file_content = temp_file_content.decode()
            temp_file_content = temp_file_content[0:]

            # Convert file content
            file_content = json.loads(temp_file_content)

            # Create json results list
            for i in range(len(file_content["results"])):
                temp_list = [file_content["results"][i]]

                for key, value in temp_list[0].items():
                    # Differentiate between adding time or measurement result
                    # Add time
                    if key == "time":
                        temp_list = [key, value]
                        json_results_list.append(temp_list)
                    # Convert and add measurement result
                    else:
                        temp_list = [key, float(value)]
                        json_results_list.append(temp_list)

        # Count total number of results found in files
        json_total_results = len(json_results_list)

        # Sort final lists
        db_results_list = sorted(db_results_list)
        json_results_list = sorted(json_results_list)

        # Check if number of sensors in database = number of exported sensors
        self.assertEqual(self.n_sensors * len(sensor_models), file_count)

        # Check if number of database results = number of exported results
        self.assertEqual(db_total_results, json_total_results)

        # Check if results in database = results in exported files
        self.assertEqual(db_results_list, json_results_list)

    def test_export_csv(self):
        csv_results_list = []  # list for checking csv results
        csv_total_results = 0  # total number of csv results
        db_results_list = []  # list for checking database results
        db_total_results = 0  # total number of database results
        file_count = 0  # total number of exported files

        temp_units_list = []  # temporary list for storing units
        temp_results_list = []  # temporary list for storing results

        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        # Create database result list
        for result_model in result_models:
            for value in result_model.objects.values():
                del value["results_id"]
                for key, value in value.items():
                    # Differentiate between adding time or measurement results
                    # Convert and add time
                    if key == "time":
                        value = value.astimezone(timezone("Europe/Berlin"))
                        temp_list = [key, value.strftime("%Y-%m-%d %H:%M:%S.%f%z")]
                        db_results_list.append(temp_list)
                    # Convert and add measurement results
                    else:
                        temp_list = [key, float(value)]
                        db_results_list.append(temp_list)

        # Count total number of results found in database
        db_total_results = len(db_results_list)

        # Send save request
        response = self.client.post("/settings/", data={"save": f"{data.id}_csv"})

        # Check response for correct attachment
        response_check = response["Content-Disposition"]
        self.assertIn("attachment" and f"filename={data.name}", response_check)

        # Access zip file
        zip = zipfile.ZipFile(io.BytesIO(response.content))

        # Count total number of exported files
        file_count = len(zip.filelist)

        # Access files in zip
        for item in zip.infolist():
            temp_file_content = zip.read(item.filename)

            # Convert file content from byte to string
            temp_file_content = temp_file_content.decode()

            # Split file content and create list for testing
            file_content = []
            reader = csv.reader(temp_file_content.split("\n"))

            # Extract results from file
            for row in reader:
                if row:
                    if row[0] != "# ":
                        file_content.append(row)

            # Create temporary results lists
            for i in range(self.n_results):
                # Add time
                temp_list = ["time", file_content[1:][i][0]]
                csv_results_list.append(temp_list)

                # Convert and add qties
                for j in range(len(file_content[0][1:])):
                    temp_list = file_content[0][1:][j]
                    pattern = r"\[[^\]]*\]"
                    temp_list = re.sub(pattern, "", temp_list)
                    temp_list = re.sub("]", "", temp_list)
                    temp_units_list.append(temp_list)

            # Convert and add results
            for i in range(len(file_content[1:])):
                temp_list = file_content[1:][i][1:]
                for j in range(len(temp_list)):
                    temp_var = float(temp_list[j])
                    temp_results_list.append(temp_var)

        # Create final csv results list
        for i in range(len(temp_units_list)):
            temp_list = [temp_units_list[i], temp_results_list[i]]
            csv_results_list.append(temp_list)

        # Count total number of results found in files
        csv_total_results = len(csv_results_list)

        # Sort final lists
        db_results_list = sorted(db_results_list)
        csv_results_list = sorted(csv_results_list)

        # Check if number of sensors in database = number of exported sensors
        self.assertEqual(self.n_sensors * len(sensor_models), file_count)

        # Check if number of database results = number of exported results
        self.assertEqual(db_total_results, csv_total_results)

        # Check if results in database = results in exported files
        self.assertEqual(db_results_list, csv_results_list)

    def test_export_nc(self):
        nc_results_list = []  # list for checking csv results
        nc_total_results = 0  # total number of csv results
        db_results_list = []  # list for checking database results
        db_total_results = 0  # total number of database results
        sensor_counter = 0  # total number of sensors in nc files

        temp_qties_list = []  # temporary list for storing qties
        temp_results_list = []  # temporary list for storing results

        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        # Create database result list
        for result_model in result_models:
            for value in result_model.objects.values():
                del value["results_id"]
                for key, value in value.items():
                    # Differentiate between adding time or measurement results
                    # Convert and add time
                    if key == "time":
                        value = value.astimezone(timezone("Europe/Berlin"))
                        temp_list = [key, value.strftime("%Y-%m-%d %H:%M:%S.%f%z")]
                        db_results_list.append(temp_list)
                    # Convert and add measurement results
                    else:
                        temp_list = [key, float(value)]
                        db_results_list.append(temp_list)

        # Count total number of results found in database
        db_total_results = len(db_results_list)

        # Send save request
        response = self.client.post("/settings/", data={"save": f"{data.id}_nc"})

        # Check response for correct attachment
        response_check = response["Content-Disposition"]
        self.assertIn("attachment" and f"filename={data.name}", response_check)

        # Access zip file
        zip = zipfile.ZipFile(io.BytesIO(response.content))

        # Access files in zip
        file_content = netCDF4.Dataset(f"{self.result_dir}/{data.name}.nc")

        # Loop over all sensor groups and sub groups
        for sensor_type in file_content.groups.keys():
            # Count number of sensor types found
            sensor_counter += len(file_content.groups[sensor_type].groups.keys())

            # Set sensor sub group for each sensor type
            sensor_sub_group = file_content.groups[sensor_type].groups

            for sensor in sensor_sub_group.keys():
                # Set data group for each sensor
                sensor_data_group = file_content.groups[sensor_type][sensor]

                for key, value in sensor_data_group.variables.items():
                    for i in range(len(value)):
                        # Differentiate between adding time or measurement results
                        # Convert and add time
                        if key == "time":
                            t0_str = sensor_data_group.variables["time"].units
                            t = datetime.strptime(
                                t0_str, "seconds since %Y-%m-%d %H:%M:%S.%f%z"
                            )
                            t = t.astimezone(timezone("Europe/Berlin"))

                            t += timedelta(seconds=float(value[i]))
                            t_str = t.strftime("%Y-%m-%d %H:%M:%S.%f%z")
                            temp_list = ["time", t_str]
                            nc_results_list.append(temp_list)

                        # Convert and add results
                        else:
                            temp_qties_list.append(key)
                            temp_results_list.append(float("{:.2f}".format(value[i])))

        # Create final nc results list
        for i in range(len(temp_qties_list)):
            temp_list = [temp_qties_list[i], temp_results_list[i]]
            nc_results_list.append(temp_list)

        # Sort final lists
        db_results_list = sorted(db_results_list)
        nc_results_list = sorted(nc_results_list)

        # Count total number of results found in files
        nc_total_results = len(nc_results_list)

        # Check if number of sensor types in DB = number of sensor types in file
        self.assertEqual(len(file_content.groups.keys()), len(sensor_models))

        # Check if number of database results = number of exported results
        self.assertEqual(db_total_results, nc_total_results)

        # Check if number of sensors in database = number of sensors in file
        self.assertEqual(self.n_sensors * len(sensor_models), sensor_counter)

        # Check if results in database = results in exported files
        self.assertEqual(db_results_list, nc_results_list)

    def test_export_nc(self):
        nc_results_list = []  # list for checking csv results
        nc_total_results = 0  # total number of csv results
        db_results_list = []  # list for checking database results
        db_total_results = 0  # total number of database results
        sensor_counter = 0  # total number of sensors in nc files

        temp_qties_list = []  # temporary list for storing qties
        temp_results_list = []  # temporary list for storing results

        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        # Create database result list
        for result_model in result_models:
            for value in result_model.objects.values():
                del value["results_id"]
                for key, value in value.items():
                    # Differentiate between adding time or measurement results
                    # Convert and add time
                    if key == "time":
                        value = value.astimezone(timezone("Europe/Berlin"))
                        temp_list = [key, value.strftime("%Y-%m-%d %H:%M:%S.%f%z")]
                        db_results_list.append(temp_list)
                    # Convert and add measurement results
                    else:
                        temp_list = [key, float(value)]
                        db_results_list.append(temp_list)

        # Count total number of results found in database
        db_total_results = len(db_results_list)

        # Send save request
        response = self.client.post("/settings/", data={"save": f"{data.id}_nc"})

        # Check response for correct attachment
        response_check = response["Content-Disposition"]
        self.assertIn("attachment" and f"filename={data.name}", response_check)

        # Access zip file
        zip = zipfile.ZipFile(io.BytesIO(response.content))

        # Access files in zip
        file_content = netCDF4.Dataset(f"{self.result_dir}/{data.name}.nc")

        # Loop over all sensor groups and sub groups
        for sensor_type in file_content.groups.keys():
            # Count number of sensor types found
            sensor_counter += len(file_content.groups[sensor_type].groups.keys())

            # Set sensor sub group for each sensor type
            sensor_sub_group = file_content.groups[sensor_type].groups

            for sensor in sensor_sub_group.keys():
                # Set data group for each sensor
                sensor_data_group = file_content.groups[sensor_type][sensor]

                for key, value in sensor_data_group.variables.items():
                    for i in range(len(value)):
                        # Differentiate between adding time or measurement results
                        # Convert and add time
                        if key == "time":
                            t0_str = sensor_data_group.variables["time"].units
                            t = datetime.strptime(
                                t0_str, "seconds since %Y-%m-%d %H:%M:%S.%f%z"
                            )
                            t = t.astimezone(timezone("Europe/Berlin"))

                            t += timedelta(seconds=float(value[i]))
                            t_str = t.strftime("%Y-%m-%d %H:%M:%S.%f%z")
                            temp_list = ["time", t_str]
                            nc_results_list.append(temp_list)

                        # Convert and add results
                        else:
                            temp_qties_list.append(key)
                            temp_results_list.append(float("{:.2f}".format(value[i])))

        # Create final nc results list
        for i in range(len(temp_qties_list)):
            temp_list = [temp_qties_list[i], temp_results_list[i]]
            nc_results_list.append(temp_list)

        # Sort final lists
        db_results_list = sorted(db_results_list)
        nc_results_list = sorted(nc_results_list)

        # Count total number of results found in files
        nc_total_results = len(nc_results_list)

        # Check if number of sensor types in DB = number of sensor types in file
        self.assertEqual(len(file_content.groups.keys()), len(sensor_models))

        # Check if number of database results = number of exported results
        self.assertEqual(db_total_results, nc_total_results)

        # Check if number of sensors in database = number of sensors in file
        self.assertEqual(self.n_sensors * len(sensor_models), sensor_counter)

        # Check if results in database = results in exported files
        self.assertEqual(db_results_list, nc_results_list)

    def test_delete_measurements(self):
        mr1, _, _ = genMeasurement(n_sensors=self.n_sensors, n_results=self.n_results)
        mr2, _, _ = genMeasurement(n_sensors=self.n_sensors, n_results=self.n_results)
        mr3, _, _ = genMeasurement(n_sensors=self.n_sensors, n_results=self.n_results)

        # Get list of files inside result directory
        result_contents_initial = os.listdir(self.result_dir)

        # Check if button is in view
        request = HttpRequest()
        response = settingsView(request)
        html = response.content.decode("utf8")
        delete_cmd = f"delete_{mr1.id}"
        self.assertIn(delete_cmd, html)

        # Send Measurement 1 save request for all supported formats
        for fmt in dataExport.export_formats:
            response = self.client.post(
                "/settings/", data={"save": f"{mr1.id}_{fmt['extension']}"}
            )

        # Send single delete request for Measurement 1
        response = self.client.post("/settings/", data={"delete_measurement": mr1.id})

        # Check if local unit test measurement files were deleted
        result_contents_after_deletion = os.listdir(self.result_dir)
        self.assertListEqual(result_contents_initial, result_contents_after_deletion)

        # Check if measurement entry has been removed
        response = settingsView(request)
        html = response.content.decode("utf8")
        self.assertNotIn(delete_cmd, html)

        # Send Measurement 2 and 3 save request for all supported formats
        for fmt in dataExport.export_formats:
            response = self.client.post(
                "/settings/", data={"save": f"{mr2.id}_{fmt['extension']}"}
            )
            response = self.client.post(
                "/settings/", data={"save": f"{mr3.id}_{fmt['extension']}"}
            )

        self.assertEqual(MeasurementData.objects.all().count(), 2)

        # Send delete all request
        response = self.client.post("/settings/", data={"delete_all_data": "all"})

        # Delete rest of measurement entries
        response = settingsView(request)
        html = response.content.decode("utf8")

        # Check if sensor and result cascading
        for sensor_type in SupportedSensors:
            n_results = (
                SupportedSensors[sensor_type]["model"]["res"].objects.all().count()
            )
            self.assertEqual(n_results, 0)
            n_sensors = (
                SupportedSensors[sensor_type]["model"]["base"].objects.all().count()
            )
            self.assertEqual(n_sensors, 0)

    def test_delete_non_existing(self):
        # Try to delete non existing measurement
        self.client.post("/settings/", data={"delete_measurement": 42})
        self.assertEqual(Log.objects.last().typ, "Error")

    def test_view_for_data(self):
        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        # Set up html response
        request = HttpRequest()
        response = settingsView(request)
        html = response.content.decode("utf8")

        # Check for file name in view
        self.assertIn(data.name, html)

        # Check for correct id and save button for supported file formats
        supportedFormats = ["json", "csv", "nc"]

        for format in supportedFormats:
            save_button = f'name="save" value="{data.id}_{format}"'
            self.assertIn(save_button, html)

    def test_for_file_format(self):
        # Generate Measurement Data
        data, sensor_models, result_models = genMeasurement(
            n_sensors=self.n_sensors, n_results=self.n_results
        )

        # Check if correct zip file was created
        # and number of files returned from associated function = number of sensors in DB
        supportedFormats = ["json", "csv", "nc"]

        for format in supportedFormats:
            file_format = format
            tempData = dataExport(data, file_format)
            tempZip = zipfile.ZipFile(tempData.zip_file)

            if file_format != "nc":
                self.assertEqual(
                    len(tempZip.filelist), self.n_sensors * len(sensor_models)
                )

            # Check if files in zip have correct file extension
            for item in tempZip.namelist():
                self.assertIn(f".{format}", item)

        # Check if unsupported file format raises value error
        file_format = "abc"
        self.assertRaises(ValueError, dataExport, data, file_format)

    def test_for_duplicate_measurement_names(self):
        cmd = Command()

        # Check if function exists
        self.assertTrue(hasattr(cmd, "create_measurement_result_entry"))

        # Create Measurement object
        state = Measurement.objects.create(name="UniqueTestMeasurement")

        # Call function to create MeasurementData objects
        n_data_objects = 4  # No of MeasurementData objects

        for i in range(n_data_objects):
            result = cmd.create_measurement_result_entry()

        # Check if Measurement object was created
        self.assertEqual(Measurement.objects.all().count(), 1)

        # Check if correct number of MeasurementData objects was created
        self.assertEqual(MeasurementData.objects.all().count(), n_data_objects)

        # Check if result name contains state name
        self.assertIn(state.name, result.name)
