import unittest
import random
import string
import time

from selenium.webdriver.support.select import Select
from selenium import webdriver
from django.db import connection

from measurement_server.modules.unit_names import unit_names
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.measurement import SHT, SHTresults
from measurement_server.models.measurement import S8000, S8000results
from measurement_server.models.measurement import MAX31865, MAX31865results
from measurement_server.models.measurement import Measurement
from measurement_server.models.measurement import KnownSensors, Units

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from core.modules.sensors import SupportedSensors


class MmsUserTest(StaticLiveServerTestCase):
    @classmethod
    def getRandomIdentifier(cls, n):
        random_string = ""
        for _ in range(10):
            random_integer = random.randint(65, 90)
            # Keep appending random characters using chr(x)
            random_string += chr(random_integer)
        return random_string

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.cache.disk.enable", False)
        profile.set_preference("browser.cache.memory.enable", False)
        profile.set_preference("browser.cache.offline.enable", False)
        profile.set_preference("network.http.use-cache", False)
        cls.webdriver = webdriver.Firefox(profile)
        cls.webdriver.implicitly_wait(10)

    def tearDown(self):
        self.webdriver.close()
        pass

    @classmethod
    def tearDownClass(cls):
        cursor = connection.cursor()
        cls.webdriver.quit()

    # def test_can_start_mms_and_switch_spi_mux_settings(self):

    #    self.browser.get("http://localhost:4445/settings/#general")
    #    self.assertIn("MMS", self.browser.title)
    #    general = self.browser.find_element_by_id("general")
    #    self.assertIn("active", general.get_attribute("class"))
    #    self.assertIn("show", general.get_attribute("class"))

    #    spi_mux_mode = Select(self.browser.find_element_by_id("spi_mux_switch"))
    #    spi_mux_mode.select_by_visible_text("MUX")
    #    spi_mux_mode.select_by_visible_text("GPIO")

    def test_live_view_layout_works_for_many_sensor_application(self):
        n_sensors = 10
        n_results = 10

        data = MeasurementData.objects.create(name="test")
        data.save()
        mr = Measurement()
        mr.init_ftdi = True
        mr.active = True
        mr.result_id = data.id
        mr.live_mean = n_results
        mr.start_measure = data.start_measure
        mr.connected_sensors = n_sensors
        mr.save()

        sensor_models = []
        result_models = []
        units = {}

        for sensor in SupportedSensors.keys():
            sensor_models += [SupportedSensors[sensor]["model"]["base"]]
            result_models += [SupportedSensors[sensor]["model"]["res"]]
            units.update(SupportedSensors[sensor]["sensor"]._units)

        qties = units.keys()
        signs = units.values()

        for qty, sign in zip(qties, signs):
            Units.objects.create(name=qty, qty=qty, sign=sign)

        for sensor_model, result_model in zip(sensor_models, result_models):
            for i in range(n_sensors):
                sensor_identifier = self.getRandomIdentifier(i * 2)
                sen = sensor_model.objects.create(
                    SENSORserial=sensor_identifier,
                    sensor=data,
                    SENSORtype=sensor_model.__name__,
                )
                ks = KnownSensors.objects.create(
                    SENSORserial=sensor_identifier, connected=True
                )
                for qty in qties:
                    if hasattr(result_model, qty):
                        sen.units.add(Units.objects.get(qty=qty))
                        ks.units.add(Units.objects.get(qty=qty))
                sen.save()

            self.assertEqual(sensor_model.objects.all().count(), n_sensors)

        for sensor_model, result_model in zip(sensor_models, result_models):
            for sensor in sensor_model.objects.all():
                for j in range(n_results):
                    res = result_model.objects.create(results=sensor)
                    for qty in qties:
                        if hasattr(result_model, qty):
                            setattr(res, qty, random.random() * 100)
                    res.save()
            self.assertEqual(result_model.objects.all().count(), n_results * n_sensors)

        self.webdriver.get(self.live_server_url + "/live")
