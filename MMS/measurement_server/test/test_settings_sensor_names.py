# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

from random import random
from io import BytesIO
from measurement_server.views import settingsView
from measurement_server.forms import measurementModeForm
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.log import Log
from .test_settings_download_data import genMeasurement


class SettingsPageTest(TestCase):
    n_sensors = 10

    def test_sensor_renameing(self):
        # Check if manipulating sensor information via post request works

        # Create sensors and measurement data
        genMeasurement(n_sensors=self.n_sensors)

        # Compile post content
        sensors = KnownSensors.objects.all()
        payload = {
            "sensor_names": "",
        }

        # Leave last n sensors untouched
        modified_sensor = None
        for sensor in sensors:
            if modified_sensor is None and sensor.plots.all().count() > 1:
                modified_sensor = sensor
                continue

            payload.update(
                {
                    f"{sensor.SENSORserial}-identifier": sensor.SENSORidentifier,
                    f"{sensor.SENSORserial}-serial": sensor.SENSORserial,
                    f"{sensor.SENSORserial}-color": sensor.Color,
                    f"{sensor.SENSORserial}-x": sensor.x,
                    f"{sensor.SENSORserial}-y": sensor.y,
                    f"{sensor.SENSORserial}-z": sensor.z,
                }
            )
        sensor = None

        # Change information of last sensor
        sensor_pk = modified_sensor.pk
        old_identifer = modified_sensor.SENSORidentifier

        x = f"{random()*1000:0.2f}"
        y = f"{random()*1000:0.2f}"
        z = f"{random()*1000:0.2f}"
        color = "#babb1e"

        # Modify plot configuration
        plots = modified_sensor.plots.values_list("qty", flat=True)
        inactive = plots[0]
        active = plots[1:]

        # Add a false qty (should simply be ignored)
        plots_payload = list(active) + ["fake-qty"]

        payload.update(
            {
                f"{modified_sensor.SENSORserial}-identifier": modified_sensor.SENSORidentifier
                + "abc",
                f"{modified_sensor.SENSORserial}-color": color,
                f"{modified_sensor.SENSORserial}-serial": modified_sensor.SENSORserial,
                f"{modified_sensor.SENSORserial}-x": x,
                f"{modified_sensor.SENSORserial}-y": y,
                f"{modified_sensor.SENSORserial}-z": z,
                f"plots-{modified_sensor.SENSORserial}": plots_payload,
            }
        )

        ## Add a unknown sensor serial (should simply be ignored)
        # payload.update(
        #    {
        #        f"fakesensor-identifier": modified_sensor.SENSORidentifier,
        #        f"fakesensor-color": color,
        #        f"fakesensor-serial": modified_sensor.SENSORserial,
        #        f"fakesensor-x": x,
        #        f"fakesensor-y": y,
        #        f"fakesensor-z": z,
        #    }
        # )

        # Post data and check if the database has been updated
        response = self.client.post("/settings/", data=payload)
        sensor = KnownSensors.objects.get(pk=sensor_pk)

        # Check if plot modification has been applied
        self.assertEqual(sensor.plots.filter(show=True).count(), len(active))

        self.assertEqual(sensor.SENSORidentifier, old_identifer + "abc")
        self.assertEqual(str(sensor.x), x)
        self.assertEqual(str(sensor.y), y)
        self.assertEqual(str(sensor.z), z)
        self.assertEqual(sensor.Color, color)
