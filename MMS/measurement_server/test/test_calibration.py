# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from measurement_server.models.measurement import SHT85
from measurement_server.models.measurement import SHT85results
from measurement_server.models.measurement import Calibration
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import AbstractSensor
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.measurement import Units
from core.modules.sensors import SupportedSensors
from measurement_server.sensor_serializers import SHT85Serializer
import json
from measurement_server.views.calibration import calibrationView


class CalibrationTest(TestCase):
    def setUp(self):
        # use identical setup as in test_models
        md = MeasurementData.objects.create()
        unit = Units.objects.create(qty="humidity", sign="pct")
        sht = SHT85.objects.create(sensor=md)
        sht.units.add(unit)
        sht.save()
        self.sht = sht
        self.unit = unit

    def setUpPass(self):
        pass

    def test_calibration_model_functions(self):
        calib = self.sht.add_calibration(
            x0=-1,
            x1=2,
            x2=3,
            x3=4,
            x4=5,
            x5=6,
            qty=self.unit,
        )
        self.assertEqual(
            "humidity [pct]: cal(x) = 6x**5+5x**4+4x**3+3x**2+2x-1", str(calib)
        )
        calib.delete()
        calib = self.sht.add_calibration(
            x0=-1,
            qty=self.unit,
        )

        self.assertEqual("humidity [pct]: cal(x) = -1", str(calib))

        calib.delete()
        calib = self.sht.add_calibration(
            x5=-1,
            qty=self.unit,
        )
        self.assertEqual("humidity [pct]: cal(x) = -1x**5", str(calib))

    def test_assert_if_only_zero_coeffs_result_in_invalid_calib(self):
        calib = self.sht.add_calibration(
            x0=0, x1=0, x2=0, x3=0, x4=0, x5=0, qty=self.unit
        )
        self.assertTrue(not calib)

    def test_add_calibration_to_sensor_object(self):
        sht = self.sht

        # Generate null calibration
        calib_blank = Calibration()
        unit = sht.units.get(qty="humidity")
        sht.add_calibration(qty=unit)
        sht.save()

        self.assertEqual(sht.calibration.all().count(), 1)
        self.assertIn("No valid", str(calib_blank))
        self.assertEqual(calib_blank.eval(42), 42)

        # Check if raw value is unmodified when no calibration is defined
        SHT85results.objects.create(results=sht, temperature=1)
        self.assertEqual(sht.sht85results_set.last().temperature, 1)

        # Check for clean deletion
        self.assertEqual(sht.calibration.all().count(), 1)
        Calibration.objects.all().delete()
        self.assertEqual(sht.calibration.all().count(), 0)

        # Check if cal(x) = x returns raw values
        sht.add_calibration(qty=unit, x1=1)
        sht.save()
        self.assertEqual(sht.calibration.first().eval(123), 123)

        self.assertTrue(sht.calibration.last())
        SHT85results.objects.create(results=sht, temperature=1, humidity=1)
        self.assertEqual(sht.sht85results_set.last().temperature, 1)
        self.assertEqual(sht.sht85results_set.last().humidity, 1)

        # Delete via sensor model
        self.assertEqual(sht.calibration.all().count(), 1)
        sht.calibration.all().delete()
        self.assertEqual(sht.calibration.all().count(), 0)

        # Update current calibration with complex equation
        sht.add_calibration(x0=0, x1=1.1, x2=2.2, x3=3.3, x4=4.4, x5=5.5, qty=unit)
        sht.save()

        SHT85results.objects.create(results=sht, humidity=1.2, temperature=42)
        self.assertEqual(sht.sht85results_set.last().humidity, 33)
        self.assertEqual(sht.sht85results_set.last().temperature, 42)

        # Check if numbers exceeding the decimal field are rendered None
        SHT85results.objects.create(results=sht, humidity=42)
        self.assertEqual(sht.sht85results_set.last().humidity, None)

    def test_calibration_equation_serializer(self):
        sht = self.sht
        unit = sht.units.get(qty="humidity")
        calib_identity = sht.add_calibration(x1=1, x4=2, qty=unit)

        sht_representation = SHT85Serializer(sht)

        self.assertIn("cal(x) = 2.00x**4+1.00x", str(sht.calibration.last()))

        for f in calib_identity._meta.get_fields():
            if "_calibration" in f.name:
                related_sensors = getattr(calib_identity, f.name).all()
                if "sht" in f.name:
                    self.assertEqual(related_sensors.count(), 1)
                else:
                    self.assertEqual(related_sensors.count(), 0)

    def test_check_if_new_calibration_overwrites_older_with_identical_qty(self):
        # Add calibrations to sensor
        sht = self.sht
        sht.units.all().delete()
        unit_t = Units.objects.create(qty="temperature", sign="°C")
        unit_h = Units.objects.create(qty="humidity", sign="pct")
        sht.units.add(unit_t)
        sht.units.add(unit_h)
        sht.add_calibration(x1=1, qty=unit_t)
        sht.add_calibration(x1=2, qty=unit_h)
        sht.save()

        sensor = KnownSensors()
        sensor.save()
        for calib in sht.calibration.all():
            sensor.calibration.add(calib)
        sensor.save()

        # Check if calibrations were added successfully
        self.assertEqual(sensor.calibration.all().count(), 2)

        # Now change one calibration object
        sensor.add_calibration(x1=1.01, qty=unit_t)
        sensor.save()
        sensor.add_calibration(x2=1.01, qty=unit_t)
        sensor.save()
        sensor.add_calibration(x3=1.01, qty=unit_t)
        sensor.save()

        self.assertEqual(sensor.calibration.all().count(), 2)
        self.assertEqual(float(sensor.calibration.get(qty__qty="temperature").x3), 1.01)

    def test_calibration_api_reachable(self):
        response = self.client.get("/api/calibration/")
        self.assertTrue(response.status_code == 200)
        response = self.client.get("/api/calibration/123/")
        self.assertTrue(response.status_code == 200)

    def test_get_current_calibrations_for_specific_sensor(self):
        unit_t = Units.objects.create(qty="temperature", sign="°C")
        unit_h = Units.objects.create(qty="humidity", sign="pct")

        sn1 = "123456"
        sensor = KnownSensors()
        sensor.save()
        sensor.add_calibration(x1=1, qty=unit_t)
        sensor.add_calibration(x1=2, qty=unit_h)
        sensor.SENSORserial = sn1
        sensor.save()

        sn2 = "1234567"
        sensor = KnownSensors()
        sensor.save()
        sensor.add_calibration(x2=3, qty=unit_t)
        sensor.add_calibration(x2=4, qty=unit_h)
        sensor.SENSORserial = sn2
        sensor.save()

        response = self.client.get(f"/api/calibration/{sn1}/")
        data = response.json()
        self.assertEqual(len(data[sn1]), 2)

        response = self.client.get(f"/api/calibration/{sn2}/")
        data = response.json()
        self.assertEqual(len(data[sn2]), 2)

        response = self.client.get(f"/api/calibration/")
        self.assertListEqual(response.data, [sn1, sn2])

    def test_add_calibration_to_registered_sensor_via_api(self):
        # Post empty string
        resp = self.client.post(
            "/api/calibration/", data="", content_type="text/html"
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("ERROR", resp.keys())

        # Post invalid JSON
        resp = self.client.post(
            "/api/calibration/", "abc", content_type="text/html"
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("ERROR", resp.keys())

        # Post without serial number
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"abc": "abcd"}),
            content_type="application/json",
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("ERROR", resp.keys())

        # Post invalid serial number
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": "abcd"}),
            content_type="application/json",
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("ERROR", resp.keys())

        # Add sensor to DB
        unit_t = Units.objects.create(qty="temperature", sign="°C")
        unit_h = Units.objects.create(qty="humidity", sign="pct")
        sn = "123456"
        sensor = KnownSensors(SENSORserial=sn)
        sensor.save()

        # Post with missing qty
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": "123456"}),
            content_type="application/json",
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("ERROR", resp.keys())

        # Post unknown qty
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": "123456", "qty": "unknown"}),
            content_type="application/json",
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("ERROR", resp.keys())

        # Post with missing parameters
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": "123456", "qty": "temperature"}),
            content_type="application/json",
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("SUCCESS", resp.keys())
        self.assertIn("unregistered", resp["SUCCESS"].lower())

        # Post valid data
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": sn, "x2": 1, "qty": unit_t.qty}),
            content_type="application/json",
        ).json()
        self.assertIsInstance(resp, dict)
        self.assertIn("SUCCESS", resp.keys())
        self.assertEqual(Calibration.objects.all().count(), 1)

        # Post another calibration set
        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": sn, "x0": 1, "qty": unit_h.qty}),
            content_type="application/json",
        )
        self.assertEqual(Calibration.objects.all().count(), 2)
        self.assertEqual(sensor.calibration.count(), 2)

    def test_update_sensor_calibration_via_api(self):
        unit_t = Units.objects.create(qty="temperature", sign="°C")
        sn = "123456"
        sensor = KnownSensors(SENSORserial=sn)
        sensor.save()

        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": sn, "x1": 1, "qty": unit_t.qty}),
            content_type="application/json",
        )
        self.assertEqual(Calibration.objects.all().count(), 1)

        resp = self.client.post(
            "/api/calibration/",
            json.dumps({"SENSORserial": sn, "x2": 1, "qty": unit_t.qty}),
            content_type="application/json",
        )
        self.assertEqual(sensor.calibration.count(), 1)
