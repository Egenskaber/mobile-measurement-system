# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.urls import resolve
from django.test import TestCase, Client
from django.http import HttpRequest

import re
from io import BytesIO
from measurement_server.views import settingsView
from measurement_server.forms import measurementModeForm
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import Measurement
from measurement_server.models.log import Log

from .test_helpers import genMeasurement, mapped_mock_open, mapped_mock_exists


class SettingsPageTest(TestCase):
    tabs = ["Sensors", "Data", "General", "Log", "System", "About"]

    def test_url_resolves_to_settings_page(self):
        found = resolve("/settings/")
        self.assertEqual(found.func, settingsView)

    def test_settings_page_returns_correct_tabs(self):
        request = HttpRequest()
        response = settingsView(request)
        html = response.content.decode("utf8")
        for item in self.tabs:
            self.assertIn(item, html)

    def test_url_resolves_to_settings_tab(self):
        request = HttpRequest()
        response = settingsView(request)
        html = response.content.decode("utf8")

        for item in self.tabs:
            found = resolve("/settings/" + item + "/")
            self.assertEqual(found.func, settingsView)

    def test_active_tab(self):
        c = Client()

        for item in self.tabs:
            item = item.lower()
            response = c.get("/settings/" + item + "/")
            html = response.content.decode("utf8")
            html = html.replace("\n", "")
            self.assertTrue(
                re.search(r'<.*href="#' + item + '".*active.*>', html) is not None
            )
            for _item in self.tabs:
                self.assertTrue(
                    re.search(r'<.*href="#' + _item + '".*active.*>', html) is None
                )

    def test_measurement_settings_form(self):
        form = measurementModeForm()
        self.assertIn("daq_mode", form.fields.keys())
        self.assertIn("spi_mux_mode", form.fields.keys())

    def test_settings_include_mux_switch(self):
        request = HttpRequest()
        response = settingsView(request)
        html = response.content.decode("utf8")
        self.assertIn('<select name="spi_mux_mode" id="id_spi_mux_mode"', html)


from unittest import mock


class DeviceInformationTest(TestCase):
    test_case_files = {
        "/etc/debian_version": "measurement_server/test/test_data/system_information/debian_version",
        "/proc/cpuinfo": "measurement_server/test/test_data/system_information/rpi3_info_01",
        "/etc/os-release": "measurement_server/test/test_data/system_information/os-release/raspbian",
        "./mms_version": "measurement_server/test/test_data/version_information",
    }

    def test_files_are_missing(self):
        # Simulate if the system is stable against missing files
        request = HttpRequest()
        m = mock.MagicMock()
        m.return_value = False
        with mock.patch("os.path.exists", m):
            html = settingsView(request).content.decode("utf8")

    def test_software_information(self):
        request = HttpRequest()
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            with mock.patch("os.path.exists", mapped_mock_exists(["./mms_version"])):
                html = settingsView(request).content.decode("utf8")
                self.assertIn("86af5015d4109f58a96015911312f4d27b88948b", html)
                self.assertIn("86af5015", html)
                self.assertIn("0.2.4", html)
                self.assertNotIn("0.2.4)", html)

        self.test_case_files[
            "./mms_version"
        ] = "measurement_server/test/test_data/version_information_no_tag"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            with mock.patch("os.path.exists", mapped_mock_exists(["./mms_version"])):
                html = settingsView(request).content.decode("utf8")
                self.assertIn("86af5015d4109f58a96015911312f4d27b88948b", html)
                self.assertIn("86af5015", html)
                self.assertIn("unofficial", html)

    def test_delete_log(self):
        self.client.get("/settings/")
        self.assertNotEqual(Log.objects.all().count(), 0)
        response = self.client.post("/settings/", data={"delete_all_log": ""})
        self.assertEqual(Log.objects.all().count(), 0)

    def test_systemsettings(self):
        # Try valid data
        request = HttpRequest()
        request.POST = {
            "systemSettings": "",
            "avg_bucket_size": 10,
            "live_mean": 2,
            "warning_time": 3,
            "max_freq": 4,
            "live_view_table_height": 500,
            "unified_timescale": False,
        }
        request.method = "POST"
        settingsView(request)

        self.assertEqual(Measurement.objects.all().count(), 1)
        state = Measurement.objects.last()
        self.assertEqual(state.avg_bucket_size, 10)
        self.assertEqual(state.live_mean, 2)
        self.assertEqual(state.warning_time, 3)
        self.assertEqual(state.max_freq, 4)
        self.assertEqual(state.live_view_table_height, 500)
        self.assertFalse(state.unified_timescale)

        # Try invalid data of correct type
        request.POST = {
            "systemSettings": "",
            "avg_bucket_size": -10,
            "live_mean": -10,
            "warning_time": -10,
            "max_freq": -10,
            "live_view_table_height": -10,
            "unified_timescale": True,
        }
        request.method = "POST"
        settingsView(request)

        self.assertEqual(Measurement.objects.all().count(), 1)
        state = Measurement.objects.last()

        self.assertEqual(state.avg_bucket_size, 1)
        self.assertEqual(state.live_mean, 1)
        self.assertEqual(state.warning_time, 1)
        self.assertEqual(state.max_freq, -1)
        self.assertEqual(state.live_view_table_height, 100)
        self.assertTrue(state.unified_timescale)

        # Try invalid data of incorrect type
        request.POST = {
            "systemSettings": "",
            "avg_bucket_size": "a",
            "live_mean": "b",
            "warning_time": "c",
            "max_freq": "d",
            "live_view_table_height": "f",
            "unified_timescale": "g",
        }
        request.method = "POST"
        settingsView(request)

        self.assertEqual(Measurement.objects.all().count(), 1)
        state = Measurement.objects.last()

        self.assertEqual(state.avg_bucket_size, 1)
        self.assertEqual(state.live_mean, 1)
        self.assertEqual(state.warning_time, 1)
        self.assertEqual(state.max_freq, -1)
        self.assertEqual(state.live_view_table_height, 100)
        self.assertTrue(state.unified_timescale)

    # def test_daq_mode(self):
    #    request = HttpRequest()
    #    "systemSettings"
    #    self.fail()

    def test_hardware_information(self):
        request = HttpRequest()

        self.test_case_files[
            "/proc/cpuinfo"
        ] = "measurement_server/test/test_data/system_information/rpi3_info_01"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            html = settingsView(request).content.decode("utf8")
            self.assertIn("BCM2835", html)
            self.assertIn("Raspberry Pi 3 Model B Plus Rev 1.3", html)

        self.test_case_files[
            "/proc/cpuinfo"
        ] = "measurement_server/test/test_data/system_information/rpi4_info_01"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            html = settingsView(request).content.decode("utf8")
            self.assertIn("BCM2711", html)
            self.assertIn("Raspberry Pi 4 Model B Rev 1.4", html)

        self.test_case_files[
            "/proc/cpuinfo"
        ] = "measurement_server/test/test_data/system_information/rpi4_info_02"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            html = settingsView(request).content.decode("utf8")
            self.assertIn("BCM2835", html)
            self.assertIn("Raspberry Pi 4 Model B Rev 1.1", html)

        self.test_case_files[
            "/proc/cpuinfo"
        ] = "measurement_server/test/test_data/system_information/desktop_info_01"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            html = settingsView(request).content.decode("utf8")
            self.assertIn("Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz", html)
            self.assertIn("GenuineIntel", html)

        self.test_case_files[
            "/proc/cpuinfo"
        ] = "measurement_server/test/test_data/system_information/desktop_info_02"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            html = settingsView(request).content.decode("utf8")
            self.assertIn("Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz", html)
            self.assertIn("GenuineIntel", html)

        self.test_case_files[
            "/proc/cpuinfo"
        ] = "measurement_server/test/test_data/system_information/desktop_info_03"
        with mock.patch("builtins.open", mapped_mock_open(self.test_case_files)):
            html = settingsView(request).content.decode("utf8")
            self.assertIn("AMD Ryzen 3 3200G with Radeon Vega Graphics", html)
            self.assertIn("AuthenticAMD", html)


class SensorCfgTest(TestCase):
    tabs = ["Sensors", "Data", "General", "Log", "System", "About"]

    def test_sensor_downloads(self):
        mr = genMeasurement()
        response = self.client.post("/settings/", data={"save_sensors": ""})
        csv = BytesIO(response.content)
        sensors = []
        with csv as f:
            for line in f.readlines():
                sensor = line.replace(b"\n", b"")
                sensor = sensor.decode("utf-8")[:-1]
                sensor = sensor.split(",")
                sensors.append(sensor)

        for sensor in sensors:
            self.assertEqual(len(sensor), 7)

        # Check if header is present
        self.assertEqual(sensors[0][0][0], "#")

        # Check if sensor count matches database entries
        sensors = sensors[1:]
        self.assertEqual(KnownSensors.objects.all().count(), len(sensors))

    def test_upload_of_valid_sensor_data(self):
        mr = genMeasurement()

        # Try to upload unchanged sensor export
        response = self.client.post("/settings/", data={"save_sensors": ""})
        self.assertTrue(b"0.00" in response.content)
        response.content = response.content.replace(b"0.00", b"1.11")
        csv = BytesIO(response.content)
        csv.name = "modified_upload.csv"
        response = self.client.post(
            "/settings/",
            data={
                "upload_sensor_csv": "",
                "file": csv,
            },
        )
        response = self.client.post("/settings/", data={"save_sensors": ""})
        self.assertFalse(b"0.00" in response.content)

    def post_sensor_file(self, file_str, filename="modified_upload.csv"):
        csv = BytesIO(file_str)
        csv.name = filename
        response = self.client.post(
            "/settings/",
            data={
                "upload_sensor_csv": "",
                "file": csv,
            },
        )
        return response

    def test_upload_of_unprecise_sensor_data(self):
        self.assertEqual(KnownSensors.objects.all().count(), 0)
        file_str = b"121,Test1,SCD30\t,#ba00c5\t,\r\n"
        file_str += b"122,Test2,SCD30\t,#ba00c5\t,\r\n"
        file_str += b"123,Test3,SCD30\t,#ba00c5\t\r\n"
        file_str += b"124,Test4,SCD30\t,#ba00c5\t,0.00,0.00,0.00\t\r\n"
        file_str += b"125,Test5,SCD30\t,#ba00c5\t,0.00,0.00,0.00\t,\t\r\n"
        response = self.post_sensor_file(file_str, filename="modified_upload.csv")
        self.assertEqual(KnownSensors.objects.all().count(), 5)

    def test_upload_of_invalid_sensor_data(self):
        # Submit empty file
        response = self.post_sensor_file(b"")
        self.assertContains(response, b"The submitted file is empty.")

        # Submit file with incorrect ending
        response = self.post_sensor_file(b"abc", filename="modified_upload.abc")
        self.assertContains(response, b"Expecting .csv.")

        # Check if lines starting with # are ignored
        file_str = b"123,Test1,SCD30,#ba00c5,0.00,0.00,0.00,\n"
        file_str += b"#456,Test2,SCD30,#ba00c5,0.00,0.00,0.00,\n"
        file_str += b"789,Test3,SCD30,#ba00c5,0.00,0.00,0.00,\n"
        response = self.post_sensor_file(file_str, filename="modified_upload.csv")
        self.assertEqual(KnownSensors.objects.all().count(), 2)

        # Check if plot information is assigned to new sensors
        co2 = KnownSensors.objects.filter(SENSORtype="SCD30").first()
        self.assertEqual(co2.plots.all().count(), 3)
        self.assertTrue(co2.plots.filter(qty="co2").exists())
        self.assertTrue(co2.plots.get(qty="co2").show)

        # Check if lines with 4 and 7 columns are supported
        file_str = b"123,Test1,SCD30,#ba00c5,0.00,0.00,0.00,\n"
        file_str += b"124,Test2,SCD30,#ba00c5,0.00,0.00,0.00\n"
        file_str += b"789,Test3,SCD30,#ba00c5,\n"
        file_str += b"790,Test4,SCD30,#ba00c5\n"
        response = self.post_sensor_file(file_str, filename="modified_upload.csv")
        self.assertEqual(KnownSensors.objects.all().count(), 4)

        # Check if incorrect separators are detected
        file_str = b"123,Test1,SCD30,#ba00c5,0.00,0.00,0.00,\n"
        file_str += b"456\tTest2\tSCD30\t#ba00c5\t0.00\t0.00\t0.00\t\n"
        file_str += b"789,Test3,SCD30,#ba00c5,0.00,0.00,0.00,\n"
        response = self.post_sensor_file(file_str, filename="modified_upload.csv")
        self.assertContains(response, b"No column seperator found (expected ,).")

        # Check if too few cols raises errors
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"#Header"
        file_str += b"125,Test1,SCD30,\n"
        file_str += b"126,Test1,SCD30\n"
        file_str += b"#127,Test1,SCD30,123\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Missing information in row 2",
        )

        # Check if too few cols raises errors
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"125,Test1,SCD30,123,123\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Invalid format",
        )

        # Incorrect x
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"123,Test1,SCD30,#ba00c5,#,0.00,0.00,\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Invalid x",
        )

        # Incorrect y
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"123,Test1,SCD30,#ba00c5,0.00,#,0.00,\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Invalid y",
        )

        # Incorrect z
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"123,Test1,SCD30,#ba00c5,0.00,0.00,#\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Invalid z",
        )

        # Incorrect color
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"123,Test1,SCD30,ba00c5,0.00,0.00,0.00\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Incorrect color format",
        )

        n_sensors = KnownSensors.objects.all().count()
        file_str = b"123,Test1,SCD30,#aba00c5,0.00,0.00,0.00\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Color string too short",
        )

        n_sensors = KnownSensors.objects.all().count()
        file_str = b"123,Test1,SCD30,#ga00c5,0.00,0.00,0.00\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Non hex number",
        )

        # Incorrect sensor type
        n_sensors = KnownSensors.objects.all().count()
        file_str = b"1234,Test1,ABC11,#aa00c5,0.00,0.00,0.00\n"
        response = self.post_sensor_file(file_str)
        self.assertContains(
            response,
            b"Unknown sensor type",
        )

        # Check if sensor count has not changed
        self.assertEqual(KnownSensors.objects.all().count(), n_sensors)

        # Check if files larger than 1MB raise an error
        response = self.post_sensor_file(
            b"a" * (2000**2 + 1), filename="modified_upload.csv"
        )
        self.assertContains(response, b"File too large")
