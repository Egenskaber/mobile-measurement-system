# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
This extends djangos template tags accessible in the
html templates used in this app.
"""

from django import template
from measurement_server.modules.unit_names import unit_names

register = template.Library()


@register.filter("linebreak")
def linebreak(n, m):
    offset = 3
    n = n + offset
    if n % m == 0:
        return True
    return False


@register.filter
def divide(value, arg):
    try:
        return int(int(value) / int(arg))
    except (ValueError, ZeroDivisionError):
        return None


@register.filter
def get_unit_name(object):
    if object.name in unit_names.keys():
        return str(unit_names[object.name])
    return None
