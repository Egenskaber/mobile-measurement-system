# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from rest_framework import pagination, serializers

from .models.measurement import MeasurementData

from .models.measurement import ADS1015
from .models.measurement import ADS1015results
from .models.measurement import BME280
from .models.measurement import BME280results
from .models.measurement import MAX31865
from .models.measurement import MAX31865results
from .models.measurement import NAU7802
from .models.measurement import NAU7802results
from .models.measurement import SCD30
from .models.measurement import SCD30results
from .models.measurement import SCD41
from .models.measurement import SCD41results
from .models.measurement import SDP8
from .models.measurement import SDP8results
from .models.measurement import SFM
from .models.measurement import SFMresults
from .models.measurement import SHT85
from .models.measurement import SHT85results
from .models.measurement import S8000
from .models.measurement import S8000results
from .models.measurement import Optidew
from .models.measurement import Optidewresults
from .models.measurement import SPS30
from .models.measurement import SPS30results
from .models.measurement import SMGair2
from .models.measurement import SMGair2results
from .models.measurement import PlotSettings

from .models.measurement import Units
from .models.measurement import Calibration

SENSOR_FIELDS = [
    "MUXaddress",
    "MUXport",
    "COMport",
    "SENSORtype",
    "SENSORserial",
    "SENSORidentifier",
    "FTDIserial",
    "Color",
    "units",
    "x",
    "y",
    "z",
    "results",
    "calibration",
]


class PlotSerializer(serializers.ModelSerializer):
    """Collection of all registered sensor identifier."""

    class Meta:
        model = PlotSettings
        exclude = ["id"]


class UnitSerializer(serializers.ModelSerializer):
    """Collection of all registered sensor identifier."""

    class Meta:
        model = Units
        exclude = ["id"]
        read_only = "__all__"


# Serializers for measurement results of specific sensors


class CalibrationSerializer(serializers.ModelSerializer):
    """Calibration parameters"""

    class Meta:
        model = Calibration
        fields = "__all__"


class SensorSerializer(serializers.ModelSerializer):
    results = serializers.SerializerMethodField("paginated_results")
    units = UnitSerializer(many=True, read_only=True)
    calibration = CalibrationSerializer(many=True, read_only=False)

    class Meta:
        model = None
        fields = SENSOR_FIELDS
        read_only = fields

    def __init__(self, *args, **kwargs):
        no_res = kwargs.pop("no_res", None)
        super().__init__(*args, **kwargs)
        if no_res:
            self.fields.pop("results")


class SHT85resultsSerializer(serializers.ModelSerializer):
    """Results of SHT sensors"""

    class Meta:
        model = SHT85results
        fields = ["time", "humidity", "temperature"]
        read_only = "__all__"


class ADS1015resultsSerializer(serializers.ModelSerializer):
    """Results of ADS1015 sensors"""

    class Meta:
        model = ADS1015results
        fields = ["time", "dU01"]
        read_only = fields


class MAX31865resultsSerializer(serializers.ModelSerializer):
    """Results of MAX31865 sensors"""

    class Meta:
        model = MAX31865results
        fields = ["time", "temperature"]
        read_only = fields


class SDP8resultsSerializer(serializers.ModelSerializer):
    """Results of SDP8 sensors"""

    class Meta:
        model = SDP8results
        fields = ["time", "diff_pressure", "temperature"]
        read_only = fields


class SCD30resultsSerializer(serializers.ModelSerializer):
    """Results of SCD30 sensors"""

    class Meta:
        model = SCD30results
        fields = ["time", "co2", "temperature", "humidity"]
        read_only = fields


class SCD41resultsSerializer(serializers.ModelSerializer):
    """Results of SCD41 sensors"""

    class Meta:
        model = SCD41results
        fields = ["time", "co2", "temperature", "humidity"]
        read_only = fields


class NAU7802resultsSerializer(serializers.ModelSerializer):
    """Results of NAU7802 sensors"""

    class Meta:
        model = NAU7802results
        fields = ["time", "adc"]
        read_only = fields


class BME280resultsSerializer(serializers.ModelSerializer):
    """Results of BME280 sensors"""

    class Meta:
        model = BME280results
        fields = ["time", "abs_pressure", "humidity", "temperature"]
        read_only = fields


class SMGair2resultsSerializer(serializers.ModelSerializer):
    """Results of SMGair2 sensors"""

    class Meta:
        model = SMGair2results
        fields = [
            "time",
            "abs_pressure",
            "density",
            "temperature",
            "volumeflow",
        ]
        read_only = fields


class S8000resultsSerializer(serializers.ModelSerializer):
    """Results of S8000 sensors"""

    class Meta:
        model = S8000results
        fields = [
            "time",
            "dewpoint",
            "temperature",
        ]
        read_only = fields


class OptidewresultsSerializer(serializers.ModelSerializer):
    """Results of Optidew sensors"""

    class Meta:
        model = Optidewresults
        fields = [
            "time",
            "dewpoint",
            "temperature",
            "humidity",
            "state",
        ]
        read_only = fields


class SPS30resultsSerializer(serializers.ModelSerializer):
    """Results of SPS30 sensors"""

    class Meta:
        model = SPS30results
        fields = [
            "time",
            "m_03_to_1_0_mu",
            "m_03_to_2_5_mu",
            "m_03_to_4_0_mu",
            "m_03_to_10_0_mu",
            "n_03_to_0_5_mu",
            "n_03_to_1_0_mu",
            "n_03_to_2_5_mu",
            "n_03_to_4_0_mu",
            "n_03_to_10_0_mu",
            "tps",
        ]
        read_only = fields


class SFMresultsSerializer(serializers.ModelSerializer):
    """Results of SFM sensors"""

    class Meta:
        model = SFMresults
        fields = ["time", "volumeflow"]
        read_only = fields


# Serializers for sensors


class ADS1015Serializer(SensorSerializer):
    class Meta:
        model = ADS1015
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = ADS1015results.objects.filter(results_id=obj)
        serializer = ADS1015resultsSerializer(
            obj.ads1015results_set.all(), many=True, read_only=True
        )

        return serializer.data


class MAX31865Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = MAX31865
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = MAX31865results.objects.filter(results_id=obj)
        serializer = MAX31865resultsSerializer(
            obj.max31865results_set.all(), many=True, read_only=True
        )

        return serializer.data


class SDP8Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = SDP8
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SDP8results.objects.filter(results_id=obj)
        serializer = SDP8resultsSerializer(
            obj.sdp8results_set.all(), many=True, read_only=True
        )

        return serializer.data


class SCD30Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = SCD30
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SCD30results.objects.filter(results_id=obj)
        serializer = SCD30resultsSerializer(
            obj.scd30results_set.all(), many=True, read_only=True
        )

        return serializer.data


class SCD41Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = SCD41
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SCD41results.objects.filter(results_id=obj)
        serializer = SCD41resultsSerializer(
            obj.scd41results_set.all(), many=True, read_only=True
        )

        return serializer.data


class NAU7802Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = NAU7802
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = NAU7802results.objects.filter(results_id=obj)
        serializer = NAU7802resultsSerializer(
            obj.nau7802results_set.all(), many=True, read_only=True
        )

        return serializer.data


class BME280Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = BME280
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = BME280results.objects.filter(results_id=obj)
        serializer = BME280resultsSerializer(
            obj.bme280results_set.all(), many=True, read_only=True
        )

        return serializer.data


class S8000Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = S8000
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = S8000results.objects.filter(results_id=obj)
        serializer = S8000resultsSerializer(
            obj.s8000results_set.all(), many=True, read_only=True
        )

        return serializer.data


class OptidewSerializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = Optidew
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = Optidewresults.objects.filter(results_id=obj)
        serializer = OptidewresultsSerializer(
            obj.optidewresults_set.all(), many=True, read_only=True
        )

        return serializer.data


class SPS30Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = SPS30
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SPS30results.objects.filter(results_id=obj)
        serializer = SPS30resultsSerializer(
            obj.sps30results_set.all(), many=True, read_only=True
        )

        return serializer.data


class SMGair2Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = SMGair2
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SMGair2results.objects.filter(results_id=obj)
        serializer = SMGair2resultsSerializer(
            obj.smgair2results_set.all(), many=True, read_only=True
        )

        return serializer.data


class SFMSerializer(SensorSerializer):
    class Meta:
        model = SFM
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SFMresults.objects.filter(results_id=obj)
        serializer = SFMresultsSerializer(
            obj.sfmresults_set.all(), many=True, read_only=True
        )

        return serializer.data


class SHT85Serializer(SensorSerializer):
    results = serializers.SerializerMethodField("paginated_results")

    class Meta:
        model = SHT85
        fields = SENSOR_FIELDS
        readonly = fields

    def paginated_results(self, obj):
        results = SHT85results.objects.filter(results_id=obj)
        serializer = SHT85resultsSerializer(
            obj.sht85results_set.all(), many=True, read_only=True
        )
        return serializer.data


# Data serializers


class MeasurementDataSHT85Serializer(serializers.ModelSerializer):
    sht = SHT85Serializer(source="sht85_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["sht"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.sht85_set.count() != 0:
            data = data["sht85"]
        return data


class MeasurementDataSCD30Serializer(serializers.ModelSerializer):
    scd30 = SCD30Serializer(source="scd30_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["scd30"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.scd30_set.count() != 0:
            data = data["scd30"]
        return data


class MeasurementDataSCD41Serializer(serializers.ModelSerializer):
    scd30 = SCD41Serializer(source="scd41_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["scd41"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.scd41_set.count() != 0:
            data = data["scd41"]
        return data


class MeasurementDataBME280Serializer(serializers.ModelSerializer):
    bme280 = BME280Serializer(source="bme280_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["bme280"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.bme280_set.count() != 0:
            data = data["bme280"]
        return data


class MeasurementDataSMGair2Serializer(serializers.ModelSerializer):
    smgair2 = SMGair2Serializer(source="smgair2_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["smgair2"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.smgair2_set.count() != 0:
            data = data["smgair2"]
        return data


class MeasurementDataS8000Serializer(serializers.ModelSerializer):
    s8000 = S8000Serializer(source="s8000_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["s8000"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.s8000.count() != 0:
            data = data["s8000"]
        return data


class MeasurementDataOptidewSerializer(serializers.ModelSerializer):
    optidew = OptidewSerializer(source="optidew_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["optidew"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.optidew.count() != 0:
            data = data["optidew"]
        return data


class MeasurementDataSPS30Serializer(serializers.ModelSerializer):
    sps30 = SPS30Serializer(source="sps30_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["sps30"]
        readonly = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.sps30_set.count() != 0:
            data = data["sps30"]
        return data


class MeasurementDataSFMSerializer(serializers.ModelSerializer):
    sfm = SFMSerializer(source="sfm_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["sfm"]
        read_only = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.sfm_set.count() != 0:
            data = data["sfm"]
        return data


class MeasurementDataADS1015Serializer(serializers.ModelSerializer):
    ads1015 = ADS1015Serializer(source="ads1015_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["ads1015"]
        read_only = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.ads1015_set.count() != 0:
            data = data["ads1015"]
        return data


class MeasurementDataNAU7802Serializer(serializers.ModelSerializer):
    nau7802 = NAU7802Serializer(source="nau7802_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["nau7802"]
        read_only = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.nau7802_set.count() != 0:
            data = data["nau7802"]
        return data


class MeasurementDataMAX31865Serializer(serializers.ModelSerializer):
    max31865 = MAX31865Serializer(source="max31865_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["max31865"]
        read_only = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.max31865_set.count() != 0:
            data = data["max31865"]
        return data


class MeasurementDataSDP8Serializer(serializers.ModelSerializer):
    sdp8 = SDP8Serializer(source="sdp8_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = ["sdp8"]
        read_only = fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.sdp8_set.count() != 0:
            data = data["sdp8"]
        return data
