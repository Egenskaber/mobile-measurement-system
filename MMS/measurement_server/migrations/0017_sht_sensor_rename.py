# Generated by Django 2.2.1 on 2022-04-14 20:26

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("measurement_server", "0016_versioninfo_nctimescale"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="SHT",
            new_name="SHT85",
        ),
        migrations.RenameModel(
            old_name="SHTresults",
            new_name="SHT85results",
        ),
    ]
