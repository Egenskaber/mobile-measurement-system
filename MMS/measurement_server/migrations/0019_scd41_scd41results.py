# Generated by Django 4.2.2 on 2023-07-20 09:24

import colorful.fields
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("measurement_server", "0018_optidew_optidewresults"),
    ]

    operations = [
        migrations.CreateModel(
            name="SCD41",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("FTDIserial", models.CharField(default="", max_length=60)),
                ("MUXaddress", models.IntegerField(default=0)),
                ("MUXport", models.IntegerField(default=0)),
                ("COMport", models.CharField(default="", max_length=60)),
                ("CS", models.IntegerField(default=0)),
                ("SENSORtype", models.CharField(default="unk", max_length=20)),
                ("SENSORserial", models.CharField(default="", max_length=20)),
                ("Color", colorful.fields.RGBColorField(default="#000000")),
                ("SENSORidentifier", models.CharField(default="", max_length=220)),
                ("connected", models.BooleanField(default=False)),
                ("x", models.DecimalField(decimal_places=2, default=0, max_digits=7)),
                ("y", models.DecimalField(decimal_places=2, default=0, max_digits=7)),
                ("z", models.DecimalField(decimal_places=2, default=0, max_digits=7)),
                (
                    "serial_mode",
                    models.CharField(
                        choices=[("SPI", "SPI"), ("I2C", "I2C"), ("COM", "COM")],
                        default="",
                        max_length=3,
                    ),
                ),
                (
                    "plots",
                    models.ManyToManyField(
                        blank=True, to="measurement_server.plotsettings"
                    ),
                ),
                (
                    "sensor",
                    models.ForeignKey(
                        default=1,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="measurement_server.measurementdata",
                    ),
                ),
                (
                    "units",
                    models.ManyToManyField(blank=True, to="measurement_server.units"),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="SCD41results",
            fields=[
                (
                    "time",
                    models.DateTimeField(
                        default=django.utils.timezone.now,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "temperature",
                    models.DecimalField(
                        decimal_places=2, default=None, max_digits=5, null=True
                    ),
                ),
                (
                    "humidity",
                    models.DecimalField(
                        decimal_places=2, default=None, max_digits=5, null=True
                    ),
                ),
                (
                    "co2",
                    models.DecimalField(
                        decimal_places=0, default=None, max_digits=5, null=True
                    ),
                ),
                (
                    "results",
                    models.ForeignKey(
                        default=1,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="measurement_server.scd41",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        # SCD41
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_scd41results', 'time');"
        ),
    ]
