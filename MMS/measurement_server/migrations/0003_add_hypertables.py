# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [("measurement_server", "0002_sensors")]

    operations = [
        # SPS30
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_sps30results', 'time');"
        ),
        # SMGair2
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_smgair2results', 'time');"
        ),
        # SDP8
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_sdp8results', 'time');"
        ),
        # SHT85
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_shtresults', 'time');"
        ),
        # SFM3000
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_sfmresults', 'time');"
        ),
        # ADS1015
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_ads1015results', 'time');"
        ),
        # SCD30
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_scd30results', 'time');"
        ),
        # BME280
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_bme280results', 'time');"
        ),
        # NAU7802
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_nau7802results', 'time');"
        ),
        # MAX31865
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_max31865results', 'time');"
        ),
        # S8000 dew point mirror
        migrations.RunSQL(
            "SELECT create_hypertable('measurement_server_s8000results', 'time');"
        ),
    ]
