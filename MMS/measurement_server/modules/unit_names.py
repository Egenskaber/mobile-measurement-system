# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

unit_names = {
    "co2": "CO<sub>2</sub>",
    "m_03_to_1_0_mu": "m(0.3-1&#x1D707m)",
    "m_03_to_2_5_mu": "m(0.3-2.5&#x1D707m)",
    "m_03_to_4_0_mu": "m(0.3-4&#x1D707m)",
    "m_03_to_10_0_mu": "m(0.3-10&#x1D707m)",
    "n_03_to_0_5_mu": "N(0.3-0.5&#x1D707m)",
    "n_03_to_1_0_mu": "N(0.3-1&#x1D707m)",
    "n_03_to_2_5_mu": "N(0.3-2.5&#x1D707m)",
    "n_03_to_4_0_mu": "N(0.3-4&#x1D707m)",
    "n_03_to_10_0_mu": "N(0.3-10&#x1D707m)",
    "temperature": "Temperature",
    "humidity": "Humidity",
    "abs_pressure": "Abs. pressure",
    "diff_pressure": "Diff. pressure",
    "tps": "Particle size",
    "adc": "ADC counts",
    "volumeflow": "Volume flow",
    "dewpoint": "Dewpoint",
}
