# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Read system information to dictionaries
"""

import os
import re


def software_information() -> dict[str, str]:
    """
    Read software version identifier and return an information dictionairy
    """
    valid_filenames = [
        "./mms_version",
        "MMS/measurement_server/test/test_data/version_information",
    ]

    commit = "Unknown"
    software_info = {
        "commit_short": "Unknown",
        "commit_full": "Unknown",
        "tag": "Unknown",
        "commit_author": "Unknown",
    }
    for filename in valid_filenames:
        if os.path.exists(filename):
            with open(filename, "r") as f:
                lines = f.readlines()
                commit_full = lines[0].replace("commit ", "")
                commit_full = commit_full.split(" ")[0]
                commit_short = commit_full[:8]
                if "tag: " in lines[0]:
                    tag = lines[0].split("tag: ")[-1]
                    tag = tag.replace("MMS2-", "")
                    tag = tag.replace(")\n", "")
                else:
                    tag = "unofficial"

                author = lines[1].replace("Author: ", "")

            software_info["commit_short"] = commit_short
            software_info["commit_full"] = commit_full
            software_info["commit_author"] = author
            software_info["tag"] = tag
            break
    return software_info


def hardware_information() -> dict[str, str]:
    filename_os_release = "/etc/os-release"
    filename_debian_version = "/etc/debian_version"
    filename_cpuinfo = "/proc/cpuinfo"
    filename_hostname = "/etc/hostname"

    # Operating mashine information
    hardware_info = {
        "tag": "Unknown",
        "hostname": "Unknown",
        "PRETTY_NAME": "Unknown",
        "debian_version": "Unknown",
        "last_commit": "Unknown",
        "device": {"cpu": "Unknown", "type": "Unknown"},
    }

    filename = filename_os_release
    if os.path.exists(filename):
        with open(filename, "r") as f:
            for line in f.readlines():
                key, data = line.rstrip().split("=")
                hardware_info[key] = data.replace('"', "")
    else:
        hardware_info["PRETTY_NAME"] = f"No {filename} file provided."

    filename = filename_debian_version
    if os.path.exists(filename):
        version = open(filename).readline()
        hardware_info["version"] = version.rstrip()
    else:
        hardware_info["version"] = f"No {filename} file provided."

    filename = filename_hostname
    if os.path.exists(filename):
        hostname = open(filename).readline()
        hardware_info["hostname"] = hostname.rstrip()
    else:
        hardware_info["hostname"] = f"No {filename} file provided."

    filename = filename_cpuinfo
    if os.path.exists(filename):
        with open(filename, "r") as f:
            for line in f.readlines():
                entry = line.split(":")
                if len(entry) == 2:
                    key = cut_whitespace(entry[0])
                    data = cut_whitespace(entry[1])
                    if key == "Hardware" or key == "model name":
                        hardware_info["device"]["cpu"] = data
                    if key == "vendor_id" or key == "Model":
                        hardware_info["device"]["type"] = data
    else:
        for key in hardware_info["device"].keys():
            hardware_info["device"][key] = f"No {filename} file provided."

    return hardware_info


def cut_whitespace(s: str, forbidden_char="\n\t "):
    if len(s) == 0:
        return ""
    if s[-1] in forbidden_char:
        return cut_whitespace(s[:-1], forbidden_char=forbidden_char)
    elif s[0] in forbidden_char:
        return cut_whitespace(s[1:], forbidden_char=forbidden_char)
    else:
        return s
