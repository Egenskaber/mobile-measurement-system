# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Holds dataExport class with respective export functions for exporting measurement data
in supported file formats and generating zip files for each file format
"""


from zipfile import ZipFile
import csv
import json
import os
import glob

import netCDF4
import settings
from core.modules.sensors import SupportedSensors
from netCDF4 import Dataset
from datetime import datetime
from datetime import timedelta
from pytz import timezone
import numpy as np

from ..models.log import Log


class dataExport:
    """
    Convert stored measurement data with respective function to selected file format
    """

    export_formats = [
        {"extension": "csv", "name": "CSV"},
        {"extension": "json", "name": "JSON"},
        {"extension": "nc", "name": "NETCDF"},
    ]

    def __init__(self, data, file_format: str = "csv", unified_timescale: bool = False):
        """
        Check passed file format and generate zip file

        Args:
            data : Object generated from MeasurementData
            file_format (string) : Passed file format
            unified_timescale (bool): Inidicates if a common timescale shall be computed (currently only supported for netCDF)

        Returns
            list: output filenames

        """

        # check if passed file format is supported
        supported_formats = list(map(lambda d: d["extension"], self.export_formats))

        if file_format not in supported_formats:
            raise ValueError(f"'{file_format}' is not supported")

        if file_format == "csv":
            files = self.prepareCSV(data)
        elif file_format == "json":
            files = self.JSON(data)
        elif file_format == "nc":
            files = self.netCDF(data, unified_timescale=unified_timescale)

        self.zip_file = self.generateZip(files, data.name)

    @staticmethod
    def generateZip(files, filename):
        """
        Compress a list of files into a zip archive with a given filename

        Args:
            files (list) : List with files generated from export functions
            filename : Generated filename and path

        Returns:
            zip_filename : Generated zip file

        """

        zip_filename = settings.MEDIA_ROOT + "/results/" + filename + ".zip"

        with ZipFile(zip_filename, "w") as zipfile:
            for file in files:
                zipfile.write(file, arcname=os.path.basename(os.path.abspath(file)))

        return zip_filename

    @classmethod
    def contains_measurement_values(cls, data):
        """
        Check if MeasurementData objects contains measurement values

        Args:
            data (measurement_server.models.measurement.MeasurementData): Measurement result entry

        Returns:
            boolean True if measurement values are included in Measurementdata entry, False otherwise
        """
        for sensor_type in SupportedSensors:
            sensor_res_cls_name = sensor_type.lower()
            for sensor in getattr(data, f"{sensor_res_cls_name}_set").all():
                results = getattr(sensor, f"{sensor_res_cls_name}results_set").all()
                if results.all().count() != 0:
                    return True
        return False

    @classmethod
    def netCDF(cls, data, unified_timescale: bool = False):
        """
        Generates NetCDF files for each sensor associated with the
        MeasurementData object data

        Args:
            data : Object generated from MeasurementData
            unified_timescale (bool) : Option to unify results for sensor types on a common time axis

        Returns:
            files : Generated NetCDF files

        """

        # Generates a nc file for each sensor.
        files = []
        filename = f"{settings.MEDIA_ROOT }/results/{data.name}.nc"

        # Datetime string format
        fmt = "%Y-%m-%d %H:%M:%S.%f"

        # Time of file creation
        t0 = datetime.now().astimezone(timezone("Europe/Berlin"))
        export_time_str = t0.strftime(fmt)

        # Start/stop time of the measurement
        start_measure = data.start_measure
        stop_measure = data.stop_measure
        start_time_str = start_measure.strftime(fmt)
        stop_time_str = stop_measure.strftime(fmt)

        # Measurement duration
        duration_str = (
            f"{(stop_measure - start_measure).total_seconds()/60:0.1f} minutes"
        )

        # Create header for rootgrp
        rootgrp = Dataset(filename, "w", format="NETCDF4")
        rootgrp.description = data.name
        rootgrp.history = f"Time of export on {export_time_str}"
        rootgrp.start = f"Measurement started on {start_time_str}"
        rootgrp.stop = f"Measurement stopped on {stop_time_str}"
        rootgrp.duration = duration_str
        rootgrp.mms_version = "0.0.0"
        rootgrp.source = "MMS 0.0.0"
        rootgrp.mms_commit = "0fa3ddd"

        # Global dimensions available for all sensor types
        time = rootgrp.createDimension("time", None)
        calibration = rootgrp.createDimension("calibration", None)

        if not cls.contains_measurement_values(data):
            rootgrp.close()
            return [filename]

        # Get sample frequency, measurement start and stop
        max_freq = 0
        min_freq = 1e5
        t0 = None
        t1 = None
        for sensor_type in SupportedSensors:
            sensor_res_cls_name = sensor_type.lower()
            for sensor in getattr(data, f"{sensor_res_cls_name}_set").all():
                results = getattr(sensor, f"{sensor_res_cls_name}results_set").all()
                if results.all().count() < 2:
                    continue
                _t0 = results.first().time
                if t0 is None or t0 > _t0:
                    t0 = _t0

                _t1 = results.last().time
                if t1 is None or t1 < _t1:
                    t1 = _t1
                n = results.all().count()
                dt = (t1 - t0) / n
                f = 1.0 / dt.total_seconds()
                if f > max_freq:
                    max_freq = f
                if f < min_freq:
                    min_freq = f

        if t0 is None:
            unified_timescale = False
            Log(
                message="No unified timescale possible due to missing data.",
                typ="Error",
            ).save()

        if min_freq < 0.8 * max_freq:
            unified_timescale = False
            log = Log(
                message=f"Variations of measurement frequencies is too high. No automatic unified timescale possible. (min/max sample rate: {min_freq:0.2f}/{max_freq:0.2f})",
                typ="Error",
            ).save()

        if unified_timescale:
            common_time = np.arange(0, (t1 - t0).total_seconds() + 1.0 / f, 1.0 / f)
            time = rootgrp.createVariable("time", "f8", ("time",))
            time.units = "seconds since " + t0.strftime("%Y-%m-%d %H:%M:%s.%f%z")
            time.standard_name = "t"
            time.long_name = "time"
            time[:] = common_time

        # Loop over all supported sensors
        for sensor_type in SupportedSensors:
            # Temporary lists for storing current sensor/unit names from result_data
            # and creating sensor type groups and unit dimensions for each group
            temp_sensor_list = []
            temp_unit_list = []

            # Get serializers for the present sensor type
            sensor_serializer = SupportedSensors[sensor_type]["sensor_serializer"]
            sensor_res_cls_name = sensor_type.lower()

            # Generate result file to create sensor groups and unit dimensions
            for sensor in getattr(data, f"{sensor_res_cls_name}_set").all():
                result_data = sensor_serializer(sensor).data

                # Store used sensors
                if result_data["SENSORtype"] not in temp_sensor_list:
                    temp_sensor_list.append(result_data["SENSORtype"])

                # Store units for used sensors
                for i in range(len(result_data["units"])):
                    if result_data["units"][i]["qty"] not in temp_unit_list:
                        temp_unit_list.append(result_data["units"][i]["qty"])

            # Create group for each used sensor type
            for sensor in temp_sensor_list:
                sensortype_grp = rootgrp.createGroup(sensor.upper())

            # Create unit dimensions for each used sensor type
            for unit in temp_unit_list:
                sensortype_grp.createDimension(unit, None)

            # Generate result file for a single sensor of type sensor_type
            for sensor in getattr(data, f"{sensor_res_cls_name}_set").all():
                results = getattr(sensor, f"{sensor_res_cls_name}results_set").all()
                # Create sensor headers
                s = sensortype_grp.createGroup(
                    sensor.SENSORtype + "_" + sensor.SENSORserial
                )
                s.identifier = sensor.SENSORidentifier
                s.ftdi = sensor.FTDIserial
                s.comport = sensor.COMport
                s.serialnumber = sensor.SENSORserial
                s.muxadress = sensor.MUXaddress
                s.muxport = sensor.MUXport
                s.x = float(sensor.x)
                s.y = float(sensor.y)
                s.z = float(sensor.z)

                if not unified_timescale:
                    # Create time
                    time = s.createVariable("time", "f8", ("time",))
                    time.units = "seconds since " + t0.strftime(
                        "%Y-%m-%d %H:%M:%S.%f%z"
                    )
                    time.standard_name = "t"
                    time.long_name = "time"
                    time[:] = None

                # Create units and results for each sensor
                # Loop over all supported units for each sensor, then create units for each sensor
                for unit in sensor.units.all():
                    # tmp = temporary variable for creating netCDF variables for each sensor
                    tmp = s.createVariable(unit.qty, "f4", ("time",))
                    tmp.units = unit.sign
                    tmp.absolute_uncertainty = 0
                    tmp.max_value_rel_uncertainty = 0
                    tmp.measurement_value_rel_uncertainty = 0
                    tmp.standard_name = unit.qty[:3]
                    tmp.long_name = unit.qty
                    tmp[:] = None

                    # Loop over all results for each sensor
                    sensor_time = list(results.values_list("time", flat=True))
                    sensor_values = list(results.values_list(unit.qty, flat=True))

                    for i in range(results.count()):
                        if sensor_values[i] is not None:
                            sensor_values[i] = float(sensor_values[i])
                        sensor_time[i] = (sensor_time[i] - t0).total_seconds()

                    if unified_timescale:
                        tmp[:] = np.interp(common_time, sensor_time, sensor_values)
                    else:
                        tmp[:] = sensor_values
                        time[:] = sensor_time

        rootgrp.close()

        ## check for invalid sensor frequencies
        ## print(nt, _nt)
        # if _nt < 0.01 * nt or nt < 0.01 * _nt:
        #    raise ValueError(
        #        "Insufficient data for time averaging. Use raw export instead!"
        #    )

        files.append(filename)
        return files

    @staticmethod
    def JSON(data):
        """
        Generates json files for each sensor associated with the
        MeasurementData object data

        Args:
            data : Object generated from MeasurementData

        Returns:
            files: Generated json files

        """

        # Generates a json file for each sensor.
        files = []

        # Loop over all supported sensors
        for sensor_type in SupportedSensors:
            # Get serializers for the present sensor type

            sensor_serializer = SupportedSensors[sensor_type]["sensor_serializer"]
            sensor_res_cls_name = sensor_type.lower()

            # Generate result file for a single sensor of type sensor_type
            for sensor in getattr(data, f"{sensor_res_cls_name}_set").all():
                result_data = sensor_serializer(sensor).data

                filename = f"{settings.MEDIA_ROOT }/results/{data.name}_{sensor.SENSORtype}_{sensor.SENSORserial}.json"
                with open(filename, "w") as json_file:
                    json_file.write(json.dumps(result_data))
                files.append(filename)
        return files

    def prepareCSV(self, data):
        """
        Generate csv files for each sensor associated with the
        MeasurementData object data

        Args:
            data: Object generated from MeasurementData

        Returns:
            files: Generated csv files

        """

        files = []  # All created sensor result files

        # Generate a csv file for each sensor.
        for sensor_type in SupportedSensors:
            # Get serializers for the present sensor type

            sensor_serializer = SupportedSensors[sensor_type]["sensor_serializer"]
            sensor_res_cls_name = sensor_type.lower()

            for sensor in getattr(data, f"{sensor_res_cls_name}_set").all():
                base_directory = f"{settings.MEDIA_ROOT}/results/{data.id}"
                result_directory = (
                    f"{base_directory}/{sensor_type}/{sensor.SENSORserial}/"
                )

                result_files = glob.glob(result_directory + "*[0-9].json")
                result_files.sort()
                result_data = {}
                for field in sensor._meta.get_fields():
                    key = field.name
                    if key == "units":
                        result_data[key] = []
                        for unit in sensor.units.all():
                            result_data[key].append(
                                {"qty": unit.qty, "sign": unit.sign}
                            )
                    # elif key == "calibration":
                    #    result_data[key] = []
                    #    for calib in sensor.calibrations.all():
                    #        result_data[key].append(
                    #            {"qty": unit.qty, "sign": unit.sign}
                    #        )

                    elif "results" in key:
                        result_data["results"] = []
                    else:
                        result_data[key] = getattr(sensor, key)

                result_data["results"] = sensor_serializer(sensor).data["results"]

                files.append(self.storeCSV(result_data, data))

        return files

    def storeCSV(self, sensor, data):
        """
        Store generated csv files

        Args:
            sensor : Sensors from SupportedSensors
            data : Object generated from MeasurementData

        Returns:
            sensor_filename : Generated filename and path for sensor

        """

        sensor_filename = (
            settings.MEDIA_ROOT
            + "/results/"
            + data.name
            + "_"
            + sensor["SENSORtype"]
            + "_"
            + sensor["SENSORserial"]
            + ".csv"
        )

        measurement_header = [
            "name",
            "created",
            "start_measure",
            "stop_measure",
            "stopped",
        ]

        sensor_header = [
            "SENSORtype",
            "SENSORidentifier",
            "SENSORserial",
            "MUXaddress",
            "MUXport",
            "x",
            "y",
            "z",
            # "calibrations",
        ]

        with open(sensor_filename, "w") as out:
            writer = csv.writer(out)

            for info in measurement_header:
                writer.writerow(["# ", info, getattr(data, info)])

            for info in sensor_header:
                writer.writerow(["# ", info, sensor[info]])

            keys = list(sensor["results"][0].keys())
            data_keys = ["# Time"]
            for key in keys:
                for unit in sensor["units"]:
                    if unit["qty"] == key:
                        data_keys.append(key + f"[{unit['sign']}]")

            writer.writerow(data_keys)

            for data in sensor["results"]:
                value = []
                for key in keys:
                    value.append(data[key])
                writer.writerow(value)

        return sensor_filename
