# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from ..models.measurement import KnownSensors
from ..models.measurement import Units
from ..models.measurement import PlotSettings
from core.modules.sensors import SupportedSensors


def isHex(string):
    for c in string:
        if (ord(c) < ord("0") or ord(c) > ord("9")) and (
            ord(c) < ord("A") or ord(c) > ord("F")
        ):
            return False
    return True


def get_all_referenced_units():
    unit_ids = KnownSensors.objects.filter(connected=True).values_list("units")
    deduplicated_unit_ids = []
    for unit_id in unit_ids:
        if unit_id not in deduplicated_unit_ids:
            deduplicated_unit_ids.append(unit_id[0])
    units = Units.objects.filter(pk__in=deduplicated_unit_ids)
    return units


def get_sensor_counts(data_query):
    out = {}
    for device in SupportedSensors:
        count = getattr(data_query, device.lower() + "_set").count()
        if device.lower() == "sht85":
            out["sht"] = str(count)
        else:
            out[device.lower()] = str(count)
    return out


def get_sensor_plot_information(ignore_plot_show=False):
    sensors = KnownSensors.objects.all().values_list("SENSORserial", "plots")

    serials = []
    plots = []

    for sensor in sensors:
        serials.append(sensor[0])
        plots.append(sensor[1])
    plots, serials = zip(*sorted(zip(plots, serials)))

    plots = PlotSettings.objects.filter(pk__in=plots).order_by("id")

    active_plots = {}
    for serial, plot in zip(serials, plots):
        if serial not in active_plots.keys():
            active_plots[serial] = []
        if plot.show or ignore_plot_show:
            active_plots[serial].append(plot.qty)

    return active_plots
