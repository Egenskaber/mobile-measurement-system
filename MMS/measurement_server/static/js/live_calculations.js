// SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
// SPDX-License-Identifier: MIT

import {countDecimals} from "/static/js/helpers.js"

export  function doSelectAllSensors(globalCheckbox) {
    if (globalCheckbox.checked) {
        let box =$(".checkbox."+globalCheckbox.value+"-sensor");
        box.prop("checked",true)
    }
  }

export function toggleSensorToMeanSet(sensorCheckbox) {
      var el = sensorCheckbox.target
      if (!el.checked) {
          let sn = el.parentElement.parentElement.getAttribute("sn")
          let qty = el.parentElement.parentElement.getAttribute("quantity")
          var globalCheckbox = $("#"+qty+"Table thead [type=checkbox]")
          globalCheckbox.prop("checked", false)
      }
  }

export function computeDerivedQuantities(table) {
    var min_value = null
    var max_value = null
    var mean_value= 0 
    var N = 0
    var max_decimal_count = 0
    
    $('#'+table.id+ " tr[sn]:has(.checkbox)").each(function(){
        if ($(this).find("input").prop("checked")) {
            let val = $(this).find(".current-value").html()
            if (isNaN(val)) {
              return 
            }
            var decimal_count = countDecimals(val)
            if (decimal_count > max_decimal_count) {
              max_decimal_count = decimal_count
            }
            if (min_value == null || val < min_value) {
              min_value = val
            }
            if (max_value == null || val > max_value) {
              max_value = val
            }
            mean_value += parseFloat(val);
            N++; 
        }
    })
    if (N==0) {
        $("#"+table.id+' tr.derived').attr("hidden", true)
    } else {
        $("#"+table.id+' tr.derived').removeAttr("hidden")
    }
    if (mean_value) {
      mean_value /= N;
      mean_value = mean_value.toFixed(max_decimal_count)
    } else {
      mean_value = null
    }
    
    if (mean_value) {
      $('#'+table.id+' .mean-value').html(mean_value)
      $('#'+table.id+' .min-value').html(min_value)
      $('#'+table.id+' .max-value').html(max_value)
      $('#'+table.id+' tr.derived .unit').removeAttr("hidden")
    } else {
      $('#'+table.id+' .mean-value').html(null)
      $('#'+table.id+' .min-value').html(null)
      $('#'+table.id+' .max-value').html(null)
      $('#'+table.id+' tr.derived .unit').attr("hidden", true)
    }
  }
