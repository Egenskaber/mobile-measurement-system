// SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
// SPDX-License-Identifier: MIT

export function countDecimals(value) {
  let text = value.toString()
  // verify if number 0.000005 is represented as "5e-6"
  if (text.indexOf('e-') > -1) {
    let [base, trail] = text.split('e-');
    let deg = parseInt(trail, 10);
    return deg;
  }
  // count decimals for number in representation like "0.123456"
  if (Math.floor(value) !== value) {
    return value.toString().split(".")[1].length || 0;
  }
  return 0;
}

export function meanValueString(values, N0){
  var  N = values.length;
  var val = 0;
  for (var j = 0; j<values.length; j++){
    if (values[j]!==null) {
      val += parseFloat(values[j])
    } else {
      N = N-1
    }
  }
  val = val / N
                                                                    
                                                                    
  if (N < N0) {
    // NaN values found in last N0 values
    return {"value": val.toFixed(2), "error": N0-N, "samples": N0};
  } else {
    return {"value": val.toFixed(2), "error": 0 , "samples": N0};
  }
}

export function humanReadableUnitStrings(el) {
  switch (el.getAttribute("sign")) {
    case "C" : el.innerHTML = "°C"; break;
    case "pct" : el.innerHTML = "%"; break;
    case "C" : el.innerHTML = "°C"; break;
    case "mum" : el.innerHTML = "&#x1D707m"; break;
    case "ug/m3" : el.innerHTML = "&#x1D707g/m³"; break;
    case "1/cm3" : el.innerHTML = "1/cm³"; break;
    default:
      el.innerHTML = el.getAttribute("sign");
  }
}

export function humanReadableTableHeadingStrings(head) {
  var el = $(head).find(".qty");
  switch (head.getAttribute("quantity")) {
    case "co2" : el.html("CO<sub>2</sub>"); break;
    case "m_03_to_1_0_mu" : el.html("m(0.3-1&#x1D707m)"); break;
    case "m_03_to_2_5_mu" : el.html("m(0.3-2.5&#x1D707m)"); break;
    case "m_03_to_4_0_mu" : el.html("m(0.3-4&#x1D707m)"); break;
    case "m_03_to_10_0_mu" : el.html("m(0.3-10&#x1D707m)"); break;
    case "n_03_to_0_5_mu" : el.html("N(0.3-0.5&#x1D707m)"); break;
    case "n_03_to_1_0_mu" : el.html("N(0.3-1&#x1D707m)"); break;
    case "n_03_to_2_5_mu" : el.html("N(0.3-2.5&#x1D707m)"); break;
    case "n_03_to_4_0_mu" : el.html("N(0.3-4&#x1D707m)"); break;
    case "n_03_to_10_0_mu" : el.html("N(0.3-10&#x1D707m)"); break;
    case "temperature" : el.html("Temperature"); break;
    case "adc" : el.html("ADC bins"); break;
    case "humidity" : el.html("Humidity"); break;
    case "abs_pressure" : el.html("Abs. pressure"); break;
    case "diff_pressure" : el.html("Diff. pressure"); break;
    case "tps" : el.html("Particle size"); break;
    case "dewpoint" : el.html("Dewpoint"); break;
    case "volumeflow" : el.html("Volumeflow"); break;
    default:
      el.html(head.getAttribute("quantity"));
  }
}
