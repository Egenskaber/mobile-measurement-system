# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.urls import path, include, re_path

from rest_framework import routers
from rest_framework_extensions.routers import NestedRouterMixin

from . import views


class NestedDefaultRouter(NestedRouterMixin, routers.DefaultRouter):
    pass


# API routes
api_router = NestedDefaultRouter()
api_router.register(r"results/overview", views.MeasurementDataOverView)
result_routes = api_router.register(r"results", views.MeasurementDataView)

router = routers.DefaultRouter()
router.register("measurement", views.MeasurementView)
router.register("sensors", views.SensorView)
router.register("log", views.logView)
router.register("init", views.initView)
router.register("units", views.unitView)
router.register("mux", views.muxportView)
router.register("mqtt", views.mqttView)


urlpatterns = [
    path("", views.home),
    path("home/", views.home, name="home"),
    path("api/", include(router.urls), name="api"),
    path("init/", views.init_devices, name="init"),
    path("start/", views.init_measurement, name="start"),
    path("live/", views.data_live, name="live"),
    path("live/legacy", views.legacy_data_live, name="live_legacy"),
    path("plot/", views.data_visualization, name="plot"),
    path("plot/selected", views.data_selected_visualization, name="plot-selected"),
    path("downloads/", views.documentation, name="downloads"),
    re_path(r"^api/quick/?", views.quick_measurement, name="quick"),
    path("api/downloads/", views.api_documentation, name="download_data"),
    re_path(r"^api/plot/(.+)/$", views.data_plot, name="plot_data"),
    re_path(
        r"^api/last/current/(.+)/?$", views.data_last_current, name="last_current_data"
    ),
    re_path(r"^api/last/(\w+)/(\w+)/?$", views.data_last, name="last_data"),
    re_path(r"^api/last/(\w+)/?$", views.data_last, name="last_data_default"),
    re_path(r"^api/initialize/?", views.init_scan, name="remoteInit"),
    path(
        "api/calibration/",
        views.calibrationView,
        name="calibrations",
    ),
    path(
        "api/calibration/<str:serial_number>/",
        views.calibrationView,
        name="calibration",
    ),
    path("api/", include(api_router.urls), name="api"),
    path("settings/", views.settingsView, name="settings"),
    path("settings/<tabname>/", views.settingsView, name="settings_tabs"),
    # Legacy calls
    path("cmd/init", views.init_scan, name="remoteInit-old"),
    re_path(r"^quick/?", views.quick_measurement, name="quick-old"),
]
