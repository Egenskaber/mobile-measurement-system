# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

# Third party
from django import forms
from django.forms import ModelForm
from django.utils import timezone
from django.utils.timezone import now
from django.core.exceptions import ValidationError

from colorful.widgets import ColorFieldWidget

# Custom
from core.modules.sensors import SupportedSensors
from measurement_server.models.measurement import Measurement
from measurement_server.models.mqtt import MQTT
from measurement_server.models.measurement import Units
from measurement_server.models.measurement import PlotSettings
from .models.measurement import KnownSensors
from .modules.helpers import isHex
from .fields import AlphaNumericField

"""
These forms select database entries and allow to manipulate
them in a controlled environment and make it easy to integrate
such interface into the django template environment.
"""


class measurementSettingsForm(ModelForm):
    """
    Set values associated to the Live data
    and the visualization menu
    """

    class Meta:
        model = Measurement
        fields = [
            "avg_bucket_size",
            "live_mean",
            "warning_time",
            "max_freq",
            "live_view_table_height",
            "unified_timescale",
        ]

    def clean_max_freq(self):
        data = self.cleaned_data["max_freq"]
        if data < -1:
            return -1
        return data

    def clean_avg_bucket_size(self):
        data = self.cleaned_data["avg_bucket_size"]
        if data < 0:
            return 1
        return data

    def clean_live_mean(self):
        data = self.cleaned_data["live_mean"]
        if data < 1:
            return 1
        return data

    def clean_warning_time(self):
        data = self.cleaned_data["warning_time"]
        if data < 1:
            return 1
        return data

    def clean_live_view_table_height(self):
        data = self.cleaned_data["live_view_table_height"]
        if data < 100:
            return 100
        return data


class visualizationAdjustmentForm(ModelForm):
    """
    Set values associated to the visualization menu
    """

    class Meta:
        model = Measurement
        fields = ["avg_bucket_size", "plot_period"]

    def clean_plot_period(self):
        data = self.cleaned_data["plot_period"]
        if data < 0:
            return 0
        return data


class measurementModeForm(ModelForm):
    """Switch between supported measurements modes"""

    class Meta:
        model = Measurement
        fields = ["daq_mode", "spi_mux_mode"]


class uploadSensorConfigForm(forms.Form):
    """
    This form allows to import sensor identifier and colors
    The file size and format is checked and information are parsed
    to the database
    """

    file = forms.FileField(required=True)

    def clean_file(self):
        file = self.cleaned_data["file"]

        if not str(file).endswith(".csv"):
            raise ValidationError("Expecting .csv.")

        if file.size > 2 * 1000**2:
            raise ValidationError(
                f"File too large (max 1MB got {file.size/1E6:0.1f}MB)"
            )

        valid_info = []

        lines = []
        for line in file:  # This applies universal newlines
            lines.append(line)

        for i, line in enumerate(lines):
            line = line.decode()

            # Remove control characters and spaces
            ctr_char = dict.fromkeys(range(33))
            line = line.translate(ctr_char)

            if line[-1] == ",":
                line = line[:-1]
            if line[0] == "#":
                continue
            elif "," not in line:
                raise ValidationError("No column seperator found (expected ,).")

            info = line.split(",")
            x, y, z = None, None, None
            non_empty_cols = 0

            if len(info) < 4 or "" in info[:4]:
                raise ValidationError(
                    f"Missing information in row {i+1}.\
                        (serial, identifier, sensortype, color(hex))\
                        are expected in the first 4 columns."
                )
            elif len(info) not in [4, 7]:
                raise ValidationError(
                    f"Invalid format. Either (serial, identifier, sensortype, color(hex)) \
                      or (serial, identfier, sensortype, color(hex), x, y, z)"
                )
            elif len(info) == 7:
                msg = ""
                try:
                    x = float(info[4])
                except ValueError:
                    msg += f"Invalid x coordinate at {info[0]}/{info[1]}"

                try:
                    y = float(info[5])
                except ValueError:
                    msg += f"Invalid y coordinate at {info[0]}/{info[1]}"

                try:
                    z = float(info[6])
                except ValueError:
                    msg += f"Invalid z coordinate at {info[0]}/{info[1]}"

                if msg != "":
                    raise ValidationError(msg)

            supported_mres = [k.lower() for k in SupportedSensors.keys()]

            if info[2].lower() not in supported_mres:
                raise ValidationError(f"Unknown sensor type {info[2]}.")

            if info[3][0] != "#":
                raise ValidationError(
                    f"Incorrect color format in line {i+1} expected i.e. #00ABF1 got {info[3]}"
                )
            if len(info[3]) != 7:
                raise ValidationError(
                    f"Color string too short in line {i+1} expected i.e. #00ABF1 got {info[3]}"
                )

            upper_info = info[3].upper()[1:]

            if not isHex(upper_info):
                raise ValidationError(
                    f"Non hex number in line {i+1} expected i.e. #00ABF1 got {info[3]}"
                )

            qs = KnownSensors.objects.filter(SENSORserial=info[0])

            if qs:
                if len(info) == 4:
                    qs.update(
                        Color=info[3], SENSORidentifier=info[1], SENSORtype=info[2]
                    )
                else:
                    qs.update(
                        Color=info[3],
                        SENSORidentifier=info[1],
                        SENSORtype=info[2],
                        x=x,
                        y=y,
                        z=z,
                    )

            else:
                sensor = SupportedSensors[info[2]]["sensor"]
                ks = KnownSensors()
                ks.SENSORserial = info[0]
                ks.SENSORidentifier = info[1]
                ks.SENSORtype = info[2]
                ks.Color = info[3]
                if len(info) > 4:
                    ks.x = info[4]
                    ks.y = info[5]
                    ks.z = info[6]

                ks.serial_mode = sensor._serial_mode
                ks.save()

                # Add supported measurement units
                for u in sensor._units:
                    # This implies that all measurements of the same qty
                    # are converted to the same unit
                    unit, create = Units.objects.get_or_create(qty=u)
                    unit.qty = u
                    unit.sign = sensor._units[u]
                    unit.save()
                    if unit not in ks.units.all():
                        ks.units.add(unit)

                    plot = PlotSettings(qty=u, show=True)
                    plot.save()
                    ks.plots.add(plot)
                ks.save()


class scheduleMeasurementForm(forms.Form):
    """
    Configure a measurement with a given name and a set start and stop time.
    """

    # Measuremnt title which will also be the exported filename + suffix
    name = AlphaNumericField(max_length=200)

    # Measurement starting time
    start_measure = forms.DateTimeField()

    # Measurement duration
    duration_seconds = forms.IntegerField(initial=0, max_value=59, min_value=0)
    duration_minutes = forms.IntegerField(initial=1, max_value=59, min_value=0)
    duration_hours = forms.IntegerField(initial=0, max_value=23, min_value=0)
    duration_days = forms.IntegerField(initial=0, max_value=1000, min_value=0)

    def __init__(self, start, stop, *args, **kwargs):
        super(scheduleMeasurementForm, self).__init__(*args, **kwargs)
        # Measuremnt title
        class_date = timezone.localtime(now())
        name = class_date.strftime("%Y%m%d_%H%M")
        self.fields["start_measure"].initial = start
        self.fields["name"].initial = name

    def clean(self):
        cleaned_data = super(scheduleMeasurementForm, self).clean()
        minutes = cleaned_data.get("duration_minutes")
        hours = cleaned_data.get("duration_hours")
        days = cleaned_data.get("duration_days")
        seconds = cleaned_data.get("duration_seconds")
        if seconds + minutes + days + hours == 0:
            raise forms.ValidationError(
                "Please set a measurement duration of at least 1 second."
            )


class selectMeasurementForm(forms.Form):
    """
    Display measurement name to select Measurement ID
    to i.e. pick a data set to visualize.
    """

    measurement_name = forms.ChoiceField(label="Measurement ID")

    def __init__(self, measurements, *args, **kwargs):
        super(selectMeasurementForm, self).__init__(*args, **kwargs)
        choices = [[m.id, m.name] for m in measurements]
        self.fields["measurement_name"].choices = choices
        self.fields["measurement_name"].initial = choices[-1]


class modifySensorIdentifierForm(forms.Form):
    """
    Allow to manipulate sensor information like
    identifiers, position indicators or colors.
    """

    def clean_serial(self):
        """Make sure serials are unique."""
        if not KnownSensors.objects.filter(
            SENSORserial=self.cleaned_data["serial"]
        ).exists():
            raise ValidationError(
                f"Unknown sensor serial number {self.cleaned_data['serial']}"
            )
        return self.cleaned_data["serial"]

    def clean_identifier(self):
        """Make sure identifier are unique and do not contain whitespace."""
        identifier = self.cleaned_data["identifier"].replace(" ", "")

        if KnownSensors.objects.filter(SENSORidentifier=identifier).count() > 1:
            raise ValidationError("Identifier is not unique.")

        return identifier

    def __init__(self, sensor, *args, **kwargs):
        super(modifySensorIdentifierForm, self).__init__(*args, **kwargs)

        self.sensor = sensor
        serial = forms.CharField(initial=sensor.SENSORserial)

        identifier = forms.CharField(initial=sensor.SENSORidentifier)
        identifier.widget.attrs.update({"class": "sensor-identifier"})

        color = forms.CharField(
            widget=ColorFieldWidget, max_length=10, initial=sensor.Color
        )
        self.fields["serial"] = serial
        self.fields["color"] = color
        self.fields["identifier"] = identifier

        # Positioning information
        x = forms.DecimalField(
            decimal_places=2, max_digits=7, required=False, initial=sensor.x
        )
        y = forms.DecimalField(
            decimal_places=2, max_digits=7, required=False, initial=sensor.y
        )
        z = forms.DecimalField(
            decimal_places=2, max_digits=7, required=False, initial=sensor.z
        )

        # Add css class to position fields
        x.widget.attrs.update({"class": "position-input"})
        y.widget.attrs.update({"class": "position-input"})
        z.widget.attrs.update({"class": "position-input"})

        # Register positioning fields
        self.fields["x"] = x
        self.fields["y"] = y
        self.fields["z"] = z

        for plot_settings in sensor.plots.all():
            self.fields[plot_settings.qty] = forms.BooleanField()
            self.fields[plot_settings.qty].initial = plot_settings.show
            self.fields[plot_settings.qty].required = False
            self.fields[plot_settings.qty].html_name = Units.objects.get(
                qty=plot_settings.qty
            ).name


class mqttSettingsForm(ModelForm):
    """
    Interface to submit MQTT broker connection data.
    See ``measurement_server/views/settings.py`` for a implementation reference.
    """

    password = forms.CharField(widget=forms.PasswordInput, required=False)

    class Meta:
        model = MQTT
        exclude = ("active",)
