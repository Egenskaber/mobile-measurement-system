# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from rest_framework import serializers

# Data models
from .models.log import Log
from .models.init import Init
from .models.measurement import Measurement
from .models.measurement import KnownSensors
from .models.measurement import MeasurementData
from .models.measurement import MUXAddress
from .models.measurement import Calibration
from .models.mqtt import MQTT
from core.modules.mqtt_client import MQTTClient
from .sensor_serializers import (
    SHT85Serializer,
    SHT85resultsSerializer,
    MeasurementDataSHT85Serializer,
    ADS1015Serializer,
    ADS1015resultsSerializer,
    MeasurementDataADS1015Serializer,
    NAU7802Serializer,
    NAU7802resultsSerializer,
    MeasurementDataNAU7802Serializer,
    SFMSerializer,
    SFMresultsSerializer,
    MeasurementDataSFMSerializer,
    MAX31865Serializer,
    MAX31865resultsSerializer,
    MeasurementDataMAX31865Serializer,
    SMGair2Serializer,
    SMGair2resultsSerializer,
    MeasurementDataSMGair2Serializer,
    SDP8Serializer,
    SDP8resultsSerializer,
    MeasurementDataSDP8Serializer,
    S8000Serializer,
    S8000resultsSerializer,
    MeasurementDataS8000Serializer,
    OptidewSerializer,
    OptidewresultsSerializer,
    MeasurementDataOptidewSerializer,
    SPS30Serializer,
    SPS30resultsSerializer,
    MeasurementDataSPS30Serializer,
    BME280Serializer,
    BME280resultsSerializer,
    MeasurementDataBME280Serializer,
    SCD30Serializer,
    SCD30resultsSerializer,
    SCD41Serializer,
    SCD41resultsSerializer,
    MeasurementDataSCD30Serializer,
    UnitSerializer,
    PlotSerializer,
)


class ModelSerializerMQTT(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def save(self, topic, msg, **kwargs):
        mqtt = MQTTClient()
        if mqtt is not None:
            if isinstance(msg, str):
                mqtt.publish(topic, msg)
            elif isinstance(msg, dict):
                for k in msg.keys():
                    mqtt.publish(topic + k, msg[k])
        super().save(**kwargs)


class MqttSerializer(serializers.ModelSerializer):
    """MQTT setup"""

    class Meta:
        model = MQTT
        read_only_fields = ["connected"]
        exclude = ["password"]


class LogSerializer(serializers.ModelSerializer):
    """Log entry API"""

    topic = "log"

    class Meta:
        model = Log
        fields = "__all__"
        read_only = fields


class SensorSerializer(ModelSerializerMQTT):
    """Collection of all registered sensor identifier."""

    units = UnitSerializer(many=True, read_only=True)
    calibration = serializers.StringRelatedField(
        many=True,
    )
    # calibration = serializers.HyperlinkedRelatedField(
    #    many=True,
    #    read_only=False,
    #    queryset=Calibration.objects.filter(readonly=False),
    #    view_name="calibration-detail",
    # )
    plots = PlotSerializer(many=True)
    topic = "sensors"

    class Meta:
        model = KnownSensors
        fields = "__all__"
        # read_only = fields

    def save(self, **kwargs):
        typ = self.validated_data["SENSORtype"]
        sn = self.validated_data["SENSORserial"]
        topic = f"{self.topic}/{typ}/{sn}/"
        msg = {}
        for k in self.validated_data.keys():
            if k not in ["SENSORtype", "SENSORserial"]:
                msg[k] = str(self.validated_data[k])

        super().save(topic, msg, **kwargs)


class MeasurementSerializer(serializers.ModelSerializer):
    """Measurement state API."""

    class Meta:
        model = Measurement
        fields = "__all__"


class initSerializer(serializers.ModelSerializer):
    """Holds information about the initialization process."""

    class Meta:
        model = Init
        fields = "__all__"


class MeasurementDataSerializer(serializers.ModelSerializer):
    sht = SHT85Serializer(source="sht85_set", many=True, read_only=True)
    scd30 = SCD30Serializer(source="scd30_set", many=True, read_only=True)
    scd41 = SCD41Serializer(source="scd41_set", many=True, read_only=True)
    sps30 = SPS30Serializer(source="sps30_set", many=True, read_only=True)
    s8000 = S8000Serializer(source="s8000_set", many=True, read_only=True)
    optidew = OptidewSerializer(source="optidew_set", many=True, read_only=True)
    smgair2 = SMGair2Serializer(source="smgair2_set", many=True, read_only=True)
    bme280 = BME280Serializer(source="bme280_set", many=True, read_only=True)
    nau7802 = NAU7802Serializer(source="nau7802_set", many=True, read_only=True)
    sfm = SFMSerializer(source="sfm_set", many=True, read_only=True)
    sdp8 = SDP8Serializer(source="sdp8_set", many=True, read_only=True)
    max31865 = MAX31865Serializer(source="max31865_set", many=True, read_only=True)
    ads1015 = ADS1015Serializer(source="ads1015_set", many=True, read_only=True)

    class Meta:
        model = MeasurementData
        fields = "__all__"
        readonly = fields


class MeasurementDataOverviewSerializer(serializers.ModelSerializer):
    N_sht = serializers.SerializerMethodField()
    N_scd30 = serializers.SerializerMethodField()
    N_scd41 = serializers.SerializerMethodField()
    N_smgair2 = serializers.SerializerMethodField()
    N_sps30 = serializers.SerializerMethodField()
    N_s8000 = serializers.SerializerMethodField()
    N_optidew = serializers.SerializerMethodField()
    N_bme280 = serializers.SerializerMethodField()
    N_nau7802 = serializers.SerializerMethodField()
    N_sfm = serializers.SerializerMethodField()
    N_sdp8 = serializers.SerializerMethodField()
    N_max31865 = serializers.SerializerMethodField()
    N_ads1015 = serializers.SerializerMethodField()

    def get_N_sht(self, obj):
        return obj.sht85_set.count()

    def get_N_scd30(self, obj):
        return obj.scd30_set.count()

    def get_N_smgair2(self, obj):
        return obj.smgair2_set.count()

    def get_N_s8000(self, obj):
        return obj.s8000_set.count()

    def get_N_optidew(self, obj):
        return obj.optidew_set.count()

    def get_N_sps30(self, obj):
        return obj.sps30_set.count()

    def get_N_bme280(self, obj):
        return obj.bme280_set.count()

    def get_N_nau7802(self, obj):
        return obj.nau7802_set.count()

    def get_N_sfm(self, obj):
        return obj.sfm_set.count()

    def get_N_sdp8(self, obj):
        return obj.sdp8_set.count()

    def get_N_max31865(self, obj):
        return obj.max31865_set.count()

    def get_N_ads1015(self, obj):
        return obj.ads1015_set.count()

    class Meta:
        model = MeasurementData
        fields = ["id", "name", "start_measure", "stop_measure", "stopped"]
        fields += [
            "N_sht",
            "N_scd30",
            "N_scd41",
            "N_smgair2",
            "N_sps30",
            "N_s8000",
            "N_optidew",
            "N_bme280",
            "N_nau7802",
            "N_sfm",
            "N_sdp8",
            "N_max31865",
            "N_ads1015",
        ]
        readonly = fields


class MUXAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = MUXAddress
        fields = "__all__"
