# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Declaration of views for main interface
"""

import re
import json
import datetime

# For checking diskspace.
import shutil

# Django specifics
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.timezone import now
from django.utils.timezone import make_aware
from django.utils.dateparse import parse_datetime
from django.db import connection, DatabaseError, NotSupportedError

# DRF
from rest_framework_extensions.mixins import NestedViewSetMixin

# Data models
from ..models.log import Log
from ..models.init import Init
from ..models.measurement import Units
from ..models.measurement import Measurement
from ..models.measurement import MeasurementData
from ..models.measurement import PlotSettings

# Sensors
from ..models.measurement import KnownSensors
from ..models.mqtt import MQTT

from core.modules.sensors import SupportedSensors
from core.modules.mqtt_client import MQTTClient

# Forms
from ..forms import modifySensorIdentifierForm
from ..forms import selectMeasurementForm
from ..forms import scheduleMeasurementForm
from ..forms import measurementSettingsForm
from ..forms import measurementModeForm
from ..forms import mqttSettingsForm
from ..forms import visualizationAdjustmentForm

# Helpers
from ..modules.dataExport import dataExport
from ..modules.unit_names import unit_names
from ..modules.helpers import get_all_referenced_units
from ..modules.helpers import get_sensor_counts
from ..modules.helpers import get_sensor_plot_information

# Get download information
from ..media.datasheets.overview import downloads


def home(request):
    return render(request, "measurement_server/home.html")


def init_devices(request):
    """
    start and display ftdi and sensor initialization
    """
    if request.method == "POST":
        if "init" in request.POST:
            if Init.objects.all().count() == 0:
                Init.objects.create()
            state = Init.objects.get(id=1)
            if not state.init:
                state.init = True
                state.save()
        else:
            return HttpResponse("Invalid POST Req." + request.POST)
    context = {"form": "None"}
    return render(request, "measurement_server/init.html", context)


def init_measurement(request):
    """
    set time, duration, frequency of measurement, name of measurement
    """
    total, used, free = shutil.disk_usage("/")
    free = free // 2**30
    if free < 2 and MeasurementData.objects.count() > 0:
        MeasurementData.objects.last().space_warning = True

    start = now()
    stop = now() + datetime.timedelta(hours=1)
    if request.method == "POST":
        scheduleForm = scheduleMeasurementForm(start, stop, request.POST)
        if scheduleForm.is_valid():
            Measurement.objects.all().delete()
            new_measurement = Measurement()
            Log(message="Using default measurement settings.", typ="FTDI").save()

            start = parse_datetime(scheduleForm.data["start_measure"])
            new_measurement.name = scheduleForm.data["name"]

            days = datetime.timedelta(days=int(scheduleForm.data["duration_days"]))
            hours = datetime.timedelta(hours=int(scheduleForm.data["duration_hours"]))
            minutes = datetime.timedelta(
                minutes=int(scheduleForm.data["duration_minutes"])
            )
            seconds = datetime.timedelta(
                seconds=int(scheduleForm.data["duration_seconds"])
            )
            duration = days + hours + minutes + seconds

            start = make_aware(start)
            if start < now():
                start = now()

            new_measurement.start_measure = start
            new_measurement.stop_measure = start + duration
            new_measurement.disk_space = free
            new_measurement.connected_sensors = KnownSensors.objects.filter(
                connected=True
            ).count()
            new_measurement.save()

    else:
        scheduleForm = scheduleMeasurementForm(start, stop)

    context = {"schedule_form": scheduleForm}
    return render(request, "measurement_server/measurement_start.html", context)


def data_live(request):
    live_url = None
    raw_url = None
    context = {
        "live_url": None,
        "warning_thes": 1e3,
        "sensor_numbers": {},
        "warning_thres": 1e3,
        "raw_url": None,
        "units": Units.objects.all(),
        "live_view_table_height": 500,
    }

    if MeasurementData.objects.last() is None:
        pass
    elif (
        Measurement.objects.count() > 0
        and Measurement.objects.first().start_measure
        == MeasurementData.objects.last().start_measure
        and Measurement.objects.first().active
    ):
        measurement = Measurement.objects.last()
        measurement_data = MeasurementData.objects.last()
        live_url = (
            "/api/last/" + str(measurement_data.id) + "/" + str(measurement.live_mean)
        )
        raw_url = "/api/results/" + str(measurement_data.id) + "/"

        units = get_all_referenced_units()
        context = {
            "live_url": live_url,
            "warning_thres": measurement.warning_time,
            "sensor_numbers": get_sensor_counts(measurement_data),
            "raw_url": raw_url,
            "units": units,
            "live_view_table_height": measurement.live_view_table_height,
        }
    return render(request, "measurement_server/live.html", context)

def data_selected_visualization(request):
    return data_visualization(request, ignore_plot_show=False)

def data_visualization(request, ignore_plot_show=True):
    if MeasurementData.objects.all().count() > 0:
        measurement_id = MeasurementData.objects.last().id
        measurement = Measurement.objects.last()
        visualization_settings = None
        measurement_form = None

        if request.method == "POST":
            if "form" in request.POST.keys():
                if request.POST["form"] == "adjust_visualization":
                    visualization_settings = visualizationAdjustmentForm(
                        request.POST, instance=measurement
                    )
                    visualization_settings.save()
                elif request.POST["form"] == "select_measurement":
                    measurement_form = selectMeasurementForm(
                        MeasurementData.objects.all(), request.POST
                    )
                    if measurement_form.is_valid():
                        measurement_id = request.POST["measurement_name"]
            elif "save" in request.POST.keys():
                return settingsView(request)

        if measurement_form is None:
            measurement_form = selectMeasurementForm(MeasurementData.objects.all())
        if visualization_settings is None:
            visualization_settings = visualizationAdjustmentForm(instance=measurement)

        measurement_obj = MeasurementData.objects.get(id=measurement_id)

        raw_url = "/api/results/" + str(measurement_id)
        measurement_url = "/files/results/" + str(measurement_id)

        context = {
            "measurement_exists": True,
            "avg_bucket_size": measurement.avg_bucket_size,
            "measurement_form": measurement_form,
            "measurement_url": measurement_url,
            "measurement_details": measurement_obj,
            "sensor_numbers": get_sensor_counts(measurement_obj),
            "raw_url": raw_url,
            "units": Units.objects.all(),
            "export_formats": dataExport.export_formats,
            "visualization_settings": visualization_settings,
            "sensor_plot_settings": get_sensor_plot_information(ignore_plot_show=ignore_plot_show),
        }
    else:
        context = {"measurement_exists": False}

    return render(request, "measurement_server/data_plot.html", context)


def documentation(request):
    """
    Displays all hardware datasheets
    """
    context = {"downloads": downloads}
    return render(request, "measurement_server/downloads.html", context)
