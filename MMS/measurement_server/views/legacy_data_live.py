from django.shortcuts import render
from ..models.measurement import Units
from ..models.measurement import Measurement
from ..models.measurement import MeasurementData
from ..modules.helpers import get_all_referenced_units
from ..modules.helpers import get_sensor_counts


def legacy_data_live(request):
    live_url = None
    raw_url = None
    context = {
        "live_url": None,
        "warning_thes": 1e3,
        "measurement_no_sht": 0,
        "measurement_no_scd30": 0,
        "measurement_no_ads1015": 0,
        "measurement_no_nau7802": 0,
        "measurement_no_smgair2": 0,
        "measurement_no_sps30": 0,
        "measurement_no_s8000": 0,
        "measurement_no_bme280": 0,
        "measurement_no_sfm": 0,
        "measurement_no_sdp8": 0,
        "measurement_no_max31865": 0,
        "warning_thres": 1e3,
        "raw_url": None,
    }

    if MeasurementData.objects.last() is None:
        pass
    elif (
        Measurement.objects.count() > 0
        and Measurement.objects.first().start_measure
        == MeasurementData.objects.last().start_measure
        and Measurement.objects.first().active
    ):
        live_url = (
            "/api/last/"
            + str(MeasurementData.objects.last().id)
            + "/"
            + str(Measurement.objects.last().live_mean)
        )
        raw_url = "/api/results/" + str(MeasurementData.objects.last().id) + "/"
        context = {
            "live_url": live_url,
            "warning_thres": Measurement.objects.last().warning_time,
            "measurement_no_sht": MeasurementData.objects.last().sht85_set.count,
            "measurement_no_scd30": MeasurementData.objects.last().scd30_set.count,
            "measurement_no_ads1015": MeasurementData.objects.last().ads1015_set.count,
            "measurement_no_nau7802": MeasurementData.objects.last().nau7802_set.count,
            "measurement_no_smgair2": MeasurementData.objects.last().smgair2_set.count,
            "measurement_no_sps30": MeasurementData.objects.last().sps30_set.count,
            "measurement_no_s8000": MeasurementData.objects.last().s8000_set.count,
            "measurement_no_bme280": MeasurementData.objects.last().bme280_set.count,
            "measurement_no_sfm": MeasurementData.objects.last().sfm_set.count,
            "measurement_no_sdp8": MeasurementData.objects.last().sdp8_set.count,
            "measurement_no_max31865": MeasurementData.objects.last().max31865_set.count,
            "raw_url": raw_url,
        }
    return render(request, "measurement_server/live_legacy.html", context)
