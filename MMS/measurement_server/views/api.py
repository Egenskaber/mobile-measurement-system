"""
You can access the API via <url>/api/.
API views include customized HttpRequest handlers
to i.e. start or stop measurements quickly.
Django-Rest-Framework is used for generating a database
representation. The API holds all information accessible via
the Webinterface and can be used to develop a custom interface
speicialized for your needs.
"""

import shutil
import datetime
from django.utils.timezone import now

from django.http import JsonResponse
from rest_framework.response import Response
from django_auto_prefetching import AutoPrefetchViewSetMixin
from django.utils.timezone import localtime
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

from rest_framework.decorators import api_view
from rest_framework import viewsets
from rest_framework_extensions.mixins import NestedViewSetMixin

from core.modules.mqtt_client import MQTTClient

from ..models.log import Log
from ..models.init import Init
from ..models.measurement import Measurement
from ..models.measurement import MeasurementData
from ..models.measurement import MUXAddress
from ..models.measurement import KnownSensors
from ..models.measurement import Units
from ..models.mqtt import MQTT

from ..media.datasheets.overview import downloads

from ..serializers import LogSerializer
from ..serializers import initSerializer
from ..serializers import MUXAddressSerializer
from ..serializers import UnitSerializer
from ..serializers import MeasurementDataOverviewSerializer
from ..serializers import MqttSerializer
from ..serializers import MeasurementDataSerializer
from ..serializers import MeasurementSerializer
from ..serializers import SensorSerializer


def get_recent_measurement_obj(mqtt_client):
    """
    Check if a Measurement obj is known.
    It zero or more than 2 systems states are present reset to default.

    Args:
        mqtt_client (core.modules.mqtt_client.MQTTClient): MQTT settings to send database changes to broker.

    Return:
        Measurement object containing the current system state.

    """

    if Measurement.objects.all().count() > 1:
        # Delete all measurements
        Measurement.objects.all().delete()
        # Setup new measurement
        mr = Measurement()
        mr.save()
        Log(message="Using default measurement settings.", typ="Sys").save(
            mqtt=mqtt_client
        )
    # If only one Measurement object exists it will be reused
    elif Measurement.objects.all().count() == 1:
        mr = Measurement.objects.last()
    # No Measurement object => create one
    else:
        Log(message="Using default measurement settings.", typ="Sys").save(
            mqtt=mqtt_client
        )
        mr = Measurement()
        mr.save()

    return mr


@api_view(["GET", "POST"])
def quick_measurement(request):
    """
    On get the current system state (most recent Measurement model object) is returned.
    On post either a running measurement is stopped or a new measurement is started.
    If no measurement object is present in the database
    only a error and an invalide active state (None) is returned.
    Sending start while a measurement is running or sending `stop` although no measurement
    is running an error will be returned.

    Args:
        request (HttpRequest): Get or post request

    Return:
        JsonResponse if no valid Measurement information is present
        or otherwise a rest_framework.Request object with current measurement information.
    """

    # Quick database health check.
    if request.method == "GET":
        if Measurement.objects.all().count() == 0:
            return JsonResponse(
                {"error": "System state cannot be read from database.", "active": None}
            )
        else:
            mr = get_recent_measurement_obj(mqtt_client)

    elif request.method == "POST":
        mqtt_client = MQTTClient()
        mr = get_recent_measurement_obj(mqtt_client)

        if "quick" in request.data.keys() and request.data["quick"] == "start":
            if mr.active:
                return JsonResponse(
                    {"error": "Measurement is running already.", "active": True}
                )

            # Check if Measurement instance exists
            #  If there is more than one an error occured and everything
            #  will be set to default
            Log(message="Received quick measurement start.", typ="Sys").save(
                mqtt=mqtt_client
            )

            # Populate Measurement object
            class_date = localtime(now())
            name = class_date.strftime("%Y%m%d_%H%M")

            start = now()
            duration = datetime.timedelta(days=999 * 365)

            total, used, free = shutil.disk_usage("/")
            free = free // 2**30

            mr.name = name
            mr.start_measure = start
            mr.stop_measure = start + duration
            mr.disk_space = free
            mr.connected_sensors = KnownSensors.objects.filter(connected=True).count()
            mr.active = True
            mr.stopped = False

            mr.save()

        elif "quick" in request.data.keys() and request.data["quick"] == "stop":
            if not mr.active:
                return JsonResponse(
                    {"error": "No measurement to stop.", "active": False}
                )

            Log(message="Received measurement stop.", typ="Sys").save()
            mr.active = False
            mr.stopped = True
            mr.save()
        else:
            return JsonResponse({"error": "Invalid post request.", "active": None})

    return Response(MeasurementSerializer(mr, context={"request": request}).data)


@api_view(["GET", "POST"])
def init_scan(request):
    """
    Set up an interface to start an initialization process from remote
    """

    if Init.objects.all().count() < 1:
        new_init = Init()
        new_init.save()
        data = initSerializer(new_init).data
        return Response(
            {
                "error": "No init entry found in database! A new one has been created. Try again.",
                "data": data,
            }
        )
    elif Init.objects.all().count() > 1:
        Init.objects.all().delete()
        new_init = Init()
        new_init.save()
        data = initSerializer(new_init).data
        return Response(
            {
                "error": "More than one init entry found in database! All were deleted and a new one has been created. Try again.",
                "data": data,
            }
        )

    state = Init.objects.all().last()
    data = initSerializer(state).data

    if request.method == "POST":
        if "init" in request.data and request.data["init"] == "True" and not state.init:
            state.init = True
            state.save()
            data = initSerializer(state).data
            return Response({"message": "Started Init", "data": data})
        else:
            return Response({"error": "Invalid API request.", "data": data})

    return Response({"message": "Current state", "data": data})


# Auto generated model drf viewsets
class unitView(viewsets.ModelViewSet):
    queryset = Units.objects.all()
    serializer_class = UnitSerializer


class MeasurementDataOverView(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = MeasurementData.objects.all()
    serializer_class = MeasurementDataOverviewSerializer


class logView(viewsets.ReadOnlyModelViewSet):
    queryset = Log.objects.all()
    serializer_class = LogSerializer


# TODO: Obsolet?
class muxportView(viewsets.ModelViewSet):
    queryset = MUXAddress.objects.all()
    serializer_class = MUXAddressSerializer


class mqttView(viewsets.ModelViewSet):
    queryset = MQTT.objects.all()
    serializer_class = MqttSerializer


class MeasurementDataView(
    NestedViewSetMixin, AutoPrefetchViewSetMixin, viewsets.ModelViewSet
):
    queryset = MeasurementData.objects.all()
    serializer_class = MeasurementDataSerializer


class MeasurementView(viewsets.ModelViewSet):
    queryset = Measurement.objects.all()
    serializer_class = MeasurementSerializer


class SensorView(viewsets.ModelViewSet):
    queryset = KnownSensors.objects.all()
    serializer_class = SensorSerializer


class initView(viewsets.ModelViewSet):
    queryset = Init.objects.all()
    serializer_class = initSerializer


def api_documentation(request):
    """
    Displays all hardware datasheets
    """
    return JsonResponse(downloads, safe=False)
