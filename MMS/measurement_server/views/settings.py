# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Declaration of views for main interface
"""

import re
import os

# For checking diskspace.
import shutil

# Django specifics
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.timezone import now
from django.db import connection, DatabaseError, NotSupportedError

# Data models
from ..models.log import Log
from ..models.measurement import Measurement
from ..models.measurement import MeasurementData

# Sensors
from ..models.measurement import KnownSensors
from ..models.mqtt import MQTT

from core.modules.sensors import SupportedSensors
from core.modules.mqtt_client import MQTTClient

# Forms
from ..forms import modifySensorIdentifierForm
from ..forms import measurementSettingsForm
from ..forms import measurementModeForm
from ..forms import mqttSettingsForm
from ..forms import uploadSensorConfigForm

# Helpers
from ..modules.dataExport import dataExport
from ..modules.unit_names import unit_names

import settings
from ..modules.system import hardware_information
from ..modules.system import software_information


def settingsView(request, tabname=""):
    # make sure that all currently initialized sensors are included in sensor identifier

    log = Log.objects.all()

    total, used, free = shutil.disk_usage("/")
    # Check if measurement data exists
    # update free data space
    # register max data points form
    measurement_settings_form = None
    mqtt_settings_form = None
    if Measurement.objects.all().exists():
        data = Measurement.objects.last()
    else:
        # if no Measurement object exists > create new Measurement object
        data = Measurement()
        Log(message="Using default measurement settings.", typ="FTDI").save()

    data.disk_space = free // 2**30
    measurement_settings_form = measurementSettingsForm(instance=data)
    mqtt_settings_form = mqttSettingsForm(instance=MQTT.objects.last())
    measurement_mode_form = measurementModeForm(instance=data)
    sensor_upload_form = uploadSensorConfigForm()

    # results folder path
    results_path = f"{settings.MEDIA_ROOT}/results/"

    # get current sensors
    current_sensors = []

    # store total number of connected sensors
    data.connected_sensors = KnownSensors.objects.filter(connected=True).count()
    data.save()
    unified_timescale = data.unified_timescale

    if data.connected_sensors > 0:
        current_sensors = KnownSensors.objects.filter(connected=True)

    # Make sure all current sensors are active.
    for cs in current_sensors:
        s, created = KnownSensors.objects.get_or_create(SENSORserial=cs.SENSORserial)
        s.SENSORtype = cs.SENSORtype
        s.FTDIserial = cs.FTDIserial
        s.save()

    sensor_name_forms = [
        modifySensorIdentifierForm(f, prefix=str(f.SENSORserial))
        for f in KnownSensors.objects.all()
    ]

    if request.method == "POST":
        if "delete_sensors" in request.POST:
            if request.POST["delete_sensors"] == "inactive":
                message = "Deleted all inactive sensors."
                KnownSensors.objects.filter(connected=False).delete()

            elif request.POST["delete_sensors"] == "all":
                KnownSensors.objects.all().delete()
                message = "Deleted all registered sensors."

            else:
                message = (
                    f"Unknown sensor delete command { request.POST['delete_sensors'] }"
                )

            if Measurement.objects.all().exists():
                data = Measurement.objects.last()
                data.connected_sensors = KnownSensors.objects.filter(
                    connected=True
                ).count()
                data.save()

            if message is not None:
                Log(message=message, typ="Sensor").save()

            sensor_name_forms = []
            for f in KnownSensors.objects.all():
                prefix = str(f.SENSORserial)
                sensor_form = modifySensorIdentifierForm(f, prefix=prefix)
                sensor_name_forms.append(sensor_form)

        elif "save_sensors" in request.POST:
            queryset = KnownSensors.objects.all()
            time_str = now().strftime("%Y%m%d_%H%M")
            filename = f"{time_str}_sensor_export.csv"
            columns = [
                "SENSORserial",
                "SENSORidentifier",
                "SENSORtype",
                "Color",
                "x",
                "y",
                "z",
            ]

            response = HttpResponse(content_type="application/ms-excel")
            response["Content-Disposition"] = f"attachment;filename={filename}"

            out = "#"
            # Store header
            for column in columns:
                out += column + ","
            out += "\n"

            for sensor in queryset:
                for column in columns:
                    out += getattr(sensor, column).__str__() + ","

                out += "\n"

            response.write(out)
            return response

        elif "upload_sensor_csv" in request.POST:
            """
            Evaluate data upload containing sensor information to
            update existing sensors and register new sensors in KnownSensors.
            """

            sensor_upload_form = uploadSensorConfigForm(request.POST, request.FILES)
            if sensor_upload_form.is_valid():
                if "file" in request.FILES.keys():
                    request.FILES["file"]

            sensor_name_forms = []
            for f in KnownSensors.objects.all():
                sensor_name_form = modifySensorIdentifierForm(
                    f, prefix=str(f.SENSORserial)
                )
                sensor_name_forms.append(sensor_name_form)

        elif "sensor_names" in request.POST:
            # Modification of sensor information
            for form in sensor_name_forms:
                form = modifySensorIdentifierForm(
                    form.sensor, request.POST, prefix=str(form.sensor.SENSORserial)
                )
                if form.is_valid():
                    # get corresponding sensor
                    sensor = KnownSensors.objects.get(
                        SENSORserial=form.sensor.SENSORserial
                    )

                    if (
                        sensor.SENSORidentifier != form.cleaned_data["identifier"]
                        or sensor.Color != form.cleaned_data["color"]
                        or sensor.x != form.cleaned_data["x"]
                        or sensor.y != form.cleaned_data["y"]
                        or sensor.z != form.cleaned_data["z"]
                    ):
                        # Set value
                        sensor.Color = form.cleaned_data["color"]
                        sensor.SENSORidentifier = form.cleaned_data["identifier"]
                        sensor.x = form.cleaned_data["x"]
                        sensor.y = form.cleaned_data["y"]
                        sensor.z = form.cleaned_data["z"]
                        sensor.save()

                        # write change to log
                        message = f"{sensor.SENSORidentifier} -> {sensor.SENSORserial}"
                        log = Log(message=message, typ="Sensor")
                        log.save()

                    if f"plots-{sensor.SENSORserial}" in request.POST:
                        plot_selection = request.POST.getlist(
                            f"plots-{sensor.SENSORserial}"
                        )
                        sensor.plots.all().update(show=False)
                        for qty in plot_selection:
                            if not sensor.plots.filter(qty=qty).exists():
                                continue
                            plot = sensor.plots.get(qty=qty)
                            plot.show = True
                            plot.save()

            sensor_name_forms = [
                modifySensorIdentifierForm(f, prefix=str(f.SENSORserial))
                for f in KnownSensors.objects.all()
            ]

        elif "delete_measurement" in request.POST:
            # Set selected measurement ID for deletion
            measurement_id = request.POST["delete_measurement"]

            # Do nothing if measurement id is unknown
            if MeasurementData.objects.filter(id=measurement_id).exists():
                # Delete single measurement files in /media/results/ folder
                selected_measurement = MeasurementData.objects.get(id=measurement_id)

                for filename in os.listdir(results_path):
                    # Check for supported file format and correct file name
                    for fmt in dataExport.export_formats:
                        suffix = "." + fmt["extension"]
                        if filename.startswith(
                            selected_measurement.name
                        ) and filename.endswith(suffix):
                            os.remove(os.path.join(results_path, filename))

                    if filename.startswith(
                        selected_measurement.name
                    ) and filename.endswith(".zip"):
                        os.remove(os.path.join(results_path, filename))

                # Prepare log entry
                message = f"Deleted measurement with {selected_measurement.name} - ID{measurement_id}"
                log = Log(
                    message=f"Deleted measurement with {selected_measurement.name} - ID{measurement_id}",
                    typ="Event",
                )

                # Delete single measurement in Settings/Data.
                selected_measurement.delete()
            else:
                log = Log(message=f"Failed to delete {measurement_id}", typ="Error")
            log.save()

        elif "delete_all_data" in request.POST:
            # Delete all measurement in Settings/Data.
            MeasurementData.objects.all().delete()

            # Delete all measurement files in /media/results/ folder
            for filename in os.listdir(results_path):
                # Check for supported file formats
                for fmt in dataExport.export_formats:
                    suffix = "." + fmt["extension"]
                    if filename.endswith(suffix):
                        os.remove(os.path.join(results_path, filename))
                # Check for zip extension
                if filename.endswith(".zip"):
                    os.remove(os.path.join(results_path, filename))

        elif "delete_all_log" in request.POST:
            # Delete all measurement in Settings/Data.
            Log.objects.all().delete()

        elif "systemSettings" in request.POST:
            # Modify measurement system settings
            measurement_settings_form = measurementSettingsForm(
                request.POST, instance=data
            )
            if measurement_settings_form.is_valid():
                measurement_settings_form.save()

        elif "daq_mode" in request.POST:
            # Update data aquisition mode (server/stream)
            measurement_mode_form = measurementModeForm(request.POST, instance=data)
            if measurement_mode_form.is_valid():
                measurement_mode_form.save()
                for mode in Measurement.MODES:
                    if mode[0] == data.daq_mode:
                        message = f"Switched to {mode[1]}"
                        log = Log(message=message, typ="Event")
                if message == "":
                    message = "[ERROR] Unknown measurement mode!"
                    log = Log(message=message, typ="Error")
                log.save()

        elif "mqttSettings" in request.POST:
            # Update mqtt server settings
            mqtt_settings_form = mqttSettingsForm(
                request.POST, instance=MQTT.objects.last()
            )
            if mqtt_settings_form.is_valid():
                mqtt_data = mqtt_settings_form.save()
                message = (
                    f"Topic: {mqtt_data.topic} Host: {mqtt_data.host}:{mqtt_data.port}"
                )
                if hasattr(mqtt_data, "user"):
                    message += f" User: {mqtt_data.user}"
                Log(message=message, typ="MQTT").save()
                mqtt_client = MQTTClient()

        elif "save" in request.POST:
            cmd = request.POST["save"]

            data_id, output_format = cmd.split("_")
            data = MeasurementData.objects.get(id=data_id)

            zip_path = dataExport(
                data, file_format=output_format, unified_timescale=unified_timescale
            ).zip_file
            zip_name = zip_path.split("/")[-1]

            with open(zip_path, "rb") as fp:
                data = fp.read()
                response = HttpResponse(content_type="application/ms-excel")
                # force browser to download file
                response["Content-Disposition"] = "attachment; filename=%s" % zip_name
                response.write(data)
            return response

    measurement_ids = MeasurementData.objects.all().values_list("id", flat=True)
    measurement_names = MeasurementData.objects.all().values_list("name", flat=True)
    measurement_starts = MeasurementData.objects.all().values_list(
        "start_measure", flat=True
    )
    measurement_stops = MeasurementData.objects.all().values_list(
        "stop_measure", flat=True
    )
    measurement_information = zip(
        measurement_ids, measurement_names, measurement_starts, measurement_stops
    )
    mqtt = MQTT.objects.last()

    mms_info = hardware_information() | software_information()

    context = {
        "export_formats": dataExport.export_formats,
        "sensor_name_forms": sensor_name_forms,
        "unit_names": unit_names,
        "measurement_information": measurement_information,
        "mms_info": mms_info,
        "log": Log.objects.order_by("-id")[:20],
        "space": free,
        "measurement_mode_form": measurement_mode_form,
        "mqtt_settings_form": mqtt_settings_form,
        "mqtt": mqtt,
        "measurement_settings_form": measurement_settings_form,
        "mux_leading_addr": [str(i) for i in range(8)],
        "mux_trailing_addr": [str(hex(i))[-1] for i in range(16)],
        "sensor_upload_form": sensor_upload_form,
        "tabname": tabname,
    }
    return render(request, "measurement_server/settings.html", context)
