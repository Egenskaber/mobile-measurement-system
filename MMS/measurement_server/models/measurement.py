# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Holds all models used to generate the API
and to communicate between Worker and GUI
"""

from colorful.fields import RGBColorField
from django.db import models
from django.utils.timezone import now
from django.core.validators import RegexValidator
from django.contrib.contenttypes.fields import GenericRelation
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from .calibration import Calibration
from .units import Units

DEFAULT_SENSOR_ID = 1


class MQTTModel(models.Model):
    """
    This model is used as a base model for this project.
    All models which are indended to broadcast updates
    to a MQTT broker shall inherit from this abstract model.
    While preserving all attributes of its children this class
    extends the save method such that data written to the database
    will also be published to a MQTTClient instance .
    """

    maintopic = None

    def save(self, *args, mqtt=None, units=None, **kwargs):
        super().save(*args, **kwargs)
        if mqtt is not None:
            fields = self._meta.get_fields()

            for field in fields:
                if isinstance(field, models.ManyToOneRel):
                    continue
                if isinstance(field, models.fields.related.ManyToManyField):
                    continue
                if isinstance(field, models.ForeignKey):
                    continue

                if units is not None and units.all().filter(qty=field.name).exists():
                    # Update value
                    topic = f"{self.maintopic}/{field.name}/value"
                    msg = getattr(self, field.name)
                    mqtt.publish(topic, str(msg))

                    # Update unit
                    unit = units.all().get(qty=field.name)
                    topic = f"{self.maintopic}/{field.name}/unit"
                    mqtt.publish(topic, str(unit.sign))
                else:
                    topic = f"{self.maintopic}/{field.name}"
                    msg = getattr(self, field.name)
                    mqtt.publish(topic, str(msg))

    class Meta:
        abstract = True


class Measurement(MQTTModel):
    """
    Stores all relevant information about the current measurement
    """

    alphanumeric = RegexValidator(
        r"^[0-9a-zA-Z_]*$", "Only alphanumeric characters are allowed."
    )
    class_date = timezone.localtime(now())

    # Title of most recent measurement
    name = models.CharField(
        max_length=220,
        default="unnamed_measurement",
        validators=[alphanumeric],
    )
    # Creaion time of measurement entry
    created = models.DateTimeField(auto_now=True)
    # active indicates if this measurement is currenty running
    active = models.BooleanField(default=False)
    # Scheduled measurement begin.
    start_measure = models.DateTimeField(default=now)
    # Scheduled measurement Stop.
    stop_measure = models.DateTimeField(default=now)
    # Check if a measurement is stopped.
    stopped = models.BooleanField(default=False)
    # Check if unified timescale is active for netCDF files
    unified_timescale = models.BooleanField(default=False)
    # Current MMS version
    mms_version = models.CharField(max_length=16, default="0.0.0")
    # Current Git commit
    git_commit = models.CharField(max_length=16, default="")
    # Result URL
    result_id = models.IntegerField(default=0)
    # Space warning
    disk_space = models.IntegerField(default=0)
    # Period used for plot downsampling [seconds]
    avg_bucket_size = models.IntegerField(default=5)
    # Last n minutes displayed in a single plot (0 == all values)
    plot_period = models.IntegerField(default=10)
    # Number of samples for mean calculation of live data view
    live_mean = models.IntegerField(default=10)
    # Number of connected com ports
    connected_comports = models.IntegerField(default=0)
    # Number of connected ftdi
    connected_ftdi = models.IntegerField(default=0)
    # Number of sensors connected to FTDIs
    connected_ftdi_sensors = models.IntegerField(default=0)
    # Number of sensors connected to ComPorts
    connected_com_sensors = models.IntegerField(default=0)
    # Number of connected sensors
    connected_sensors = models.IntegerField(default=0)
    # Maximum time a sensor may be silent before a warning is fired
    warning_time = models.IntegerField(default=5)
    # Live view table height [px]
    live_view_table_height = models.IntegerField(default=400)
    # Data acquisition mode
    MODES = [
        ("SR", "Server"),  # store data on mashine
        ("ST", "Stream"),  # no data storing just serving for external mashines
    ]
    daq_mode = models.CharField(max_length=2, choices=MODES, default="SR")
    # Serial interface mode
    SERIAL_MODES = [
        ("SPI", "SPI"),  # store data on mashine
        ("I2C", "I2C"),  # no data storing just serving for external mashines
        ("COM", "COM"),  # no data storing just serving for external mashines
    ]
    serial_mode = models.CharField(max_length=3, choices=SERIAL_MODES, default="")
    maintopic = "measurement"
    # -1 for no restriction
    max_freq = models.DecimalField(
        null=True, default=-1, max_digits=5, decimal_places=2
    )
    # Decides if 4 CS pings are used on a multiplexer or as direct cs
    SPI_MUX_MODES = [("MUX", "Multiplexer"), ("GPIO", "GPIO")]

    spi_mux_mode = models.CharField(max_length=4, choices=SPI_MUX_MODES, default="MUX")

    def __str__(self):
        return self.name + "@" + self.daq_mode


class MUXAddress(models.Model):
    port = models.CharField(max_length=2, default="70")


class PlotSettings(models.Model):
    qty = models.CharField(max_length=25, default="")
    show = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.qty}: {self.show}"


class MQTTResultModel(MQTTModel):
    def save(self, *args, **kwargs):
        # collect all calibration fields

        calibrations = self.results.calibration.all()

        # When a new result is stored check if an applicable calibration exists
        # If possible apply the calibration and check if the result works with
        # the database field.
        for calib in calibrations:
            if calib:
                calib_eq = calib.eval
                qty = calib.qty.qty
                field = self._meta.get_field(qty)
                value = getattr(self, qty)
                calib_res = calib_eq(
                    value,
                    max_digits=field.max_digits,
                    decimal_places=field.decimal_places,
                )
                setattr(self, qty, calib_res)

        obj_name = self.results.__class__.__name__
        self.maintopic = f"{obj_name}/{self.results.SENSORserial}"
        super().save(*args, units=self.results.units, **kwargs)

    class Meta:
        abstract = True


class MQTTSensorModel(MQTTModel):
    calibration = GenericRelation(
        Calibration,
        related_query_name="%(app_label)s_%(class)s",
        on_delete=models.PROTECT,
    )

    def save(self, *args, **kwargs):
        obj_name = self.__class__.__name__
        self.maintopic = f"{obj_name}/{self.SENSORserial}"
        super().save(*args, **kwargs)

    class Meta:
        abstract = True

    def add_calibration(self, **kwargs) -> "Calibration":
        content_type = ContentType.objects.get_for_model(self)

        if kwargs["qty"].pk not in self.calibration.values_list("qty", flat=True):
            return Calibration.objects.create(
                sensor=self, content_type=content_type, object_id=self.pk, **kwargs
            )
        calib = self.calibration.filter(qty__qty=kwargs["qty"].qty)
        calib.update(**kwargs)
        return calib


class AbstractSensor(MQTTSensorModel):
    """
    Defines mixin for sensor database
    """

    FTDIserial = models.CharField(max_length=60, default="")
    MUXaddress = models.IntegerField(default=0)
    MUXport = models.IntegerField(default=0)
    COMport = models.CharField(max_length=60, default="")
    CS = models.IntegerField(default=0)
    SENSORtype = models.CharField(max_length=20, default="unk")
    SENSORserial = models.CharField(max_length=20, default="")
    Color = RGBColorField(default="#000000")
    SENSORidentifier = models.CharField(max_length=220, default="")
    connected = models.BooleanField(default=False)
    units = models.ManyToManyField(Units, blank=True)
    plots = models.ManyToManyField(PlotSettings, blank=True)
    # Sensor position
    x = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    y = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    z = models.DecimalField(default=0, max_digits=7, decimal_places=2)
    # Serial interface mode
    SERIAL_MODES = [
        ("SPI", "SPI"),
        ("I2C", "I2C"),
        ("COM", "COM"),
    ]
    serial_mode = models.CharField(max_length=3, choices=SERIAL_MODES, default="")

    class Meta:
        abstract = True

    def __str__(self):
        return f"FTDI: {self.FTDIserial} \t\
                 MUX: {self.MUXaddress}:{self.MUXport}"


class KnownSensors(AbstractSensor):
    """
    Holds information about all sensors that were connected to the system.
    """

    time = models.DateTimeField(default=now)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        calibrations = self.calibration.all()
        all_quantities = list(calibrations.values_list("qty__qty", flat=True))
        doubled_quantities = set(
            [x for x in all_quantities if all_quantities.count(x) > 1]
        )

        if doubled_quantities:
            for dqty in doubled_quantities:
                entries = calibrations.filter(qty__qty=dqty)
                if entries[0].time > entries[1].time:
                    self.calibration.remove(entries[1])
                else:
                    self.calibration.remove(entries[0])


class MeasurementData(MQTTModel):
    """
    Holds persisting measurmeent results
    """

    maintopic = "measurementdata"
    name = models.CharField(max_length=220, default="Measurement tag")
    created = models.DateTimeField(auto_now=True)
    start_measure = models.DateTimeField(default=now)
    stop_measure = models.DateTimeField(default=now)
    stopped = models.BooleanField(default=False)


# Result classes
class MAX31865(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class MAX31865results(MQTTResultModel):
    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    results = models.ForeignKey(
        MAX31865, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SDP8(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SDP8results(MQTTResultModel):
    """
    Stores measurement values of SDP8 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    diff_pressure = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    results = models.ForeignKey(
        SDP8, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SHT85(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SHT85results(MQTTResultModel):
    """
    Stores measurement values of SHT85 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    humidity = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    results = models.ForeignKey(
        SHT85, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SCD30(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SCD30results(MQTTResultModel):
    """
    Stores measurement values of SCD30 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    humidity = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    co2 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=0)
    results = models.ForeignKey(
        SCD30, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )

    def save(self, *args, **kwargs):
        self.maintopic = f"SCD30/{self.results.SENSORserial}"
        super().save(*args, **kwargs)


class SCD41(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SCD41results(MQTTResultModel):
    """
    Stores measurement values of SCD30 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    humidity = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    co2 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=0)
    results = models.ForeignKey(
        SCD41, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )

    def save(self, *args, **kwargs):
        self.maintopic = f"SCD41/{self.results.SENSORserial}"
        super().save(*args, **kwargs)


class ADS1015(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class ADS1015results(MQTTResultModel):
    """
    Stores measurement values of ADS1015 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    dU01 = models.DecimalField(null=True, default=None, max_digits=8, decimal_places=2)
    results = models.ForeignKey(
        ADS1015, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )

    def save(self, *args, **kwargs):
        self.maintopic = f"ADS1015/{self.results.SENSORserial}"
        super().save(*args, **kwargs)


class NAU7802(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class NAU7802results(MQTTResultModel):
    """
    Stores measurement values of NAU7802 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    adc = models.DecimalField(null=True, default=None, max_digits=12, decimal_places=2)
    results = models.ForeignKey(
        NAU7802, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )

    def save(self, *args, **kwargs):
        self.maintopic = f"NAU7802/{self.results.SENSORserial}"
        super().save(*args, **kwargs)


class SMGair2(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SMGair2results(MQTTResultModel):
    """
    Stores measurement values of BME280 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    abs_pressure = models.DecimalField(
        null=True, default=None, max_digits=12, decimal_places=2
    )
    volumeflow = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    density = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    results = models.ForeignKey(
        SMGair2, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class S8000(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class S8000results(MQTTResultModel):
    """
    Stores measurement values of S8000 dew point mirrors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    dewpoint = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    results = models.ForeignKey(
        S8000, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class Optidew(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class Optidewresults(MQTTResultModel):
    """
    Stores measurement values of Optidew dew point mirrors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    dewpoint = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    humidity = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    state = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    results = models.ForeignKey(
        Optidew, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SPS30(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SPS30results(MQTTResultModel):
    """
    Stores measurement values of SPS30 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    m_03_to_1_0_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    m_03_to_2_5_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    m_03_to_4_0_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    m_03_to_10_0_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    n_03_to_0_5_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    n_03_to_1_0_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    n_03_to_2_5_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    n_03_to_4_0_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    n_03_to_10_0_mu = models.DecimalField(
        null=True, default=None, max_digits=9, decimal_places=2
    )
    tps = models.DecimalField(null=True, default=None, max_digits=9, decimal_places=2)

    results = models.ForeignKey(
        SPS30, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class BME280(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class BME280results(MQTTResultModel):
    """
    Stores measurement values of BME280 sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    humidity = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    temperature = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    abs_pressure = models.DecimalField(
        null=True, default=None, max_digits=12, decimal_places=2
    )
    results = models.ForeignKey(
        BME280, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SFM(AbstractSensor):
    sensor = models.ForeignKey(
        MeasurementData, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )


class SFMresults(MQTTResultModel):
    """
    Stores measurement values of SFM sensors.
    """

    time = models.DateTimeField(default=now, primary_key=True)
    volumeflow = models.DecimalField(
        null=True, default=None, max_digits=5, decimal_places=2
    )
    results = models.ForeignKey(
        SFM, on_delete=models.CASCADE, default=DEFAULT_SENSOR_ID
    )
