# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

types = {"sensor": "Sensors", "peripheral": "Periphery"}
downloads = [
    {
        "url": "/datasheets/pdf/ads1015.pdf",
        "image": "datasheets/img/ads1015.jpg",
        "name": "ADS1015",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/nau7802.pdf",
        "image": "datasheets/img/nau7802.jpg",
        "name": "NAU7802 / Qwiic Scale",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/mux_tca9548a.pdf",
        "image": "datasheets/img/mux_tca9548a.jpg",
        "name": "Multiplexer TCA9548a",
        "type": "peripheral",
    },
    {
        "url": "/datasheets/pdf/sensirion_sfm3000.pdf",
        "image": "datasheets/img/sensirion_sfm3000.png",
        "name": "Massenflussmesser SFM3000",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/sensirion_sht85.pdf",
        "image": "datasheets/img/sensirion_sht85.png",
        "name": "SHT 85",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/ftdi_um232h_b.pdf",
        "image": "datasheets/img/ftdi_um232h_b.png",
        "name": "FTDI UM232H B",
        "type": "peripheral",
    },
    {
        "url": "/datasheets/pdf/ftdi_um232h.pdf",
        "image": "datasheets/img/ftdi_um232h.png",
        "name": "FTDI UM232H",
        "type": "peripheral",
    },
    {
        "url": "/datasheets/pdf/adafruit_ft232h.pdf",
        "image": "datasheets/img/adafruit_ft232h.jpg",
        "name": "Adafruit FT232H",
        "type": "peripheral",
    },
    {
        "url": "/datasheets/pdf/seedstudio_ds3231.pdf",
        "image": "datasheets/img/seedstudio_ds3231.png",
        "name": "RTC-Uhrenmodul DS3231",
        "type": "peripheral",
    },
    {
        "url": "/datasheets/pdf/bosch_bme280.pdf",
        "image": "datasheets/img/bme280.jpg",
        "name": "Bosch BME280",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/maximintegrated_max31865.pdf",
        "image": "datasheets/img/adafruit_max31865.jpg",
        "name": "Adafruit MAX 31865 RTD",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/sensirion_sdp8xx.pdf",
        "image": "datasheets/img/sensirion_sdp8xx.jpg",
        "name": "Sensirion SDP 810",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/michell_s8000.pdf",
        "image": "datasheets/img/michell_s8000.jpg",
        "name": "S8000 dew point mirror",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/sensirion_sps30.pdf",
        "image": "datasheets/img/sensirion_sps30.jpg",
        "name": "Sensirion SPS 30",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/sensirion_scd30.pdf",
        "image": "datasheets/img/sensirion_scd30.jpg",
        "name": "Sensirion SCD30",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/sensirion_scd41.pdf",
        "image": "datasheets/img/sensirion_scd41.png",
        "name": "Sensirion SCD41",
        "type": "sensor",
    },
    {
        "url": "/datasheets/pdf/michell_optidew.pdf",
        "image": "datasheets/img/michell_optidew.gif",
        "name": "Optidew dew point mirror",
        "type": "sensor",
    },
]
