"""
With one sensor connected check if read sensors works
"""

import mock
from django.test import TestCase
from unittest.mock import Mock

# from measurement_server.models.init import Init
from core.management.commands.readSensor import Command
from measurement_server.models.init import Init
from measurement_server.models.log import Log
from django.db import connection
import timeout_decorator
import json
import copy
import os
from measurement_server.models.measurement import SHT85
from measurement_server.models.measurement import Units
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import Measurement
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.calibration import Calibration
from usb.core import USBError


@timeout_decorator.timeout(0.4, timeout_exception=StopIteration)
def wait_for_db(cmd):
    cmd.wait_for_db(t=0.1)


class ReadSensorTests(TestCase):
    def setUp(self):
        pass

    def test_wait_for_db(self):
        cmd = Command()
        # Is a working connection identified correctly?
        self.assertTrue(cmd.wait_for_db())
        # Is a broken connection identified correctly=
        connection.creation.destroy_test_db(keepdb=True)
        with self.assertRaises(StopIteration):
            wait_for_db(cmd)

    def test_color_gen(self):
        cmd = Command()
        self.assertTrue(hasattr(cmd, "gen_color"))

        # Use cases
        c = int(cmd.gen_color(n=1)[1:], 16)
        self.assertLessEqual(c, 0x010101)
        c = int(cmd.gen_color(n=16)[1:], 16)
        self.assertLessEqual(c, 0x101010)
        self.assertEqual(cmd.gen_color(n=0), cmd.gen_color(n=1))
        c = int(cmd.gen_color(n=255)[1:], 16)
        self.assertLessEqual(c, 0xFFFFFF)
        # Edge case
        self.assertEqual(cmd.gen_color(n=0), "#000000")

    def test_get_ftdi_count(self):
        """
        This test expects one FTDI+MUX+SFM3000
        """

        cmd = Command()
        self.assertTrue(hasattr(cmd, "getFTDIcount"))

        def side_effect_usberr(*args, **kwargs):
            raise USBError("Dummy")

        with mock.patch(
            "core.management.commands.readSensor.usbtools.UsbTools.find_all"
        ) as MockUSB:
            MockUSB.return_value = [0] * 2
            n = cmd.getFTDIcount()
            self.assertEqual(n, 2)

        with mock.patch(
            "core.management.commands.readSensor.usbtools.UsbTools.find_all"
        ) as MockUSB:
            MockUSB.side_effect_usberr = side_effect_usberr
            n = cmd.getFTDIcount()
            self.assertEqual(n, 0)

    def test_compute_measurement_data_average_single_sensor(self):
        # Load a sensor value dict from file
        # Compile a data set using artificial values
        # Check if the computed average is as expected
        cmd = Command()

        self.assertTrue(hasattr(cmd, "compute_measurement_data_average"))

        # Fetch sample dict.
        real_path = os.path.realpath(__file__)
        cwd = os.path.dirname(real_path)
        with open(f"{cwd}/test_data/sht_output_dict.json", "r") as f:
            sample_data = json.load(f)
        self.assertIsInstance(sample_data, dict)

        # Generate data set with only valid numbers.
        sn = sample_data["SENSORserial"]
        data_set = {sn: copy.deepcopy(sample_data)}

        # Make it a set.
        data_set[sn]["values"] = []

        # Prepare artificial data set
        n = 42
        expected_mean = (n - 1) / 2

        # Add elements to value set
        for i in range(n):
            for qty in sample_data["values"].keys():
                sample_data["values"][qty]["value"] = i
            data_set[sample_data["SENSORserial"]]["values"] += [
                copy.deepcopy(sample_data["values"])
            ]

        # Evaluate data set mean
        computed_data = cmd.compute_measurement_data_average(data_set)
        for qty in computed_data[sn]["values"].keys():
            self.assertEqual(computed_data[sn]["values"][qty]["value"], expected_mean)

    def test_compute_measurement_data_average_muliple_sensors_all_errors(self):
        # In measurement run all non valid values are filtered if at least
        # one valid value has been recorded
        # compute_measurement_data_average should return a dictionary
        # with serial_numbers as keys and error element entries.
        cmd = Command()

        self.assertTrue(hasattr(cmd, "compute_measurement_data_average"))

        # Fetch sample dict.
        real_path = os.path.realpath(__file__)
        cwd = os.path.dirname(real_path)

        # Compile artificial sht sensor values
        with open(f"{cwd}/test_data/sht_output_err_dict.json", "r") as f:
            sample_data = json.load(f)
        self.assertIsInstance(sample_data, dict)

        # Generate data for one sensor.
        sn = sample_data["SENSORserial"]
        data_set = {sn: copy.deepcopy(sample_data)}

        # Make it a set.
        data_set[sn]["values"] = []

        # Add elements to value set

        # Compile artificial sfm sensor values
        sn = "123456789"
        data_set[sn] = copy.deepcopy(sample_data)

        expected_data = data_set

        # Evaluate data set mean
        computed_data = cmd.compute_measurement_data_average(data_set)
        self.assertDictEqual(computed_data, expected_data)

    def test_compute_measurement_data_average_muliple_sensors(self):
        # Load a sensor value dict from file
        # Compile a data set using artificial values
        # note that errors during value acquisition may cause different
        # value counts.
        # Check if the computed average is as expected
        cmd = Command()
        expected_mean = {}

        self.assertTrue(hasattr(cmd, "compute_measurement_data_average"))

        # Fetch sample dict.
        real_path = os.path.realpath(__file__)
        cwd = os.path.dirname(real_path)

        # Compile artificial sht sensor values
        with open(f"{cwd}/test_data/sht_output_dict.json", "r") as f:
            sample_data = json.load(f)
        self.assertIsInstance(sample_data, dict)

        # Generate data for one sensor.
        sn = sample_data["SENSORserial"]
        data_set = {sn: copy.deepcopy(sample_data)}

        # Make it a set.
        data_set[sn]["values"] = []

        # Prepare artificial data set
        n = 42
        expected_mean[sn] = (n - 1) / 2

        # Add elements to value set
        for i in range(n):
            for qty in sample_data["values"].keys():
                sample_data["values"][qty]["value"] = i
            data_set[sn]["values"] += [copy.deepcopy(sample_data["values"])]

        # Compile artificial sfm sensor values
        with open(f"{cwd}/test_data/sfm_output_dict.json", "r") as f:
            sample_data = json.load(f)
        self.assertIsInstance(sample_data, dict)

        # Generate data for one sensor.
        sn = sample_data["SENSORserial"]
        data_set[sn] = copy.deepcopy(sample_data)

        # Make it a set.
        data_set[sn]["values"] = []

        # Prepare artificial data set
        n = 11
        expected_mean[sn] = (n - 1) / 2

        # Add elements to value set
        for i in range(n):
            for qty in sample_data["values"].keys():
                sample_data["values"][qty]["value"] = i
            data_set[sn]["values"] += [copy.deepcopy(sample_data["values"])]

        # Evaluate data set mean
        computed_data = cmd.compute_measurement_data_average(data_set)
        for qty in computed_data[sn]["values"].keys():
            self.assertEqual(
                computed_data[sn]["values"][qty]["value"], expected_mean[sn]
            )

    def test_if_auto_init_on_boot_with_an_emtpy_database(self):
        # Fresh database no Init instance present:
        cmd = Command()
        state = cmd.check_for_init_request()
        self.assertTrue(state)
        self.assertEqual(Init.objects.all().count(), 1)

    def test_if_auto_init_on_boot_with_a_populated_database(self):
        cmd = Command()
        init = Init()
        init.save()
        self.assertFalse(cmd.check_for_init_request())
        init.init = True
        init.save()
        self.assertTrue(cmd.check_for_init_request())

    def test_initialize_sensor(self):
        # sensor = KnownSensors.objects.create(id=None, SENSORserial=sn)
        # sensor.qty.add(unit)
        # sensor.add_calibration(x1=1, qty=unit)
        # sensor.save()

        cmd = Command()
        self.assertTrue(hasattr(cmd, "initialize_sensor"))

        result = MeasurementData.objects.create()

        # Mock a sensor
        sensor = Mock()
        sensor.__name__ = "SHT85"
        sensor._serial_mode = "I2C"
        sensor.ftdi_serial = "HeMan"
        sensor.MUXport = 0
        sensor.mux_port = 0
        sensor.serial_number = 123456
        sensor.mux.address = 0x70
        sensor._units = {"humidity": "pct", "temperature": "C"}

        cmd.register_sensor(sensor)

        # Check if sensor is added to the list of known sensors
        self.assertEqual(KnownSensors.objects.all().count(), 1)
        registered_sensor = KnownSensors.objects.first()

        # Add calibrations to a registered sensor
        registered_sensor.add_calibration(x1=2, qty=registered_sensor.units.first())
        registered_sensor.save()

        self.assertEqual(Calibration.objects.all().count(), 1)
        self.assertEqual(Calibration.objects.filter(readonly=False).count(), 1)

        # Simulate a measurement preparation and check if a new calibration
        # object is created
        cmd.initialize_sensor(sensor, result)
        self.assertEqual(SHT85.objects.all().count(), 1)
        self.assertEqual(Calibration.objects.all().count(), 2)
        self.assertEqual(Calibration.objects.filter(readonly=False).count(), 1)

        # Check writing protection of calibrations used in measurements
        c = registered_sensor.calibration.last()
        c.x1 += 1
        c.save()
        self.assertEqual(SHT85.objects.last().calibration.last().x1, 2)

    def test_initFTDI(self):
        cmd = Command()

        # Mock a sensor
        sensor = Mock()
        sensor.__name__ = "SHT85"
        sensor._serial_mode = "I2C"
        sensor.ftdi_serial = "HeMan"
        sensor.MUXport = 0
        sensor.mux_port = 0
        sensor.serial_number = 123456
        sensor.mux.address = 0x70
        sensor._units = {"humidity": "pct", "temperature": "C"}

        # Generate system state
        mr = Measurement()
        mr.spi_mux_mode = "MUX"
        mr.save()

        cmd.register_sensor(sensor)

        def side_effect_usberr(*args, **kwargs):
            raise USBError("Dummy")

        # No FTDI connected
        with mock.patch("core.management.commands.readSensor.init_all") as MockInit:
            MockInit.return_value = []
            val = cmd.initFTDI()
            self.assertFalse(val)

        # USBError
        with mock.patch("core.management.commands.readSensor.init_all") as MockInit:
            MockInit.side_effect = side_effect_usberr
            val = cmd.initFTDI()
            self.assertFalse(val)
            self.assertIn("Lost", Log.objects.last().message)

        # 2 FTDI connected
        ftdi1 = Mock()
        ftdi2 = Mock()
        with mock.patch("core.management.commands.readSensor.init_all") as MockInit:
            with mock.patch("core.management.commands.readSensor.setup") as MockSetup:
                MockInit.return_value = [ftdi1, ftdi2]
                MockSetup.return_value = None
                val = cmd.initFTDI()
                self.assertTrue(val)

    def tearDown(self):
        pass
