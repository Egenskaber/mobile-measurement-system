# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from pyserialsensors.devices.ads1015 import ADS1015 as ADS1015_sensor
from pyserialsensors.devices.bme280 import BME280 as BME280_sensor
from pyserialsensors.devices.sfm3xxx import SFM as SFM_sensor
from pyserialsensors.devices.shtxx import SHT85 as SHT85_sensor
from pyserialsensors.devices.sdp8xx import SDP8 as SDP8_sensor
from pyserialsensors.devices.scd30 import SCD30 as SCD30_sensor
from pyserialsensors.devices.scd41 import SCD41 as SCD41_sensor
from pyserialsensors.devices.sps30 import SPS30 as SPS30_sensor
from pyserialsensors.devices.max31865 import MAX31865 as MAX31865_sensor
from pyserialsensors.devices.nau7802 import NAU7802 as NAU7802_sensor
from pyserialsensors.devices.smgair2 import SMGair2 as SMGair2_sensor
from pyserialsensors.devices.s8000 import S8000 as S8000_sensor
from pyserialsensors.devices.optidew import Optidew as Optidew_sensor

from measurement_server.models.measurement import SMGair2
from measurement_server.models.measurement import SMGair2results
from measurement_server.models.measurement import NAU7802
from measurement_server.models.measurement import NAU7802results
from measurement_server.models.measurement import SPS30
from measurement_server.models.measurement import SPS30results
from measurement_server.models.measurement import SCD30
from measurement_server.models.measurement import SCD30results
from measurement_server.models.measurement import SCD41
from measurement_server.models.measurement import SCD41results
from measurement_server.models.measurement import SDP8
from measurement_server.models.measurement import SDP8results
from measurement_server.models.measurement import SHT85
from measurement_server.models.measurement import SHT85results
from measurement_server.models.measurement import ADS1015
from measurement_server.models.measurement import ADS1015results
from measurement_server.models.measurement import BME280
from measurement_server.models.measurement import BME280results
from measurement_server.models.measurement import SFM
from measurement_server.models.measurement import SFMresults
from measurement_server.models.measurement import MAX31865
from measurement_server.models.measurement import MAX31865results
from measurement_server.models.measurement import S8000
from measurement_server.models.measurement import S8000results
from measurement_server.models.measurement import Optidew
from measurement_server.models.measurement import Optidewresults

from measurement_server.sensor_serializers import (
    SHT85Serializer,
    SHT85resultsSerializer,
    ADS1015Serializer,
    ADS1015resultsSerializer,
    NAU7802Serializer,
    NAU7802resultsSerializer,
    SFMSerializer,
    SFMresultsSerializer,
    MAX31865Serializer,
    MAX31865resultsSerializer,
    SDP8Serializer,
    SDP8resultsSerializer,
    BME280Serializer,
    BME280resultsSerializer,
    SCD30Serializer,
    SCD30resultsSerializer,
    SCD41Serializer,
    SCD41resultsSerializer,
    SPS30Serializer,
    SPS30resultsSerializer,
    SMGair2resultsSerializer,
    SMGair2Serializer,
    S8000Serializer,
    S8000resultsSerializer,
    OptidewSerializer,
    OptidewresultsSerializer,
)

SupportedSensors = {
    "NAU7802": {
        "sensor": NAU7802_sensor,
        "sensor_serializer": NAU7802Serializer,
        "model": {
            "base": NAU7802,
            "res": NAU7802results,
            "res_serializer": NAU7802resultsSerializer,
        },
    },
    "MAX31865": {
        "sensor": MAX31865_sensor,
        "sensor_serializer": MAX31865Serializer,
        "model": {
            "base": MAX31865,
            "res": MAX31865results,
            "res_serializer": MAX31865resultsSerializer,
        },
    },
    "S8000": {
        "sensor": S8000_sensor,
        "sensor_serializer": S8000Serializer,
        "model": {
            "base": S8000,
            "res": S8000results,
            "res_serializer": S8000resultsSerializer,
        },
    },
    "Optidew": {
        "sensor": Optidew_sensor,
        "sensor_serializer": OptidewSerializer,
        "model": {
            "base": Optidew,
            "res": Optidewresults,
            "res_serializer": OptidewresultsSerializer,
        },
    },
    "ADS1015": {
        "sensor": ADS1015_sensor,
        "sensor_serializer": ADS1015Serializer,
        "model": {
            "base": ADS1015,
            "res": ADS1015results,
            "res_serializer": ADS1015resultsSerializer,
        },
    },
    "BME280": {
        "sensor": BME280_sensor,
        "sensor_serializer": BME280Serializer,
        "model": {
            "base": BME280,
            "res": BME280results,
            "res_serializer": BME280resultsSerializer,
        },
    },
    "SFM": {
        "sensor": SFM_sensor,
        "sensor_serializer": SFMSerializer,
        "model": {
            "base": SFM,
            "res": SFMresults,
            "res_serializer": SFMresultsSerializer,
        },
    },
    "SHT85": {
        "sensor": SHT85_sensor,
        "sensor_serializer": SHT85Serializer,
        "model": {
            "base": SHT85,
            "res": SHT85results,
            "res_serializer": SHT85resultsSerializer,
        },
    },
    "SDP8": {
        "sensor": SDP8_sensor,
        "sensor_serializer": SDP8Serializer,
        "model": {
            "base": SDP8,
            "res": SDP8results,
            "res_serializer": SDP8resultsSerializer,
        },
    },
    "SCD30": {
        "sensor": SCD30_sensor,
        "sensor_serializer": SCD30Serializer,
        "model": {
            "base": SCD30,
            "res": SCD30results,
            "res_serializer": SCD30resultsSerializer,
        },
    },
    "SCD41": {
        "sensor": SCD41_sensor,
        "sensor_serializer": SCD41Serializer,
        "model": {
            "base": SCD41,
            "res": SCD41results,
            "res_serializer": SCD41resultsSerializer,
        },
    },
    "SPS30": {
        "sensor": SPS30_sensor,
        "sensor_serializer": SPS30Serializer,
        "model": {
            "base": SPS30,
            "res": SPS30results,
            "res_serializer": SPS30resultsSerializer,
        },
    },
    "SMGair2": {
        "sensor": SMGair2_sensor,
        "sensor_serializer": SMGair2Serializer,
        "model": {
            "base": SMGair2,
            "res": SMGair2results,
            "res_serializer": SMGair2resultsSerializer,
        },
    },
    "S8000": {
        "sensor": S8000_sensor,
        "sensor_serializer": S8000Serializer,
        "model": {
            "base": S8000,
            "res": S8000results,
            "res_serializer": S8000resultsSerializer,
        },
    },
}
