# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
This is a interface to the FTDI and sensor hardware. All state changes
are initiated by a change of a data model instance. All data model
instances are shared with the measurement_server which provides a GUI.
"""

# Python standards
import time
from django.utils.timezone import localtime
import os
import datetime
import shutil
from json import JSONDecodeError

# Django base classes
from django.core.management import BaseCommand
from django.utils.timezone import now

# check if on Raspberry pi
if os.uname()[4][:3] == "arm":
    from gpiozero import Button
    from gpiozero import LED
else:
    raise AttributeError("Case operation not possible for non Pi systems")


# Project Models
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import Measurement
from measurement_server.models.init import Init

# Button configuration.
PIN_LED_INIT = 7
PIN_BUTTON_INIT = 5
PIN_LED_MEASUREMENT = 27
PIN_BUTTON_MEASUREMENT = 10


class Command(BaseCommand):
    """
    Main commands for service worker
    """

    help = "Control LEDs and Buttons"

    def setup_pins(self):
        """
        Configure LEDs and Buttons.
        """
        self.led_init = LED(PIN_LED_INIT)
        self.btn_init = Button(PIN_BUTTON_INIT)
        self.led_measurement = LED(PIN_LED_MEASUREMENT)
        self.btn_measurement = Button(PIN_BUTTON_MEASUREMENT)

    def test_led(self):
        """
        check if all leds are connected correctly
        """
        self.led_measurement.on()
        self.led_init.on()
        time.sleep(1)
        self.led_measurement.off()
        time.sleep(1)
        self.led_init.off()

    def test_btn(self):
        """
        check if all buttons are connected correctly
        """
        self.btn_init.wait_for_press()
        self.led_init.on()
        self.btn_measurement.wait_for_press()
        self.led_measurement.on()

        time.sleep(1)
        self.led_measurement.off()
        self.led_init.off()

    def handle(self, *args, **options):
        self.setup_pins()
        init_blinking = False
        measure_blinking = False
        self.test_led()

        while True:
            # Start measurement via measurement button

            # If Measurement is true for stopped but has no result_id
            # the stopped has been set during init process
            if (
                self.btn_measurement.is_pressed
                and (
                    (
                        Measurement.objects.all().count() > 0
                        and Measurement.objects.last().active == False
                    )
                    or Measurement.objects.all().count() == 0
                    or (
                        Measurement.objects.last().result_id == 0
                        and Measurement.objects.last().stopped
                        and Measurement.objects.last().active == False
                    )
                )
                and Init.objects.get(id=1).state == "ok"
            ):
                if not Measurement.objects.exists():
                    new_measurement = Measurement()
                else:
                    new_measurement = Measurement.objects.last()

                # Setup new measurement
                new_measurement.stopped = False

                class_date = localtime(now())
                name = class_date.strftime("%Y%m%d_%H%M")

                start = now()
                duration = datetime.timedelta(days=100)

                total, used, free = shutil.disk_usage("/")
                free = free // 2**30

                new_measurement.name = name
                new_measurement.start_measure = start
                new_measurement.stop_measure = start + duration
                new_measurement.disk_space = free
                new_measurement.connected_sensors = KnownSensors.objects.filter(
                    connected=True
                ).count()
                new_measurement.save()
                self.led_measurement.blink(on_time=1, off_time=1, background=True)
                time.sleep(1)

            # Init sensors via init button
            init, created = Init.objects.get_or_create(id=1)

            if self.btn_init.is_pressed and (
                Measurement.objects.all().count() == 0
                or Measurement.objects.last().active == False
            ):

                def doInit(init):
                    init.init = True
                    init.init_ftdi = False
                    init.init_sensors = False
                    init.save()
                    self.led_init.blink(on_time=1, off_time=1, background=True)

                self.led_measurement.off()
                try:
                    if Measurement.objects.all().count() == 0:
                        doInit(init)
                    elif (
                        Measurement.objects.last() is not None
                        and Measurement.objects.last().active == False
                    ):
                        doInit(init)
                except AttributeError:
                    pass

            if init.init_ftdi and init.init_sensors:
                self.led_init.on()
                init_blinking = False

            if init.state == "run" and init_blinking == False:
                self.led_init.blink(on_time=1, off_time=1, background=True)
                init_blinking = True

            if init.error not in ["0", ""]:
                self.led_init.blink(on_time=0.1, off_time=0.1, background=True)
                self.led_measurement.blink(on_time=0.1, off_time=0.1, background=True)
                time.sleep(1)

            if Measurement.objects.all().count() > 0:
                # check if measurement was started via web gui
                try:
                    if (
                        Measurement.objects.last().active == True
                        and measure_blinking == False
                    ):
                        self.led_measurement.blink(
                            on_time=1, off_time=1, background=True
                        )
                        measure_blinking = True
                    elif Measurement.objects.last().active == False:
                        self.led_measurement.off()
                        measure_blinking = False
                except AttributeError:
                    pass

                if (
                    self.btn_measurement.is_pressed
                    and Measurement.objects.last().active == True
                ):
                    state = Measurement.objects.last()
                    state.active = False
                    state.stopped = True
                    state.save()
                    self.led_measurement.off()
                    measure_blinking = False
                    time.sleep(1)
                    if (
                        Measurement.objects.exists()
                        and measure_blinking == False
                        and Measurement.objects.last().active == True
                    ):
                        self.led_measurement.blink(
                            on_time=1, off_time=1, background=True
                        )
                        measure_blinking = True

                    elif Measurement.objects.last().active == False:
                        self.led_measurement.off()
                        measure_blinking = False
