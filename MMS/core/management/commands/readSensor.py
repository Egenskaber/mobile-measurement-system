# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
    This is a interface to the FTDI and sensor hardware. All state changes
    are initiated by a change of a data model instance. All data model
    instances are shared with the measurement_server which provides a GUI.
"""

# Python standards
import time
import usb
import copy
import datetime
import random
import logging

# Django base classes
from django.core.management import BaseCommand
from django.utils.timezone import now
from django.db.utils import OperationalError, InterfaceError

# Sensors
from ...modules.sensors import SupportedSensors
import pyftdi.usbtools as usbtools
from usb.core import USBError
from serial.tools import list_ports
from serial.serialutil import SerialTimeoutException

from core.modules.mqtt_client import MQTTClient

# Interface
from pyserialsensors.tools.bulk import setup
from pyserialsensors.tools.bulk import ThreadWithReturnValue
from pyserialsensors.devices.FTDI import init_all, UM232H
from pyserialsensors.core.i2controller import MmsI2cController
from pyserialsensors.core.i2controller import MmsSpiController
from pyserialsensors.core.comPortController import search_comports
from pyserialsensors.core.toolbox import scan_uart
from pyserialsensors.core.sensor import UARTSensor

# Project Models
from measurement_server.models.measurement import Measurement
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.measurement import MUXAddress
from measurement_server.models.measurement import Units
from measurement_server.models.measurement import PlotSettings
from measurement_server.models.mqtt import MQTT
from measurement_server.models.init import Init
from measurement_server.models.log import Log

from measurement_server.modules.unit_names import unit_names


class Command(BaseCommand):
    """
    Main commands for service worker wich reads sensor data and writes them
    to the database.
    """

    help = "Reading sensor data and sending to the database"

    mqtt_client = None

    @staticmethod
    def gen_color(n=200):
        """Get a random color with (r/g/b)>n"""
        # r/g/b < 200 to ensure background separation
        if n == 0:
            r, g, b = 0, 0, 0
        else:
            r, g, b = random.choices(range(0, n), k=3)

        color_string = "{0:#0{1}x}".format(r, 4)[2:]
        color_string += "{0:#0{1}x}".format(g, 4)[2:]
        color_string += "{0:#0{1}x}".format(b, 4)[2:]
        return "#" + color_string

    def wait_for_db(self, t=1, mqtt_client=None):
        """Wait until database is accessbile"""
        try:
            Log(message="Read Sensor worker connected to DB", typ="Event").save(
                mqtt=self.mqtt_client
            )
            return True
        except (InterfaceError, OperationalError):
            time.sleep(t)
            self.wait_for_db()

    def get_MUX_ADDRESS_LIST(self) -> list:
        """
        Read all available mux port addresses and convert to list of hex string.

        Retruns:
            list[str]: List of hexadecimal strings
        """

        # get all ports from database
        Ports = list(MUXAddress.objects.all().values_list("port", flat=True))

        # prepend "0x"
        for i, port in enumerate(Ports):
            Ports[i] = "0x" + port

        return Ports

    def updateSensorCounts(self, state: Measurement) -> None:
        """
        Evaluate KnownSensor entries and count sensors communicating via
        the supported protocols. Update the system state accordingly
        """

        # Collect number of connected sensors (any hardware interface)
        qs_connected = KnownSensors.objects.filter(connected=True)

        # Collect interface specific sensor counts
        qs_connected_spi = qs_connected.filter(serial_mode="SPI")
        qs_connected_i2c = qs_connected.filter(serial_mode="I2C")
        qs_connected_uart = qs_connected.filter(serial_mode="COM")

        # Update database
        state.connected_sensors = qs_connected.count()
        state.connected_ftdi_sensors = qs_connected_spi.count()
        state.connected_ftdi_sensors += qs_connected_i2c.count()
        state.connected_com_sensors = qs_connected_uart.count()
        state.active = False
        state.serial_mode = ""
        state.save(mqtt=self.mqtt_client)

    def getFTDIcount(self) -> int:
        """
        Get number of FTDI

        Returns:
            int: Number of FTDI devices connected to the host system.
        """

        n_connected_ftdi = 0
        try:
            n_connected_ftdi = len(
                usbtools.UsbTools.find_all([(0x0403, 0x6014)], nocache=True)
            )
        except (USBError, ValueError):
            usbtools.UsbTools.flush_cache()

        return n_connected_ftdi

    def check_init_db_entry(self) -> Init:
        """
        Check if a Init db entry exists.
        Otherwise a new entry is created.

        Returns:
            Init: Updated or new generated initialization information
        """

        if Init.objects.count() == 0:
            Init().save(mqtt=self.mqtt_client)
            state = Init.objects.last()
        else:
            state = Init.objects.last()
            state.init_ftdi = False
            state.init_sensors = False
            state.init = True  # Init on startup
            state.state = ""
            state.error = ""
            state.ftdi_state = ""
            state.com_state = ""
            state.save(mqtt=self.mqtt_client)

        return state

    def label_all_sensors_unconnected(self):
        """Set all sensors to unconnected state."""

        KnownSensors.objects.filter(connected=True).update(connected=False)

    def check_measurement_db_entry(self) -> Measurement:
        """
        Verify if a system state configuration entry is registered.
        If not, a new entry is generated.

        Returns:
            System configuration state
        """

        if Measurement.objects.last():
            measurement_state = Measurement.objects.last()
        else:
            Log(message="Using default measurement settings.", typ="FTDI").save(
                mqtt=self.mqtt_client
            )
            measurement_state = Measurement()

        return measurement_state

    def reset_measurement_info(self) -> None:
        """
        Set number of connected sensors to 0 this causes the Quick start button to disappear.
        """

        if Measurement.objects.last():
            data = Measurement.objects.last()
            data.connected_sensors = 0
            data.connected_com_sensors = 0
            data.connected_ftdi_sensors = 0
            data.save(mqtt=self.mqtt_client)
        else:
            Log(message="Using default measurement settings.", typ="Devices").save(
                mqtt=self.mqtt_client
            )
            measurement_state = Measurement()
            measurement_state.active = False
            measurement_state.stopped = False
            measurement_state.save(mqtt=self.mqtt_client)

    def getMQTT(self) -> MQTTClient:
        """
        Use MQTT information to establish a connection to a MQTT broker

        Return:
            MQTTClient or None: Client communication interface or if connection failed None.
        """

        mqtt_client = None
        if MQTT.objects.count():
            mqtt_entry = MQTT.objects.last()
            if mqtt_entry is not None:
                mqtt_client = MQTTClient()
        return mqtt_client

    # Array of supported sensors
    sensor_obj = []
    com_devices = None
    ftdi_setups = None

    def handle(self, *args, **options):
        self._logger = logging.getLogger("readSensor")

        # Hold until DB is up and running
        self.wait_for_db()

        # Init MQTT
        self.mqtt_client = self.getMQTT()

        # Check if init and measurement entries
        state = self.check_init_db_entry()
        measurement_state = self.check_measurement_db_entry()

        self.updateSensorCounts(measurement_state)

        # Main loop of service worker.
        while True:
            self.mqtt_client = self.getMQTT()

            # Get number of FTDI
            n_connected_ftdi = self.getFTDIcount()

            # Get number of comports available.
            comports = search_comports()
            n_connected_comports = len(comports)

            # Update measurement state information
            measurement_state = Measurement.objects.last()
            registered_comports = measurement_state.connected_comports

            if n_connected_ftdi == 0 and n_connected_comports == 0:
                # If there are no devices the measurement will be stopped
                measurement_state.active = False

            if (
                n_connected_ftdi != measurement_state.connected_ftdi
                or (n_connected_ftdi == 0 and measurement_state.connected_sensors != 0)
                or n_connected_comports != registered_comports
            ):
                measurement_state.connected_ftdi = n_connected_ftdi
                measurement_state.connected_comports = n_connected_comports
                if not (n_connected_ftdi + n_connected_comports):
                    measurement_state.connected_sensors = 0
                    measurement_state.connected_com_sensors = 0
                    measurement_state.connected_ftdi_sensors = 0
                    measurement_state.active = False
                    KnownSensors.objects.filter(connected=True).update(connected=False)
                measurement_state.save(mqtt=self.mqtt_client)

            # Check if a FTDI initialization is requested.
            if self.check_for_init_request():
                Log(
                    message="Start device initialization and scan.",
                    typ="Devices",
                ).save(mqtt=self.mqtt_client)

                self.label_all_sensors_unconnected()

                self.reset_measurement_info()
                state = self.check_init_db_entry()
                # self.intialize_sensors()

                # Initialize the FTDI device.
                if n_connected_ftdi > 0:
                    ftdi_initialized = self.initFTDI()
                else:
                    ftdi_initialized = False

                if n_connected_comports > 0 and not ftdi_initialized:
                    # make sure that the init information is updated in cases
                    # where only comport sensors are connected
                    state.state = "run"
                    state.total_ports = 0
                    state.scanned_ports = 0
                    state.detected_sensors = 0
                    state.save(mqtt=self.mqtt_client)
                    comport_initialized = self.initComDevices(comports)
                else:
                    comport_initialized = self.initComDevices(comports)
                    if not comport_initialized:
                        Log(
                            message="Initialization process failed.",
                            typ="COM",
                        ).save(mqtt=self.mqtt_client)

                # Generate init state information and parse it to the database

                # FTDI init information
                if ftdi_initialized:
                    # If FTDI device(s) initialization was successfull
                    # move on and initialize attached sensors.
                    state.init_ftdi = True
                    state.save(mqtt=self.mqtt_client)

                    Log(message="Init FTDI devices done.", typ="FTDI").save(
                        mqtt=self.mqtt_client
                    )

                    sensors_initialized = True

                    # Update the API state model according to the
                    # outcome of the initialization process.
                    if sensors_initialized and len(self.sensor_obj) > 0:
                        state.ftdi_error = 0
                        state.ftdi_state = "ok"

                    elif len(self.sensor_obj) > 0:
                        state.ftdi_state = "fail"
                        state.ftdi_error = "No sensors found."
                        Log(
                            message="Sensor initialization failed.\
                                    (No sensors found)",
                            typ="Sensor",
                        ).save(mqtt=self.mqtt_client)
                    else:
                        state.state = "fail"
                        state.ftdi_error = "Communication error with sensors."
                        Log(
                            message="Sensor initialization failed.\
                                    (Communication error)",
                            typ="Sensor",
                        ).save(mqtt=self.mqtt_client)

                elif n_connected_ftdi == 0:
                    Log(message="No FTDI to initialize.", typ="FTDI").save(
                        mqtt=self.mqtt_client
                    )
                    state.init_ftdi = False
                    state.ftdi_state = "fail"
                    state.ftdi_error = "No FTDI connected."
                else:
                    Log(
                        message="Initialization of FTDI devices failed.",
                        typ="FTDI",
                    ).save(mqtt=self.mqtt_client)
                    state.init_ftdi = False
                    state.ftdi_state = "fail"
                    state.ftdi_error = "Communication error with FTDI."

                # UART init information
                if comport_initialized:
                    # If FTDI device(s) initialization was successfull
                    # move on and initialize attached sensors.
                    state.init_com = True
                    state.save(mqtt=self.mqtt_client)

                    Log(message="Init COM devices done.", typ="COM").save(
                        mqtt=self.mqtt_client
                    )

                    # Update the API state model according to the
                    # outcome of the initialization process.
                    if len(self.sensor_obj) > 0:
                        state.com_error = 0
                        state.com_state = "ok"

                    elif len(self.sensor_obj) == 0:
                        state.com_state = "fail"
                        state.com_error = "No sensors found."
                        Log(
                            message="Sensor initialization failed.\
                                    (No sensors found)",
                            typ="Sensor",
                        ).save(mqtt=self.mqtt_client)
                    else:
                        state.state = "fail"
                        state.com_error = "Communication error with sensors."
                        Log(
                            message="Sensor initialization failed.\
                                    (Communication error)",
                            typ="Sensor",
                        ).save(mqtt=self.mqtt_client)

                elif n_connected_comports == 0:
                    Log(message="No COMports to initialize.", typ="COM").save(
                        mqtt=self.mqtt_client
                    )
                    state.init_com = False
                    state.com_state = "fail"
                    state.com_error = "No COMport available."
                else:
                    Log(
                        message="Initialization of COMport devices failed.",
                        typ="COM",
                    ).save(mqtt=self.mqtt_client)
                    state.init_com = False
                    state.com_state = "fail"
                    state.com_error = "Communication error with COM."

                # Mark initialization process as finished.
                state.init = False
                state.scanned_ports = 0

                if state.init_ftdi or state.init_com:
                    state.init_sensors = True
                    state.state = "ok"
                    state.error = "0"
                else:
                    state.init_sensors = False
                    state.state = "fail"

                # Submit update:
                state.save(mqtt=self.mqtt_client)
                Log(message="Init sensors done.", typ="Sys").save(mqtt=self.mqtt_client)

            # If no initialization request exists,
            # check for scheduled measurements.
            else:
                # Check if sensors were initialized successully.

                if (
                    (state.init_ftdi or state.init_com)
                    and
                    # Check if sensors were initialized successully.
                    state.init_sensors
                    and
                    # Check if no error persits.
                    state.state == "ok"
                    and
                    # Check if there are sensors initialized.
                    len(self.sensor_obj) > 0
                ):
                    # Get informaiton about scheduled measurements.
                    measurement_state = Measurement.objects.last()

                    # Check if a measurement is scheduled.
                    if (  # Check if a state ofject exists.
                        measurement_state is not None
                        and
                        # Check if a measurement is scheduled.
                        self.measurementScheduled(measurement_state)
                        and
                        # Check if the measurement was stopped.
                        not measurement_state.stopped
                    ):
                        # Start measurement
                        Log(message="Starting measurement", typ="Event").save(
                            mqtt=self.mqtt_client
                        )
                        self.runMeasurement()
                        measurement_state.active = False
                        measurement_state.stopped = True
                        measurement_state.save(mqtt=self.mqtt_client)

                        if Measurement.objects.last().stopped:
                            Log(
                                message="Measurement has been\
                                         stopped manually.",
                                typ="Event",
                            ).save(mqtt=self.mqtt_client)
                        else:
                            stopped = Measurement.objects.last().stopped
                            Log(
                                message=f"Measurement ended as scheduled.\
                                          {stopped}",
                                typ="Event",
                            ).save(mqtt=self.mqtt_client)
                    else:
                        # If no measurement is scheduled
                        # update API state.
                        state.active = False
                        state.save(mqtt=self.mqtt_client)

            # Wait before repeating the loop:
            time.sleep(0.1)
            time.sleep(3)
            # End of while statement.

    def check_for_init_request(self) -> bool:
        """
        Check if a sensor initialization has been requested.

        Returns:
            bool: True is request has been registered, False otherwise.
        """

        if Init.objects.count() == 0:
            state = Init()
            state.mqtt = self.mqtt_client
            state.init = True
            state.save()
        else:
            state = Init.objects.last()

        if state.init and state.state != "run":
            # Reset measurement schedule before init.
            Log(
                message="Reset measurement schedule\
                         before initialization process.",
                typ="Event",
            ).save(mqtt=self.mqtt_client)
            if Measurement.objects.last():
                mr = Measurement.objects.last()
            else:
                mr = Measurement()
                Log(
                    message="Using default measurement settings.",
                    typ="Event",
                ).save(mqtt=self.mqtt_client)

            mr.connected_sensors = 0
            mr.connected_com_sensors = 0
            mr.connected_ftdi_sensors = 0
            mr.stopped = False
            mr.start_measure = Measurement._meta.get_field(
                "start_measure"
            ).get_default()
            mr.start_measure -= datetime.timedelta(hours=1)
            mr.stop_measure = Measurement._meta.get_field("stop_measure").get_default()
            # prevent a measurement start
            mr.start_measure -= datetime.timedelta(hours=1)
            mr.stop_measure -= datetime.timedelta(hours=1)
            mr.active = Measurement._meta.get_field("active").get_default()
            mr.save(mqtt=self.mqtt_client)
            return True

        return False

    def register_sensor(self, sensor) -> None:
        """
        Write information accquired by driver module to system database.
        """

        if not KnownSensors.objects.filter(SENSORserial=sensor.serial_number).exists():
            s = KnownSensors(SENSORserial=sensor.serial_number, Color=self.gen_color())
            s.save()
        else:
            s = KnownSensors.objects.get(SENSORserial=sensor.serial_number)

        if sensor._serial_mode == "SPI":
            s.CS = int(str(sensor.cs), 16)
            s.FTDIserial = sensor.ftdi_serial

        elif sensor._serial_mode == "I2C":
            s.MUXaddress = int(str(sensor.mux.address), 16)
            s.MUXport = int(str(sensor.mux_port), 16)
            s.FTDIserial = sensor.ftdi_serial

        elif sensor._serial_mode == "COM":
            s, _ = KnownSensors.objects.get_or_create(SENSORserial=sensor.serial_number)
            s.COMport = sensor.port

            s.FTDIserial = "undefined"
            all_comports = list_ports.comports()
            for cp in all_comports:
                if cp.device == sensor.port and cp.serial_number:
                    s.FTDIserial = cp.serial_number
                    break
        else:
            raise ValueError(f"Unknown serial mode: {sensor._serial_mode}")

        s.serial_mode = sensor._serial_mode
        s.SENSORtype = sensor.__name__
        s.SENSORserial = sensor.serial_number

        # Add supported measurement units
        for u in sensor._units:
            # This implies that all measurements of the same qty
            # are converted to the same unit
            unit, _ = Units.objects.get_or_create(qty=u)
            unit.qty = u
            unit.sign = sensor._units[u]
            if u in unit_names.keys():
                unit.name = unit_names[u]
            else:
                unit.name = u
            unit.save()
            if not s.plots.filter(qty=u).exists():
                plot = PlotSettings(qty=u, show=True)
                plot.save()
                s.plots.add(plot)

            if unit not in s.units.all():
                s.units.add(unit)

        s.connected = True
        s.save()
        self.sensor_obj.append(sensor)

        if sensor._serial_mode == "I2C":
            Log(
                message=f"Registered I2C sensor ({s.SENSORtype})\
                          Port{s.MUXport}/{s.MUXaddress}@{s.FTDIserial}",
                typ="Sensor",
            ).save(mqtt=self.mqtt_client)
        elif sensor._serial_mode == "SPI":
            Log(
                message=f"Registered SPI sensor cs{s.CS}@{s.FTDIserial}",
                typ="Sensor",
            ).save(mqtt=self.mqtt_client)
        elif sensor._serial_mode == "COM":
            Log(
                message=f"Registered COM sensor {s.SENSORtype}@{s.COMport}",
                typ="Sensor",
            ).save(mqtt=self.mqtt_client)

    def initFTDI(self) -> bool:
        """
        Initialize FTDI devices and scan for sensors.

        Return:
            bool: True if sensors were detected, false if detection failed.
        """

        self.label_all_sensors_unconnected()

        state = self.check_init_db_entry()
        state.init_ftdi = False
        state.init_sensors = False

        # Indicate a running FTDI initialization.
        state.state = "run"
        state.total_ports = 0
        state.scanned_ports = 0
        state.detected_sensors = 0
        state.save(mqtt=self.mqtt_client)

        # A USBError is expected if problems with the FTDI
        # wired connection persits.
        try:
            Log(message="Begin FTDI scan.", typ="FTDI").save(mqtt=self.mqtt_client)
            # Check if SPI
            supported_converters = [(0x0403, 0x6014)]

            mux_mode = Measurement.objects.last().spi_mux_mode == "MUX"
            dev = init_all(supported_converters, CS_MUX=mux_mode)

            if len(dev) == 0:
                msg = "No FTDI detected."
                Log(message=msg, typ="FTDI").save(mqtt=self.mqtt_client)
                state.error = msg
                state.init = False
                self.ftdi_setups = None
                state.state = "fail"
                return False

            setups = []
            threads = []

            mux_address_list = self.get_MUX_ADDRESS_LIST()
            for bus in dev:
                Log(
                    message=f"Started init thread for\
                              {bus.ftdi._usb_dev.serial_number}",
                    typ="FTDI",
                ).save(mqtt=self.mqtt_client)
                kwargs = {
                    "measurement_obj": state,
                    "mux_address_list": mux_address_list,
                }
                j = ThreadWithReturnValue(target=setup, args=(bus,), kwargs=kwargs)

                j.start()
                threads.append(j)

            for t in threads:
                setups.append(t.join())

            if len(setups) > 0:
                Log(
                    message="FTDI controller initialized using\
                             {len(setups)} thread(s).",
                    typ="FTDI",
                ).save(mqtt=self.mqtt_client)
                self.ftdi_setups = setups
            else:
                Log(
                    message="Error during configuration detection.",
                    typ="FTDI",
                ).save(mqtt=self.mqtt_client)
                state.error = "Error while trying to get FTDI configuration"
                state.init = False
                self.ftdi_setups = None
                state.state = "fail"
                return False

        except usb.core.USBError:
            Log(message="Lost connection to FTDI.", typ="FTDI").save(
                mqtt=self.mqtt_client
            )

            # Write error to init model.
            state.error = "FTDI disconnected. Reconnect and restart."
            state.init = False
            state.state = "fail"
            return False

        # Check if device is in i2c mode and make sure one
        # or more FTDI devices are connected.
        for Dev in setups:
            if Dev is not None and len(Dev) > 0:
                for dev in Dev:
                    if isinstance(dev, MmsI2cController):
                        for mux in dev.mux:
                            for sensor in mux.sensors:
                                sensor = sensor["obj"]
                                if sensor.serial_number is not None:
                                    self.register_sensor(sensor)
                    elif isinstance(dev, MmsSpiController):
                        for sensor in dev.sensors:
                            sensor = sensor["obj"]
                            if sensor.serial_number is not None:
                                self.register_sensor(sensor)
                    else:
                        raise ValueError(
                            "Unsupported controller\
                                          type %s",
                            type(dev),
                        )
        # Check return result of init process.
        # store total number of connected sensors
        if Measurement.objects.count() > 0:
            data = Measurement.objects.last()
            self.updateSensorCounts(data)

        return True

    def initComDevices(self, comports: list[str]) -> bool:
        """
        Initialize devices connected to comports i.e /dev/ttyUSB0

        Args:
            comports (list[str]): List of comport paths.

        Returns:
            bool: True if sensors were detected, False otherwise
        """

        # Set all sensors to disconnected
        KnownSensors.objects.filter(connected=True, serial_mode="COM").update(
            connected=False
        )

        # Reset Init model instance:
        if Init.objects.count() == 0:
            Init().save(mqtt=self.mqtt_client)
        state = Init.objects.last()
        state.init_com = False
        state.init_com_devices = False

        # Indicate a running COM initialization.
        state.save(mqtt=self.mqtt_client)

        # A USBError is expected if problems with the COMDevice
        # wired connection persits.

        try:
            Log(message="Begin COM port scan.", typ="COM").save(mqtt=self.mqtt_client)

            devices = []
            for port in comports:
                try:
                    device = scan_uart(port, measurement_obj=state)
                except SerialTimeoutException:
                    device = None

                if device is not None:
                    devices.append(device)
                    device.start()

            if len(devices) > 0:
                Log(
                    message="COM controllers initialized\
                             using single thread implementation.",
                    typ="COM",
                ).save(mqtt=self.mqtt_client)
                self.com_devices = devices

            else:
                Log(
                    message="Error during configuration detection.",
                    typ="COM",
                ).save(mqtt=self.mqtt_client)
                data = Measurement.objects.last()
                data.connected_com_sensors = 0
                state.com_error = "Error while trying to get COM\
                                   port configuration"
                state.com_init = False
                self.com_devices = None
                state.com_state = "fail"
                state.save(mqtt=self.mqtt_client)
                return False

        except usb.core.USBError:
            Log(message="Lost connection to COMport.", typ="COM").save(
                mqtt=self.mqtt_client
            )

            # Write error to init model.
            state.com_error = "COMport disconnected. Reconnect and restart."
            state.com_init = False
            state.com_state = "fail"
            state.save(mqtt=self.mqtt_client)
            return False

        # Check if device is in i2c mode an make sure
        # 1 or more FTDI devices are connected.
        if len(devices) > 0:
            for sensor in devices:
                if sensor._serial_mode == "COM":
                    if sensor.serial_number is not None:
                        self.register_sensor(sensor)
        # No COM devices.
        else:
            Log(message="No com port found.", typ="COM").save(mqtt=self.mqtt_client)
            state.com_state = "fail"
            state.com_error = "No COM port found."
            state.com_init = False
            state.save(mqtt=self.mqtt_client)
            return False

        state.save(mqtt=self.mqtt_client)

        # Check return result of init process.
        # store total number of connected sensors
        if Measurement.objects.count() > 0:
            data = Measurement.objects.last()
            self.updateSensorCounts(data)

        return True

    def measurementScheduled(self, state: Measurement) -> bool:
        """
        Check if measurement is currently scheduled.

        Args:
            state (Measurement): Current system state

        Returns:
            bool: True if measurement is scheduled, False otherwise
        """

        t = now()

        if state.start_measure <= t and t < state.stop_measure:
            return True
        return False

    def runMeasurement(self) -> None:
        """
        Perfom a measurement while measurement is scheduled and not
        stopped. Information from sensors is requested and the results are written to the database.
        """

        # Check if a Measurement instance exists and the last measurement entry
        # is not stopped.
        if not Measurement.objects.count():
            return

        state = Measurement.objects.last()

        if state.stopped:
            return

        # Set measurement to active.
        state.active = True
        state.save(mqtt=self.mqtt_client)

        # Get measurement mode
        mode = state.daq_mode

        # Setup result object which will hold all measurement results.
        state = Measurement.objects.last()
        result = self.create_measurement_result_entry()

        # Store result id to measurement instance.
        state = Measurement.objects.last()
        state.result_id = result.id
        state.save(mqtt=self.mqtt_client)

        # Make sure correct identifiers are used and
        # init sensor entries for Result instance

        threads = []
        if self.measurementScheduled(state) and state.active and not state.stopped:
            # Associate sensors and new result array\
            # (copies current sensor information)
            if self.ftdi_setups is not None:
                for setups in self.ftdi_setups:
                    for s in setups:
                        j = ThreadWithReturnValue(
                            target=self.setup_initialzation, args=(s, result)
                        )
                        j.start()
                        threads.append(j)
                Log(
                    message=f"Started {len(self.ftdi_setups)} FTDI thread(s).",
                    typ="FTDI",
                ).save(mqtt=self.mqtt_client)
            else:
                Log(message="No FTDI threads.", typ="FTDI").save(mqtt=self.mqtt_client)

            if self.com_devices is not None:
                for com_device in self.com_devices:
                    com_device.start()
                    j = ThreadWithReturnValue(
                        target=self.setup_initialzation, args=(com_device, result)
                    )
                    j.start()
                    threads.append(j)

                    Log(
                        message=f"Started {len(self.com_devices)}\
                                  COM-Port device(s).",
                        typ="COM",
                    ).save(mqtt=self.mqtt_client)
            else:
                Log(message="No COM-Port device initialized.", typ="FTDI").save(
                    mqtt=self.mqtt_client
                )

            for thread in threads:
                thread.join()

            # update current state.
            state = Measurement.objects.last()

            # Read values for each sensor.
            ftdi_threads = []
            com_threads = []
            threads = []
            if self.measurementScheduled(state) and state.active and not state.stopped:
                if self.ftdi_setups is not None:
                    for setups in self.ftdi_setups:
                        for s in setups:
                            j = ThreadWithReturnValue(
                                target=self.run_measurement,
                                args=(s, state, result, mode),
                            )
                        j.start()
                        ftdi_threads.append(j)
                    Log(
                        message=f"Started {len(ftdi_threads)} FTDI thread(s).",
                        typ="FTDI",
                    ).save(mqtt=self.mqtt_client)
                if self.com_devices is not None:
                    for com_device in self.com_devices:
                        j = ThreadWithReturnValue(
                            target=self.run_measurement,
                            args=(com_device, state, result, mode),
                        )
                        j.start()
                        com_threads.append(j)
                    Log(
                        message=f"Started {len(com_threads)} COM thread(s).",
                        typ="COM",
                    ).save(mqtt=self.mqtt_client)

                threads = ftdi_threads + com_threads

                for thread in threads:
                    thread.join()

        # Measurement finished
        result.stop_measure = now()
        result.save(mqtt=self.mqtt_client)
        state.active = False
        state.save(mqtt=self.mqtt_client)

    def create_measurement_result_entry(self) -> MeasurementData:
        """
        Setup result object holding informaiton about a measurement run.
        A measurement name is assigned and extended by trailing numbers to ensure uniqueness.

        Returns:
            MeasurementData: Model instance of new result entry
        """

        # Get current system state
        state = Measurement.objects.last()

        # Generate new result entry and add information
        result = MeasurementData()
        result.name = state.name
        result.created = state.created
        result.start_measure = state.start_measure
        result.stop_measure = state.stop_measure
        result.stopped = state.stopped

        # Verify uniqueness of the result name.
        # If the name is used already, add preceeding numbers.
        if MeasurementData.objects.filter(name=result.name).exists():
            orig_name = result.name
            new_name = result.name
            i = 0

            while MeasurementData.objects.filter(name=new_name).exists():
                if i != 0:
                    new_name = f"{orig_name}_{i:04d}"
                    result.name = new_name
                i += 1

        result.save(mqtt=self.mqtt_client)

        return result

    def setup_initialzation(self, dev, *args) -> None:
        """
        Depending on the device type select an initialization procedure
        to check if the device presents one or more sensors and is working
        properly.
        """
        if isinstance(dev, MmsI2cController):
            for mux in dev.mux:
                for sensor in mux.sensors:
                    self.initialize_sensor(sensor, *args)
        elif isinstance(dev, MmsSpiController):
            for sensor in dev.sensors:
                self.initialize_sensor(sensor, *args)
        elif dev.__class__.__bases__[0] == UARTSensor:
            self.initialize_sensor(dev, *args)
        else:
            Log(
                message=f"Unsupported serial mode for {type(dev)}",
                typ="Sensor",
            ).save(mqtt=self.mqtt_client)
            raise ValueError(
                "Unsupported controller type %s Base: %s",
                type(dev),
                type(dev).__bases__,
            )

    def initialize_sensor(self, sensor, result) -> None:
        """
        Assing measurement value model according to sensor type.
        """

        if isinstance(sensor, dict):
            sensor = sensor["obj"]

        if sensor.__name__ in SupportedSensors:
            s = SupportedSensors[sensor.__name__]["model"]["base"]()
        else:
            Log(
                message=f"Unknown sensor <{sensor.__class__}>.",
                typ="Sensor",
            ).save(mqtt=self.mqtt_client)
            raise NameError("Unknown sensor type")

        # Store sensor details.
        if sensor._serial_mode == "I2C":
            s.MUXport = sensor.mux_port
            s.MUXaddress = int(str(sensor.mux.address))
            s.FTDIserial = sensor.ftdi_serial
            sensor.prepare_measurement()
        elif sensor._serial_mode == "SPI":
            s.CS = int(str(sensor.cs))
            s.FTDIserial = sensor.ftdi_serial
            sensor.setup()
        elif sensor._serial_mode == "COM":
            ftdi_serial = "undefined"
            all_comports = list_ports.comports()
            for cp in all_comports:
                if cp.device == sensor.port and cp.serial_number:
                    ftdi_serial = cp.serial_number
                    break

            s.COMport = str(sensor.port)
            s.FTDIserial = ftdi_serial
        else:
            Log(
                message=f"Unsupported serial\
                          mode {type(sensor._serial_mode)}",
                typ="Sensor",
            ).save(mqtt=self.mqtt_client)
            raise ValueError("Unsupported serial mode %s", type(sensor._serial_mode))

        s.SENSORtype = sensor.__name__
        s.SENSORserial = sensor.serial_number
        s.sensor = result
        s.save(mqtt=self.mqtt_client)

        # Add supported measurement units
        sensor = KnownSensors.objects.get(SENSORserial=sensor.serial_number)
        for u in sensor.units.all():
            # This implies that all measurements of the same qty
            # are converted to the same unit
            unit, _ = Units.objects.get_or_create(qty=u.qty)
            unit.save()
            if unit not in s.units.all():
                s.units.add(unit)

        # Add calibrations
        for c in sensor.calibration.all():
            # copy current calibration (such that the information
            # cannot be changed later on)
            c.pk = None
            c.readonly = True
            c.save()
            s.calibration.add(c)

        s.Color = sensor.Color
        s.x = sensor.x
        s.y = sensor.y
        s.z = sensor.z
        s.CS = sensor.CS

        # Check if sensor is known (by comparing serial numbers).
        name = ""
        if KnownSensors.objects.filter(SENSORserial=sensor.SENSORserial).count():
            name = KnownSensors.objects.get(
                SENSORserial=sensor.SENSORserial
            ).SENSORidentifier

        s.SENSORidentifier = name
        s.save(mqtt=self.mqtt_client)

    def store_data(self, data: dict, result: MeasurementData, mode: str) -> None:
        """
        Write measurement data to database depending on the selected server mode
        old measurements are overwritten or a new database entry is generated.

        Args:
            data(dict): New measurement values
            reult(MeasurementData): Current measurement case entry
            mode(str): Data retention mode

        """

        sensor_type = data["sensor_type"]
        sensor_serial = data["SENSORserial"]
        sensor_res_set = SupportedSensors[sensor_type]["model"]["base"].__name__.lower()
        sensor_res_set += "_set"
        sensor_field = getattr(result, sensor_res_set)

        if sensor_type in SupportedSensors:
            sensor_res = SupportedSensors[sensor_type]["model"]["res"]
            result_cls_name = sensor_res.__name__.lower()
            # If mode is Server (SR) a result object
            # is generated for each data point
            if mode == "SR":
                value = sensor_res()
            elif mode == "ST":
                # If mode is Stream (ST) it has to
                # be checked if there is a value
                sensor = sensor_field.get(SENSORserial=sensor_serial)
                if getattr(sensor, result_cls_name + "_set").count():
                    # If a data point has been assigned already
                    # it will be overwritten
                    value = getattr(sensor, result_cls_name + "_set").first()
                else:
                    # Generate a data point if no object is present
                    value = SupportedSensors[sensor_type]["model"]["res"]()
            # Assign time value
            value.time = now()
        else:
            Log(message=f"Unsupported sensor {sensor_type}", typ="Sensor").save(
                mqtt=self.mqtt_client
            )

        # Check if received data contains measurement values.
        if data["object"] == "DATA" and not data["error"]:
            # Read values and write to Sensor instance.
            # Store values if no error is found
            for qty in data["values"].keys():
                setattr(value, qty, data["values"][qty]["value"])
        # Stores default values if no measurement data is present
        value.results = sensor_field.get(SENSORserial=sensor_serial)
        value.save(mqtt=self.mqtt_client)

    def get_sensor_data(self, dev, state, result) -> dict:
        """
        Read data from measurement device.
        It has to be distinguished between different communication interfaces.
        """

        data_set = {}
        if isinstance(dev, MmsI2cController):
            for mux in dev.mux:
                for sensor in mux.sensors:
                    sensor = sensor["obj"]
                    data = sensor.get_data()
                    if sensor.disconnected_ftdi:
                        # if a ftdi disconnected all
                        # sensors have to be reinitialized
                        for m in dev.mux:
                            for s in mux.sensors:
                                if s["obj"] is not None:
                                    s["obj"].prepare_measurement()
                                    s["obj"].disconnected_ftdi = False
                    sn = sensor.serial_number
                    data_set[sn] = data

        # FTDI with SPI
        elif isinstance(dev, MmsSpiController):
            for sensor in dev.sensors:
                sensor = sensor["obj"]
                data = sensor.get_data()
                sn = sensor.serial_number
                data_set[sn] = data

        # Std. COM port
        elif dev._serial_mode == "COM":
            data = dev.get_data()
            sn = dev.serial_number
            data_set[sn] = data

        else:
            raise ValueError("Unsupported controller type %s", type(dev))

        return data_set

    def compute_measurement_data_average(self, data: dict) -> dict:
        """
        Compute mean of sensor measurement results

        Args:
            data (dict): list of result entries

        Returns:
            dict: Averaged measurement values
        """

        # Calculate mean
        out = {}
        for sn in data.keys():
            if data[sn]["object"] == "ERROR":
                out[sn] = data[sn]
                continue

            N = len(data[sn]["values"])
            out[sn] = None
            for measurement_entry in data[sn]["values"]:
                if out[sn] is None:
                    out[sn] = copy.deepcopy(data[sn])
                    out[sn]["values"] = out[sn]["values"][0]
                else:
                    for val in out[sn]["values"].keys():
                        out[sn]["values"][val]["value"] += measurement_entry[val][
                            "value"
                        ]

            for val in out[sn]["values"].keys():
                if out[sn]["values"][val]["value"] is not None:
                    out[sn]["values"][val]["value"] /= N

        return out

    def run_measurement(self, dev, state, result, mode) -> None:
        """
        Check if a measurement has been requested and fetch values from each sensor.
        If sensors repond faster then requested by the measurement frequency additional values are
        acquired. An averaged value is returned that matches the meaurement frequency closely.
        """

        while self.measurementScheduled(state) and state.active and not state.stopped:
            # FTDI I2C with MUX
            t0 = time.time()
            data_set = self.get_sensor_data(dev, state, result)
            n = 1
            # assuming all measurement take the same time
            dt_measurement = time.time() - t0

            # If a maximum measurement frequency is requested
            # values are collected and averaged periodically
            # to increase accuracy
            if state.max_freq > 0:
                for sn in data_set.keys():
                    if data_set[sn]["object"] != "ERROR":
                        data_set[sn]["values"] = [data_set[sn]["values"]]

                dt_min = 1.0 / float(state.max_freq)

                while time.time() - t0 + dt_measurement < dt_min:
                    new_data_set = self.get_sensor_data(dev, state, result)
                    for sn in new_data_set.keys():
                        if new_data_set[sn]["object"] == "ERROR":
                            # New invalid values do not provide better accuracy.
                            continue
                        elif data_set[sn]["object"] == "ERROR":
                            # If the first value was invalid
                            # replace it with the new one.
                            data_set[sn] = new_data_set[sn]
                            data_set[sn]["values"] = [data_set[sn]["values"]]
                        elif sn in data_set.keys():
                            # Write a list of valid values.
                            data_set[sn]["values"] += [new_data_set[sn]["values"]]
                    n += 1

                t_diff = dt_min - time.time() + t0
                if t_diff > 0:
                    time.sleep(t_diff)

                data_set = self.compute_measurement_data_average(data_set)

            for sn in data_set.keys():
                self.store_data(data_set[sn], result, mode)

            state = Measurement.objects.last()
