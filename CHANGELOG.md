# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).

## 1.2.3 - (2023-09-15)
---

### Changes
   * Updated plotting function


## 1.2.2 - (2023-08-07)
---

### Changes
   * Updated pyserialsensors version to 0.15.1 


## 1.2.1 - (2023-07-21)
---

### New
   * Added release instructions to documentation


## 1.2.0 - (2023-07-21)
---

### New
   * Added support for SCD41

### Fixed
   * Minor improvements in documentation


## 1.1.1 - (2023-06-23)
---

### Changes
  * Updated to Django 4.2.2
  * Updated pyserialsensors version to 0.13.3 
  * Default dev server port changed from 4444 to 4445

### Improvements
  * Reduced number of explicit sensor class naming in js code
  * Added instructions to add a new sensor to the system

### Fixed
  * Quick start command works again

## 1.1.0 - (2022-09-05)
---

### Changes
  * added support for Optidew dew point Hygrometer

## 1.0.0 - (2022-08-29)
---

### Fixes
  * Fixed behviour that could produce in rare cases identical measurement identifiers

### Improvements
  * Added link to documentation to README
  * Linting

## 0.9.6 - (2022-08-09)
---

### Changes
 * Updated pyserialsensors version to 0.10.3 (BME280 fix)

### Improvements
 * Increased data export test coverage
 * Increased settings view test coverage
 * Increased readSensor test coverage
 * Completed calibration test coverage
 * Minor code style improvemets


## 0.9.4 - (2022-04-19)
---

### New
 * Added dev version documentation on [Read the docs](https://mobile-measurement-system.readthedocs.io/en/dev/)
 * Added hardware documentation for the PCB of the MAX31865 PT100 breakout box (Close #116)
 * Added NetCDF export function with the option for unified timestamps (#52)
 * Added minimal code snippets for live monitoring the system via API 
 * Added sort options to the sensor overview in the system settings
 * Added warnings if a user tries to set identical sensor identifier

### Changes
 * Hit 75% test coverage (Close #77)
 * Improved visualization menu
 * Improved status bar
 * Updated pyserialsensors version to 0.9.19

### Fixes
 * Fixed missing system information
 * Fixed value sorting in live view
 * Further code style improvements including application of black code style (Close #105)
 * Fixed UI links and typos


## 0.9.1 - (2021-11-11)
---

### Fixes
* fixed SHT naming issue

## 0.9.0 - (2021-11-10)
---

### New
* positional information can be assigned to each sensor
* added support for sensor configuration uploads and downloads with proper error handling
* added api support for polynomial calibration functions (Relates to #53), !GUI implementation is missing
* added old live view accessible via live/legacy
* added alert banner if a sensor encounters warnings or errors
* added link to the data download menu to the top menu bar
* added dynamic plots
* improved error messages for sensor configuration uploads

### Changes
* lots of code style improvements
* added tests covering the data export section
* changed default plotting values
* further improvements on the data visualization

### Fixes
* a bug that denied correct SMGair2 initialization if connected via non FTDI RS232-to-USB converters
* a bug that caused frequency drops after certain FTDI communication errors (Fix #103)
* SMGair results are now part of the visualization tab
* updated pyserialsensors version 0.9.16 (this includes support for SFM4300 (Close #106) and  FTDI stability improvements (Close #103)
* the correct measurement stopped information is display in the download menu
* corrected sps30 measurement density units
* fixed an issue that occured when @-characters were used in sensor identifiers	
* fixed incompatibilities between Windows and and Unix generated CSV files with regards to sensor configuration
* messed around with static file hosting

### Breaks
* genTime is no longer part of the plot api


## 0.8.1 - (2021-07-14)
---

### Fixes
* updated pyserialsensors version 0.9.11 now supports negativ temperatures at SDP810 (Fix #95)


## 0.8.0 - (2021-07-14)
---

### New
* Added scroll area to live data view for better usability

### Changes
* improved documentation on setting up dev environments
* Improved dev environemnt start up and documentation
* Upgraded to Chart.js 3.4.1
* style improvements in settings view (Fix #98)
* added links to documentation images (Fix #97)

### Fixes
* Fixed label overflow in multi column legend (Fix #99)

## 0.7.1 - (2021-06-24)
---

### Changes
* hotfixed MAX31865 connection issue

## 0.7.0 - (2021-06-17)
---

### New

* added option to select/exclude measurement quantities from the visualization menu on a per sensor basis.
* added a changelog to the project
* added dynamic mean, max and min value calculation in live view (Fix #71)
* added identifier and value sorting option to live view (Fix #69)

### Changes
* hotfixed MAX31865 connection issue
* static files are shiped together with the main docker container such that no extra download is required during setup
* improved visualization layout

## 0.6.0 - (2021-05-30)
---

### Changes

* moved standard mounting point from /home/mms/ to /var/lib/mms/
* mqtt messages post to a topic with the measurement quantity and the payload value and physical quantity (sign) (Fix #88)
