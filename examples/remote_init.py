# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
  Example code for a remote FTDI and sensor initialization

  Endpoint: api/initialize
  GET: Will return the current initialization state
  POST: {"init": "True"} will start the initialization process and returns the previous init state.
"""

import time
import requests
import logging

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger("initExample")

URL = "http://localhost:4445/"
ENDPOINT = "api/initialize"

if __name__ == "__main__":
    url = URL + ENDPOINT

    resp = requests.get(url).json()["data"]
    if resp["init"] == True:
        _logger.warning(
            "Currently initializing. Sending an init request will have no effect!"
        )
    elif resp["init"] == False:
        resp = requests.post(url, data={"init": "True"})
        print(resp.json())
        _logger.info("Started initializing process")

        resp = requests.get(url).json()["data"]
        # The detected sensors information will be reset after
        # the init process hast finished
        # Use /api/measurement to fetch the number of connected sensors
        # during runtime
        detected_sensors = 0
        while resp["init"] == True:
            _logger.info(
                "Current init state %s/%s ports scanned.",
                resp["scanned_ports"],
                resp["total_ports"],
            )
            detected_sensors = resp["detected_sensors"]
            resp = requests.get(url).json()["data"]
            if resp["init"] == False:
                _logger.info(
                    "Finished init process. Found %s sensor(s).", detected_sensors
                )
            time.sleep(0.2)
    else:
        _logger.error("Undefined init state %s.", resp["init"])
