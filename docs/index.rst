.. Mobile Measurement System documentation master file, created by
   sphinx-quickstart on Sat Jan 22 11:37:29 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Mobile Measurement System!
=====================================================

Do you want to start right away? Checkout the :ref:`hardwaresetup` hardware requirements and :ref:`softwaresetup`.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   /setup
   /usermanual
   /developers
   /api
   /modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
