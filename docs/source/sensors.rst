Featured sensors
====================================
.. _featuredsensors:

A summary of all sensors supported by this projects is given below. 

.. list-table::
   :header-rows: 1

   * - Sensor type
     - Quantities
     - Info
   * - SHT85
     - Humidity, temperature
     - `Datasheet <https://sensirion.com/media/documents/4B40CEF3/61642381/Sensirion_Humidity_Sensors_SHT85_Datasheet.pdf>`__,
       `Certificate <https://sensirion.com/media/documents/91BA82E8/616405D3/Sensirion_Humidity_Sensors_SHTxx_Calibration_Certification.pdf>`__
   * - SCD30
     - CO2 concentration in air, temperature, humidity
     - `Datasheet <https://sensirion.com/media/documents/4EAF6AF8/61652C3C/Sensirion_CO2_Sensors_SCD30_Datasheet.pdf>`__,
       `Certificate <https://sensirion.com/media/documents/DD009322/61652BB8/Sensirion_CO2_Sensors_SCD30_MSDS.pdf>`__
   * - SCD41
     - CO2 concentration in air, temperature, humidity
     - `Datasheet <https://sensirion.com/media/documents/E0F04247/631EF271/CD_DS_SCD40_SCD41_Datasheet_D1.pdf>`__,
       `Certificate <https://sensirion.com/media/documents/42FA3069/62860E63/CalibrationCertification_SCD4.pdf>`__
   * - SPS30
     - PM1.0, PM2.5, PM4 and PM10 
     - `Datasheet <https://sensirion.com/media/documents/8600FF88/616542B5/Sensirion_PM_Sensors_Datasheet_SPS30.pdf>`__,
       `Certificate <https://sensirion.com/media/documents/3A3BF572/616540E1/Sensirion_PM_Sensors_Datasheet_SPS30_MCERTS-Certificate_2020.pdf>`__
   * - SDP810 (125Pa)
     - Differential pressure, temperature
     - `Datasheet <https://sensirion.com/media/documents/90500156/6167E43B/Sensirion_Differential_Pressure_Datasheet_SDP8xx_Digital.pdf>`__,
       `Certificate <https://sensirion.com/media/documents/4EE3251D/6166C84E/Sensirion_Differential_Pressure_Sensors_SDP8xx_Calibration_Certifica.pdf>`__
   * - SDP810 (512Pa)
     - Differential pressure, temperature
     - `Datasheet <https://sensirion.com/media/documents/90500156/6167E43B/Sensirion_Differential_Pressure_Datasheet_SDP8xx_Digital.pdf>`__,
       `Certificate <https://sensirion.com/media/documents/4EE3251D/6166C84E/Sensirion_Differential_Pressure_Sensors_SDP8xx_Calibration_Certifica.pdf>`__
   * - SFM3000
     - Volume flow
     - `Datasheet <https://sensirion.com/media/documents/BA2F2B48/616681DC/Sensirion_Mass_Flow_Meters_SFM3000_Datasheet.pdf>`__
   * - SFM3200
     - Volume flow
     - `Datasheet <https://sensirion.com/media/documents/70FC8320/62962251/GF_DS_SFM3200.pdf>`__
   * - SFM3300
     - Volume flow
     - `Datasheet <https://sensirion.com/media/documents/8ECF0D28/62D13110/Sensirion_Datasheet_SFM3300-AW_SFM3300-D.pdf>`__
   * - SFM4100
     - Volume flow
     - `Datasheet <https://sensirion.com/media/documents/6A24D8A6/61668BE1/Sensirion_Mass_Flow_Meters_SFM4100_Datasheet.pdf>`__
   * - SFM4200
     - Volume flow
     - `Datasheet <https://sensirion.com/media/documents/7AA39BF4/61668C94/Sensirion_Mass_Flow_Meters_SFM4200_Datasheet.pdf>`__
   * - BME280 
     - Ambient pressure, temperature, humidity
     - `Datasheet <https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme280-ds002.pdf>`__
   * - NAU7802
     - Bridge sensor
     - `Datasheet <https://www.nuvoton.com/resource-files/NAU7802%20Data%20Sheet%20V1.7.pdf>`__
   * - S8000
     - Temperature, dewpoint temperature
     - `Datasheet <https://www.processsensing.com/docs/datasheet/S8000-Remote-97307-DE-Datasheet-v4.pdf>`__
   * - ADS1015
     - Analog digital conversion 
     - `Datasheet <https://www.ti.com/lit/ds/symlink/ads1015.pdf?ts=1646073958291&ref_url=https%253A%252F%252Fwww.google.com%252F>`__
   * - MAX31865
     - Temperature (4-wire Pt100)
     - `Datasheet <https://datasheets.maximintegrated.com/en/ds/MAX31865.pdf>`__
   * - SMGair2
     - Volume flow, temperature, ambient pressure 
     -
   * - Optidew
     - Temperature, dewpoint temperature, humidity
     - `Datasheet <https://www.processsensing.com/docs/datasheet/Michell_Instruments_Optidew_US_Datasheet_v3-1.pdf>`__

