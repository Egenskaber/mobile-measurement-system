Web API
===================

All API requests start with "base url"/api (i.e. localhost/api or 192.168.0.102/api). 
If ``<ip>/api`` is requested via the browser (Firefox preferred) several links are displayed, with a subset of common requests.
A detailed description of all request options is presented in the following.

.. _API Measurement:

/api/measurement 
................

`Example File <_static/api/example_measurement.json>`__

Generally, provide information about the current measurement and device state. 

.. list-table::
   :header-rows: 1

   * - variable 
     - type 
     - details 
   * - name   
     - str  
     - Name of the upcoming measurement (if measurement has been scheduled), Name of the current measurement (if measurement is running), Name of the latest measurement (if measurement has been stopped) 
   * - created   
     - str   
     - Creation time of this measurement object (ISO 8601 \w TZ)
   * - start_measure   
     - str   
     - Starting time of the running measurement, starting time of the latest measurement, starting time of the next measurement (if measurement has been scheduled, ISO 8601 \w TZ)
   * - stop_measure   
     - str   
     - time when the running measurement is stopped, time when the latest measurement is stopped, time when the upcoming measurement is stopped (if measurement has been scheduled, ISO 8601 \w TZ)
   * - active   
     - bool   
     - `true` if measurement is running, `false` otherwise
   * - stopped   
     - bool   
     - `true` if measurement has been stopped manually 
   * - result_id 
     - int 
     - Result ID of most recent measurement 
   * - disk_space 
     - int 
     - Available server disk space 
   * - max `plot` data_points 
     - int 
     - Maximum number of data points in the plots (visualization menu)
   * - live_mean 
     - int 
     - Number of values used to calculate mean values during live data view 
   * - connected_sensors 
     - int 
     - Number of sensors connected to this server 
   * - max_freq 
     - decimal 
     - Maximum sampling freqency [Hz] (2 significant digits) 
   * - nc timescale 
     - bool 
     - `true` if unified timescale for netCDF files is activated, `false` otherwise 
   * - mms_version 
     - str 
     - Currently used MMS version 
   * - git_commit 
     - str 
     - Currently used MMS git commit 

/api/results/overview 
..................................................................................................................
`Example File <_static/api/example_sensors.json>`__

.. list-table::
   :header-rows: 1
   
   * - variable 
     - type 
     - details
   * - name   
     - str  
     - Name of the upcoming measurement (if measurement has been scheduled), Name of the current measurement (if measurement is running), Name of the latest measurement (if measurement has been stopped) 
   * - start_measure   
     - str  
     - Starting time of the running measurement, starting time of the latest measurement, starting time of the upcoming measurement (if measurement has been scheduled, ISO 8601 \w TZ)
   * - stop_measure   
     - str    
     - time when the running measurement is stopped, time when the latest measurement is stopped, time when the upcoming measurement is stopped (if measurement has been scheduled, ISO 8601 \w TZ)


/api/init 
.........
`Example File <_static/api/example_init.json>`__


.. list-table::
   :header-rows: 1
   
   * - variable 
     - type 
     - details 
   * - init   
     - str  
     - Indicates if an initialization process is requested 
   * - init_ftdi   
     - bool 
     - True if FTDI were initialized correctly 
   * - init_sensors   
     - bool 
     - True if sensors were initialized correctly and sensors were found
   * - state   
     - str 
     - "ok" if no problems occured, "error" otherwise 
   * - error   
     - str 
     - Error description
   * - total_ports   
     - int 
     - Number of i2c ports available for scanning
   * - scanned_ports  
     - int 
     - Number of ports scanned during the current sensor initialization 
   * - detected_sensors   
     - int 
     - Number of sensors detected
   * - sensors_initialized   
     - int 
     - Number of sensors which are detected and initialized correctly. Initialization processes are sensor specific and may include requests of calibration data or for initialization of the continuous measurement mode (sensirion sensors) 

/api/log
........
`Example File <_static/api/example_log.json>`__


.. list-table::
   :header-rows: 1

   * - variable 
     - type 
     - details 
   * - time   
     - str   
     - Time of logged event (ISO 8601 \w TZ) 
   * - message   
     - str  
     - Log message
   * - typ   
     - str   
     - Event type i.e. "ERROR" 

/api/sensors
............
`Example File <_static/api/example_sensors.json>`__

.. list-table::
   :header-rows: 1

   * - variable 
     - type 
     - details 
   * - units   
     - dict  
     - Measurement quantity (qty) and unit symbol (sign) 
   * - FTDIserial   
     - str 
     - Serial number of the FTDI  i.e. FT0NW1YD 
   * - SENSORserial   
     - str  
     - Unique sensor serial number. This identifier can be stored on the sensor chip or maybe computed as a pseudo identifier using the FTDIserial in combination with the CS/Port information or the calibration data i.e. 1493590176
   * - SENSORtype   
     - str 
     - Sensor type i.e. SHT85 
   * - SENSORidentifier 
     - str 
     - i.e. MySensor 
   * - MUXaddress   
     - int 
     - I2C address (I2C sensors only) of the I2C-Multiplexer (2 byte, base 10) i.e. 274
   * - MUXport   
     - int 
     - Port number of the I2C-Multiplexer (I2C sensors only) i.e. 0
   * - CS   
     - int 
     - Channel select (CS) pin of the FTDI (SPI sensors only) i.e. 0
   * - Color 
     - str 
     - Associated plot color of the sensors i.e. #0a00ff 
   * - connected 
     - bool 
     - True if the sensor was found during the latest initialization process.
   * - serial_mode 
     - str 
     - Protocol used to communicate with sensor, i.e. I2C 
   * - time   
     - str  
     - Time when the sensor is registered for the first time 2020-07-14 19:51:57.515450+0200 (ISO 8601 \w TZ )

/api/results/{n}
................
`Example File <_static/api/example_results.json>`__

Shows the entire data of all sensors initialized for the measurement with the corresponding ID {n}.
Sensor information are copied from `/api/sensors` and displayed together with the measurement results.
Each entry of the result list contains a key identically to a key provided in the sensor type unit list and 
a time stamp formatted according to ISO8601 \w TZ i.e. 2020-11-17 12:54:16.936868+0100. 

.. warning::
    Calling ``/api/results/{n}`` for measurements with a large amount of values may cause high loads and affect running measurements.
    It is adviced to do this request only between measurement runs.
    A large amount of values causes high loads on the client browser.
    This can be reduced by appending ``/?format=json`` (i.e. ``localhost/api/results/11/?format=json``).
    Calling ``/api/results/`` displays all values of all measurements stored on the measurement system and may cause high loads on the database.
    Avoid this call if large amounts of measurement data are stored on the device.

/api/last/{n}/{m}
.................
`Example File <_static/api/example_last.json>`__

This request returns the last {m} result entries for each sensor of the measurement run indentified by the ID {n}.
Additionally, the output provides information about the measurement as described in section ``/api/measurement``.
For each supported sensor type a list entry is registered which is populated if a sensor of that type was present in the measurement setup.
Sensor specific information as described in ``/api/sensors`` are given together with the measurement frequency and the corresponing results.
Generally, independent of the sensor type, the time stamp is labeled with "time".
The number and labels of the additional result entries depend on the sensor type.
For instance, a SHT85-sensor provides the following result entries:


.. list-table::
   :header-rows: 1

   * - label       
     - id       
     - value                      
   * - humidity    
     - 0        
     - "51.16"                    
   * -             
     - 1        
     - "51.16"                    
   * -             
     - 2        
     - "51.17"                    
   * -             
     - 3        
     - "51.19"                    
   * -             
     - 4        
     - "51.19"                    
   * - temperature 
     - 0        
     - "24.25"                    
   * -             
     - 1        
     - "24.24"                    
   * -             
     - 2        
     - "24.22"                    
   * -             
     - 3        
     - "24.25"                    
   * -             
     - 4        
     - "24.25"                    
   * - time        
     - 0        
     - "2020-07-14T17:54:27.460Z" 
   * -             
     - 1        
     - "2020-07-14T17:54:27.653Z" 
   * -             
     - 2        
     - "2020-07-14T17:54:27.938Z" 
   * -             
     - 3        
     - "2020-07-14T17:54:28.225Z" 
   * -             
     - 4        
     - "2020-07-14T17:54:28.399Z" 

/api/last/current/{m}/
........................

The format is identical to ``/api/last/{n}/{m}`` but the measurement id {n} is always equal to the highest known 
measurement id and thus corresponds to the most recent ended or currently running measurement.
To check if a measurement is currently running, see the request: ``/api/measurement``.


