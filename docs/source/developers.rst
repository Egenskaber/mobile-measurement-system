Developers
----------

Setup development environment
``````````````````````````````

Clone the project and navigate into the project directory

.. code-block:: shell

    git clone https://gitlab.dlr.de/as-boa/mobile-measurement-system.git
    cd mobile-measurement-system

You need to fullfill the prerequisites detailed in :ref:`software setup <softwaresetup>`.
Additionally, install Python >3.8, Postgres dev (libraries + headers) and docker like so:

.. code-block:: shell

    sudo apt-get install python3-dev build-essential docker python3-pip libpq-dev # Debian / Ubuntu
    sudo yum install python3-devel python3-pip docker libpq-devel # Red Hat / CentOS

Make sure to setup a virtual environment and then run

.. code-block:: shell

    pip install -r MMS/requirements.txt

You can use

.. code-block:: shell

    ./dev.sh start

to start, restart or stop a fresh development environment with a clean database running in a container called timescaledb. 
The timescaledb database is started in the `` ./postgres.sh `` script.


Tests
```````

Switch to the virtual environment of your development setup and start the local postgres database 
via ``./postgres.sh``.
Tests are executed by

.. code-block:: shell

    cd MMS
    ./test.sh dev

If you want to test a function under development you can find snippets 
for isolated testing in the comments of `test.sh`.

New sensors
``````````````

The sensors have to implemented into the `pyserialsensensor library <https://gitlab.com/Egenskaber/pyserialsensors/>`__ , which is a standalone subproject.
Projects are split to make it possible to integrate the direkt host to sensor communication without the mobile measurement system as a data acquisition system.

When the implementation into pyserialsensor is finished there are only a few lines of code that have to modified in the mobile measurement system.

    1. Update the pyserialsensor version in ``MMS/requirements.txt``
    2. Update the following files and orient at previously implemented sensors. It should be simple copy and paste

       - MMS/core/modules/sensors.py
       - MMS/measurement_server/models/measurement.py
       - MMS/measurement_server/sensor_serializers.py
       - MMS/measurement_server/serializers.py
    2. Update your development environment using ``pip install -r MMS/requirements.txt``
    3. Start the development server ``` ./dev.sh start ```
    4. Add the hypertable sql command to the new migration file in MMS/measurement_server/migrations
    5. Run the test scripts ``cd MMS; ./test.sh dev`` and debug if indicated.
    6. Add datasheets and images to
       - MMS/measurement_server/media/datasheets/overview.py
       - MMS/measurement_server/media/datasheets/pdf/
       - MMS/measurement_server/media/datasheets/img/
    7. Add link to datasheet documentation ``docs/source/sensors.rst``
    8. Add the sensor class to the interface ``MMS/measurement_server/templates/measurement_server/live.html``

.. note::
    Make sure that serial numbers do not include special characters. Otherwise javascript in i.e. live.html will give you a hard time!

Release instructions
``````````````````````

To release a new version make sure all local work on your current branch is done.

Add all changes to ``CHANGELOG.md`` in the main folder while sticking to the existing format and using the following tags:

New
    Used for new additions or features
Changes
    Used for changes to existing features
Improvements
    Used for improvements, e.g. improvements to functionality, code style, test coverage, documentation
Fixed / Fixes
    Used for fixes of existing features or bugs
Breaks
    Used for breaking points in updates where backward compatibility between versions is not ensured anymore, e.g. major version bumps

Add your changed files to the upcoming commit:

.. code-block:: shell

    git add <path/to/changed/file>

Check if all needed files were added:

.. code-block:: shell

    git status

Commit and push your local branch:

.. code-block:: shell

    git commit -m <short info about made changes>
    git push --set-upstream origin <your-branch-name>

Switch to the dev branch to merge with your local branch, then push dev branch:

.. code-block:: shell

    git switch dev
    git merge <your-branch-name>
    git push --set-upstream origin dev

Make sure your push was properly built and has passed the Git pipeline on the dev branch. Switch to the main branch to merge with the dev branch, then push the master branch:

.. code-block:: shell

    git switch main
    git merge dev
    git push --set-upstream origin main

Bump the version according to the changes made, then push the version tags:

.. code-block:: shell

    make major-release # bumps major version e.g. X.0.0
    make minor-release # bumps minor version e.g. 0.X.0
    make patch-release # bumps patch version e.g. 0.0.X

    git push --tags

Make sure your push was properly built and has passed the Git pipeline on the main branch. If so, your version update was successful.