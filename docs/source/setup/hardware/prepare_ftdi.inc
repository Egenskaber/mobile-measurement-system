.. _Hardware FTDI:

Set FTDI Identifiers
`````````````````````
If you are planing to use FT232 USB to I2C/SPI converters you need to assign unique device identifiers. 
This is well documented in the `pyFTDI project <https://eblot.github.io/pyftdi/eeprom.html#examples>`_.
An example script can be found in the `pyserialsensor subproject <https://gitlab.com/Egenskaber/pyserialsensors/-/blob/master/tools/rename.py>`_ which is responsible for the sensor communication.
