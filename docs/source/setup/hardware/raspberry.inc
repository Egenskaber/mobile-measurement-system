Raspberry Pi
`````````````

================== =============== ======== ==================================
Base parts         Note            Supplier Order ID
================== =============== ======== ==================================
Raspberry Pi 4     min. 4GB        Digi Key 1690-RASPBERRYPI4B/4GB-ND
5V power supply                    Digi Key 1690-RPIUSB-CPOWERSUPPLYBLACKEU-ND
Case (Din-Rail)                    Digi Key 1778-DINH03-ND
Real Time Clock                    Digi Key 1597-1391-ND
LED-Button         red             Conrad   4016139335087
LED-Button         green           Conrad   4016139335148
================== =============== ======== ==================================


