
Pt100 breakout boxes
---------------------

This projects supports the use of 4-wire Pt100 RTD probes.
The measurement of the sensor resistance together with a analog to digital conversion is done on a MAX31865 IC and is evaluated via SPI.
There are two different implementations supported by the mobile measurement system.

* up to 5 Pt100 using one GPIO per MAX31865 as channel select 
* up to 16 Pt100 using channel select multiplexing (via CD74HC4067M) 

You can either use only 5 channel versions or 16 channel pcb versions of the breakout boxes simultaneously.

5 channel Pt100 box 
````````````````````

This can be set up easily on a breadboard for quick testing purposes. For a more reliable version use the 16 channel version.

**Bill of Materials**

* FT232H (`Datasheet <https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232H.pdf>`__) `DigiKey <https://www.digikey.de/de/products/detail/adafruit-industries-llc/2264/5761217?s=N4IgTCBcDaIIwFYwA4C0cAsGCcqByAIiALoC%2BQA>`__, `Mouser <https://www.mouser.de/ProductDetail/Adafruit/2264?qs=GURawfaeGuCKgUR%2FBiAWKA%3D%3D>`__, `Exp Tech <https://www.exp-tech.de/module/schnittstellen/6112/adafruit-ft232h-breakout-general-purpose-usb-to-gpio-spi-i2c>`__
* MAX31865 (`Datasheet <https://datasheets.maximintegrated.com/en/ds/MAX31865.pdf>`__) `DigiKey <https://www.digikey.de/de/products/detail/adafruit-industries-llc/3328/6562952>`__, `Mouser <https://www.mouser.de/ProductDetail/Adafruit/3328?qs=sGAEpiMZZMug%252BNZZT2EIM6eeJeRId4f78LgcDi71SmA%3D>`__, `Exp Tech <https://www.exp-tech.de/sensoren/temperatur/7862/adafruit-pt100-rtd-temperature-sensor-amplifier-max31865>`__

**Wiring** 

* D0 pin of the FT232H to each CLK of the MAX31865 breakout board.
* D1 pin of the FT232H to each SDI of the MAX31865 breakout board.
* D2 pin of the FT232H to each SDO of the MAX31865 breakout board.
* For each MAX31865 connect CS to one of the D3 to D7 pin of the FT232H 
* For each MAX31865 connect F- and RTD- to one side of the 4-wire Pt100 and F+ and RTD+ to the other.

In `Settings/General` select SPI mode `GPIO` to indicate you are using the 5 channel version.


16 channel Pt100 box 
``````````````````````

.. image:: ../../hardware/pcbs/max31865_pt100_box/Hardware/max31865_16_pt100_front.png
   :width: 600


The layout of the PCB, the KiCAD project and the gerber-export can be found `here <https://gitlab.com/Egenskaber/mobile-measurement-system/-/tree/dev/hardware/pcbs/max31865_pt100_box>`__.

**Bill of Materials**

* Multiplexer CD74HC4067M
* Pt100 transducer MAX31865xAP
* Capacitor CAP CER 0.1UF 50V X7R 0805
* FERRITE BEAD 120 OHM 0805 1LN
* Reference resistor (for Pt100) ERA-6AEB431V  - RES 430 OHM 0.1% 1/8W 0805
* Resistor RES 10K OHM 1% 1/4W 0805
* Conn_01x12_Female
* PIN - Socket B4B-ZR(LF)(SN)

**Wiring**

Solder a pin row to each side of an upside down Adafruit FT232H breakout board.
Plug the FT232H into the socket rows of the PCB. 
Make sure to connect the correct pins (12 Pins on the socket and 11 pins on the the Adafruit FT232H) and double check if GND matches GND and 3.3V matches 3.3V on the FT232H.

Next you need to connect the 4-wire Pt100 to the board.
For each channel connect pins 1+3 to one side of the Pt100 and 2+4 to the other.
You can use JST PHR Female 2mm, 4-pin 1 row (`RS Components <https://de.rs-online.com/web/p/steckergehause-und-stecker/8201478>`__, `Farnell <https://de.farnell.com/jst-japan-solderless-terminals/phr-4/steckverbindergeh-use-4pol-2mm/dp/3616204>`__, `DigiKey <https://www.digikey.de/de/products/detail/jst-sales-america-inc/PHR-4/608606>`__) with self or pre-crimped wires.

A layout of a breakout box can be found `here <https://gitlab.com/Egenskaber/mobile-measurement-system/-/blob/dev/hardware/3d_models/Pt100Breakout/pt100box.stp>`__. The boxes can be built using the following parts

* 16 x Mini XLR `Farnell <https://de.farnell.com/en-DE/rean/rt4mp/plug-tiny-xlr-chassis-4way/dp/1813721?st=rt4mp%20-%20xlr-steckverbinder,%20mini%20xlr,>`__ 
* 1 x Box Hammond Electronics 1590DDFL `Farnell <https://de.farnell.com/deltron-enclosures/459-0050/druckguss-geh-use/dp/525649>`__
* 32 x pre crimed wires `Farnell <https://de.farnell.com/jst-japan-solderless-terminals/01sphsph-26001l300/kabel-300mm-2xph-buchse-26awg/dp/2320543>`__
* 16 x Female 4 pin ZHR socket `Farnell <https://de.farnell.com/jst-japan-solderless-terminals/zhr-4/buchsengeh-use-crimpbar-1-5mm/dp/3357569?MER=sy-me-pd-mi-acce>`__

