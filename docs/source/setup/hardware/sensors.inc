Sensors
```````
A full list is included in the :ref:`featured sensor <featuredsensors>` section.
The following subset can be ordered directly of the shelf.

.. list-table::
   :header-rows: 1

   * - Sensor type
     - Quantities
     - Supplier
     - Article Number
     - Note
   * - SHT 85
     - Temperature
     - Digi Key
     - 1649-1110-ND
     -
   * -
     - Humidity
     -
     -
     -
   * - SFM3000
     - Volume flow
     - Mouser
     - 403-SFM3000-200C
     - add socket cap
   * - SFM3200
     - Volume flow
     - Mouser
     - 403-SFM3200-AW
     - add socket cap
   * - SFM3300
     - Volume flow
     - Mouser
     - 403-SFM3300-AW
     - add socket cap
   * - SFM4100
     - Volume flow
     - Mouser
     - 403-SFM4100AIR
     -
   * - SFM4200
     - Volume flow
     - Mouser
     - 403-SFM4200AIR
     -
   * - SDP810 (125Pa)
     - Differential pressure
     - Digi Key
     - 1649-1073-ND
     - add socket cap
   * -
     - Temperature
     -
     -
     -
   * - SDP810 (500Pa)
     - Differential pressure
     - Digi Key
     - 1649-1074-ND
     - add socket cap
   * -
     - Temperature
     -
     -
     -
   * - BME280
     - Absolute pressure
     - Digi Key
     - 1568-1706-ND
     - Qwiic compatible
   * -
     - Temperature
     -
     -
     -
   * -
     - Humidity
     -
     -
     -
   * - MAX31865
     - Temperatue
     - Digi Key
     - 1528-1804-ND
     - Add 4-Wire PT100 RTD
   * - SCD30
     - CO2 concentration
     - Digi Key
     - 1649-1098-ND
     -
   * -
     - Temperature
     -
     -
     -
   * -
     - Humidity
     -
     -
     -
   * - SCD41
     - CO2 concentration
     - Digi Key
     - 1649-SEK-SCD41-SENSOR-ND
     -
   * -
     - Temperature
     -
     -
     -
   * -
     - Humidity
     -
     -
     -
   * - SMGAir 2
     - Volume flow
     - Ingenieurs Büro Peter Stehle
     -
     -
   * -
     - Temperature
     -
     -
     -
   * -
     - Abs. pressure
     -
     -
     -
   * - S8000
     - Dewpoint
     - Michell Instruments
     -
     -
   * -
     - Temperature
     -
     -
     -
   * - ADS1015
     - Voltage
     - Mouser
     - 474-DEV-15334
     -
   * - NAU 7802
     - Bridge Sensor
     - Mouser
     - 474-SEN-15242
     -
   * - SPS30
     - Particle matter sensor
     - Mouser
     - 403-SPS30
     -



