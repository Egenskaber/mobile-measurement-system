.. _mwe:

Minimum working examples
----------------------------

In the following the minimal working example for different interfaces is described

UART
````

Material:
    * A host system with at least 1 free USB port 
    * Particle Matter Sensor SPS30 
      (`Mouser <https://www.mouser.de/ProductDetail/Sensirion/SEK-SPS30?qs=lc2O%252BfHJPVYY3o4J9BnBfg%3D%3D>`__ , 
      `DigiKey <https://www.digikey.de/de/products/detail/sensirion-ag/SEK-SPS30/9598991>`__ ,
      `Farnell <https://de.farnell.com/sensirion/sek-sps30/evaluationskit-partikelsensor/dp/3804232>`__ ,
      `Datasheet <https://sensirion.com/media/documents/8600FF88/616542B5/Sensirion_PM_Sensors_Datasheet_SPS30.pdf>`__)

Steps:
    1. Connect to the host device by entering its network address in the web browser of a device within the same local network.
    #. Connect the SPS30 to the host device. The number of COM-ports in the interface footer should increment.
    #. Navigate to the setup menu and click the yellow button in the center to start an initialization process. After completion the sensor should be recognized and a green start button on the top right of the header menu should be visible.
    #. Click on the start button. It should switch to a red stop button and a red Live menu entry should appear in the header menu.
    #. The live menu displays current measurement values. If you want to see a visualization of the measurement values, navigate to the visualization menu.

I2C
````

Material:
    * A host system with at least 1 free USB port 
    * FT232H Breakout board
      (`Mouser <https://www.mouser.de/ProductDetail/Adafruit/2264?qs=GURawfaeGuCKgUR%2FBiAWKA%3D%3D>`__ ,
      `DigiKey <https://www.digikey.de/de/products/detail/adafruit-industries-llc/2264/5761217?s=N4IgTCBcDaIIYBM4DMBOBXAlgFwATOzAGYwALEAXQF8g>`__ ,
      `Reichelt <https://www.reichelt.com/de/en/developer-boards-usb-to-serial-adapter-ft232h-debo-usb-serial-p235510.html?&trstct=pos_0&nbc=1>`__ ,
      `Datasheet <https://cdn-reichelt.de/documents/datenblatt/A300/ADAFRUIT-FT232H-BREAKOUT.pdf>`__)
    * Environmental sensor BME280 (Humidity, Temperature, Ambient pressure)
      (`Mouser <https://www.mouser.de/ProductDetail/SparkFun/SEN-15440?qs=sGAEpiMZZMuqBwn8WqcFUj1SFkunHY109UY5zZgz%2FBA4m5hbY0Wjdg%3D%3D>`__ ,
      `DigiKey <https://www.digikey.de/de/products/detail/sparkfun-electronics/SEN-15440/10440485>`__ ,
      `Reichelt <https://www.reichelt.com/entwicklerboards-temperatur-feuchtigkeits-und-drucksensor--debo-bme280-p253982.html?CCOUNTRY=445&LANGUAGE=de&trstct=pos_1&nbc=1&&r=1>`__ ,
      `Datasheet <https://cdn-reichelt.de/documents/datenblatt/B400/BOSCH_SENSORTEC_FLYER_BME280.pdf>`__)
    * I2C Multiplexer
      (`Mouser <https://www.mouser.de/ProductDetail/SparkFun/BOB-16784?qs=sGAEpiMZZMv0NwlthflBi18IUD%252BnoBFhqpUqdTQ5qQw%3D>`__ ,
      `DigiKey <https://www.digikey.de/de/products/detail/adafruit-industries-llc/2717/5604376>`__ ,
      `Distrelec <https://www.distrelec.de/en/tca9548a-channel-qwiic-mux-breakout-sparkfun-electronics-bob-14685/p/30160861>`__ ,
      `Datasheet <https://cdn.sparkfun.com/assets/f/0/4/b/3/tca9548a.pdf>`__)
    * If you are using a Qwiic Multiplexer add Qwiic wires to conveniently connect FTDI + MUX and MUX + sensor. 
      (`Mouser <https://www.mouser.de/ProductDetail/Adafruit/4210?qs=sGAEpiMZZMv0NwlthflBi%2FELhZLjwLawvOGnBerqPZs%3D>`__ ,
      `DigiKey <https://www.digikey.de/de/products/filter/zubeh%C3%B6r/783?s=N4IgTCBcDaII4HcCWSDGACVBDARgGwFMQBdAXyA>`__ ,
      `Distrelec <https://www.distrelec.de/search?q=qwiic+cable>`__ )

Wiring:
        * SDA: Connect D1 and D2 to the blue wire (Data in and Data out) 
        * SCL: Connect D0 to the yellow wire (Clock) 
        * Vin: Connect 5V to the red wire (supply voltage)
        * GND: Connect GND to the red wire 
        * Connect D0 and D7 (clock stretch)
        * If you are not using a Sparkfun TCA9548 breakout board connect D0 and 5V with a 4.7kOhm resistor and D1 and 5V with a 4.7kOhm resistor


