
Raspberry Pis
-------------

Control buttons
```````````````

Buttons to initialize a sensor search and to start/stop a measurement are supported using its GPIOs.
Further LEDs are used to indicate a successful initialization and an ongoing measurement.
The corresponding container can be installed via
To add LED support an additional container needs to be registered:

.. code-block:: shell

    docker-compose -f docker-compose.yml -f docker-compose-pi.yml

The pins are identical throughout all Pi versions.
If the use of buttons is activated, buttons have to be connected.

* The initialization button has to be connected to GPIO 7 and GND.
* The measurement button has to be connected to GPIO 27 and GND.

LED indicators are all optional. 

* The init indicator has to be wired to GPIO 7 and GND.
* The measurement indicator has to be connected to GPIO 10 and GND.

To start the initialization process, push the initialization button.
The initialization indicator will start to blink (1/2Hz).
When the initialization process has finished successfully, the blinking stops and the initialization LED is continuously active. 
To start a measurement, the initialization process has to be finished.
If a measurement is ready to be started, the measurement LED is active. 
After pushing the measurement button the measurement starts and the measurement indicator will blink (1/2Hz).
To stop a measurement, push the button again. 
The blinking will stop and the measurement ends.
During measurements the initialization button is deactivated.
Vice versa the measurement button is deactivated when the system performs an initialization.
The web interface reflects all state changes evoked by the push buttons.
Measurements are labeled according to the system time and can be found in the 'Data' tab of the 'Settings' menu.

When using a RPi 4 make sure to add proper pull up resistors to both buttons. 


Wifi Client
```````````

A wifi access point for Raspberry Pis can be installed on the system level using the installation script in the setup directory ``setup/install_pi_ap.sh``. 
Default parameters:

- Gateway/Device IP: 192.168.4.1
- SSID: YOUR_SSID
- Password: YOUR_SECURE_PASSWORD
- max. 20 dhcp clients

To alter these settings modify
    * *hostapd* configuration file at ``setup/setup_files/hostapd.conf``
    * *name server* configuration at ``setup/setup_files/dnsmasq.conf`` 
    * *network translation* at ``setup/setup_files/nat_config.conf``

Also usefull
`````````````

After cloning images shrink using:
    https://github.com/Drewsif/PiShrink
