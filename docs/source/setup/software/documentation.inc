
Generating the documentation
````````````````````````````
If you want to compile the documentation yourself,
you need to have python 3.8 and the python package manager
pip to be installed. It is recommended to use a virtualenvironment to make
sure not to mess with your systems python packages. Then run:

.. code-block:: shell

   git clone https://gitlab.com/Egenskaber/mobile-measurement-system.git
   cd mobile-measurement-system
   make doc

After the documentation has been build successfully a browser will open
and present the start page `from docs/_build/index.html`.

    



