Installation
````````````

Prepare your system

.. code-block:: shell

    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install docker docker-compose git -qq -y
    sudo addgroup docker
    sudo usermod -aG docker $USER
    sudo service docker start
    sudo systemctl enable docker

.. note:: 
    On some systems docker might not be available in the standard distribution repositories. 
    Check the internet for guidance!

Copy the code base 

.. code-block:: shell

    git clone https://gitlab.dlr.de/as-boa/mobile-measurement-system.git

If you are planing to use I²C oder SPI sensors (standard use case) you need to register FTDI udev-rules

.. code-block:: shell

    cd mobile-measurement-system/setup
    bash ./install_usb.sh

Download and start the container stack:

.. code-block:: shell

    cd ..
    sudo docker-compose up -d

As a sanity check make sure four containers with an identifier MMS_* are up and running (no restarting)

.. code-block:: shell

    sudo docker ps

This should look similar to:

.. code-block:: shell

    [...]   IMAGE                              [...]   STATUS         PORTS                                       NAMES
    [...]   nginx:1.17                         [...]   Up 1 second    0.0.0.0:80->80/tcp, :::80->80/tcp           MMS_NGINX
    [...]   niehaus/mms:latest                 [...]   Up 1 second                                                MMS_SENSOR
    [...]   niehaus/mms:latest                 [...]   Up 2 seconds   0.0.0.0:8000->8000/tcp, :::8000->8000/tcp   MMS_GUI
    [...]   timescale/timescaledb:1.7.4-pg12   [...]   Up 3 seconds   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp   MMS_DB


The MMS is installed now and can be accessed on the hosting machine via the browser using ``http://localhost`` or via other devices on the network via ``http://<host-ip>/``.
Please refer to the :ref:`user manual <usermanual>` for operating instructions.
Sensor datasheets can be found in the documentation section of the web interface or the hardware description of this documentation. 


