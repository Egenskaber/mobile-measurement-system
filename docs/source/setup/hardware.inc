Hardware
----------------------

A list of supported devices and where they can be ordered is presented in the following

.. include:: /setup/hardware/sensors.inc

.. include:: /setup/hardware/periphery.inc

.. include:: /setup/hardware/cables.inc

.. include:: /setup/hardware/raspberry.inc

.. include:: /setup/hardware/prepare_ftdi.inc

.. include:: setup/example_setup.rst

.. include:: setup/hardware/pt100.inc

