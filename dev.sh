# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

#!/bin/bash

VARIABLE="${1:-cmd}"

if [ "$VARIABLE" = "start" ]; then
    ./postgres.sh
    sleep 5
    # Collect static
    #python MMS/manage.py collectstatic --noinput --settings=settings.dev
    # Prepare database 
    python MMS/manage.py makemigrations --noinput --settings=settings.dev
    python MMS/manage.py migrate --noinput --settings=settings.dev
    # Start web server
    python MMS/manage.py runserver 0.0.0.0:4445 --settings=settings.dev &
    # Start sensor evaluation
    python MMS/manage.py readSensor --settings=settings.dev &
elif [ $VARIABLE == "stop" ]; then
    pkill -f MMS/manage.py
    sudo docker stop timescaledb
elif [ $VARIABLE == "restart" ]; then
    ./dev.sh stop
    ./dev.sh start

else
    echo "Invalid command argument (use dev.sh (start|stop|restart))"
fi
